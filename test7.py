import sys
from PyQt5 import QtWidgets, QtCore


class CustomItem(QtWidgets.QGraphicsEllipseItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setFlag(self.ItemIsMovable)
        self.setFlag(self.ItemSendsGeometryChanges)
        self.line = None
        self.isPoint = None

    def addLine(self, line, ispoint):
        self.line = line
        self.isPoint = ispoint

    def itemChange(self, change , value):

        if change == self.ItemPositionChange and self.scene():
            newPos = value

            self.moveLineToCenter(newPos)

        return super(CustomItem, self).itemChange(change, value)

    def moveLineToCenter(self, newPos):
        xOffset = self.rect().x() + self.rect().width()/2
        yOffset = self.rect().y() + self.rect().height()/2

        newCenterPos = QtCore.QPointF(newPos.x()+xOffset, newPos.y()+yOffset)

        p1 = newCenterPos if self.isPoint else self.line.line().p1()
        p2 = self.line.line().p2() if self.isPoint else newCenterPos

        self.line.setLine(QtCore.QLineF(p1, p2))


def main():
    app =QtWidgets.QApplication(sys.argv)
    scene = QtWidgets.QGraphicsScene()

    ellipse = CustomItem(QtCore.QRectF(40, 40, 15, 15)) # left, top, width, height 後兩控制圓的形狀
    scene.addItem(ellipse)

    ellipse2 = CustomItem(QtCore.QRectF(90, 40, 15, 15))
    scene.addItem(ellipse2)

    line = scene.addLine(QtCore.QLineF(50, 50, 100, 50))

    ellipse.addLine(line, True)
    ellipse2.addLine(line, False)

    view = QtWidgets.QGraphicsView(scene)
    view.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()