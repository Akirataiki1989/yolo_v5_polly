import cv2, logging, colorlog
from pathlib import Path
import numpy as np
import datetime as dt
import configparser

'''

*****cv2.findContours(image, mode, method[, offset])*****
image:8位单通道图像。非零像素值视为1，所以图像视作二值图像
mode:轮廓检索的方式
cv2.RETR_EXTERNAL:只检索外部轮廓
cv2.RETR_LIST: 检测所有轮廓且不建立层次结构
cv2.RETR_CCOMP: 检测所有轮廓，建立两级层次结构
cv2.RETR_TREE: 检测所有轮廓，建立完整的层次结构
method:轮廓近似的方法
cv2.CHAIN_APPROX_NONE:存储所有的轮廓点
cv2.CHAIN_APPROX_SIMPLE:压缩水平，垂直和对角线段，只留下端点。 例如矩形轮廓可以用4个点编码。
cv2.CHAIN_APPROX_TC89_L1,cv2.CHAIN_APPROX_TC89_KCOS:使用Teh-Chini chain近似算法
offset:（可选参数）轮廓点的偏移量，格式为tuple,如（-10，10）表示轮廓点沿X负方向偏移10个像素点，沿Y正方向偏移10个像素点

延伸閱讀:https://blog.csdn.net/qinglingLS/article/details/106270095

*****cv2.drawContours(image, contours, contourIdx, color[, thickness[, lineType[, hierarchy[, maxLevel[, offset]]]])*****
image:需要绘制轮廓的目标图像，注意会改变原图
contours:轮廓点，上述函数cv2.findContours()的第一个返回值
contourIdx:轮廓的索引，表示绘制第几个轮廓，-1表示绘制所有的轮廓
color:绘制轮廓的颜色
thickness:（可选参数）轮廓线的宽度，-1表示填充
lineType:（可选参数）轮廓线型，包括cv2.LINE_4,cv2.LINE_8（默认）,cv2.LINE_AA,分别表示4邻域线，8领域线，抗锯齿线（可以更好地显示曲线）
hierarchy:（可选参数）层级结构，上述函数cv2.findContours()的第二个返回值，配合maxLevel参数使用
maxLevel:（可选参数）等于0表示只绘制指定的轮廓，等于1表示绘制指定轮廓及其下一级子轮廓，等于2表示绘制指定轮廓及其所有子轮廓
offset:（可选参数）轮廓点的偏移量

*****格林公式****
在高数的曲线曲面积分部分，格林公式、高斯公式和斯托克斯公式是三个十分十分重要的公式，利用这三个公式可以将复杂的积分转化为我们比较熟悉的积分进行解决。
格林公式的应用是有条件的：封闭且正向、被积函数在D上是连续的。

*****countArea()*****
取连通域边界像素中心点，连接起来，成为一个轮廓，导致一周得边界像素点丢失，即求得得面积比真实得面积少了一圈。
'''

class main:
    def __init__(self, magnificationRate_path='magnificationRate.ini'):
        self.logger = StyleLog(loglevel='DEBUG', logger_name='Roundness', logfile='Roundness.log')
        self.main_task_id = str(dt.datetime.now().timestamp())
        diagnosis_folder_path = Path('./Roundness_diagnosis')
        diagnosis_folder_path.mkdir(parents=True, exist_ok=True)
        self.main_task_folder_path = diagnosis_folder_path / self.main_task_id
        self.main_task_folder_path.mkdir(parents=True, exist_ok=True)
        self.magnificationRate_config = configparser.ConfigParser()
        self.magnificationRate_config.read(magnificationRate_path)

        '''
        require_dict = {'image_id':'<str>',  如果為空會以當下時間戳為任務ID，主要是給diagnosis用
                        'source_img_np':'<numpy.ndarray>', RBG格式np-array
                        'x1y1x2y2':'<list>', 裁切座標4個,值可float
                        'low_hsv':'<list>', HSV low, 前處理需用到HSV作為二值化要給,值的範圍0~255,不可float
                        'up_hsv':'<list>', HSV up, 前處理需用到HSV作為二值化要給,值的範圍0~255,不可float
                        'output_diagnosis':'<bool>', Dbug用,True會輸出執行log和裁切目標在各計算方式下畫圖,用於分析用,建議false
                        'crop_offset':'<list>',  裁切圖往四周延伸,建議5,比較好抓輪廓
                        'camera_zoom_magnification':'int', 相機的倍率,如果有用到「實圓率」計算一定要給,會根據鏡頭倍率抓magnificationRate.ini內的係數
                        'target_distance':'int' 理想孔徑直徑,如果有用到「實圓率」計算一定要給
                        'fit_ellipse_select_mode':'<list>' 選擇均圓率輸出模式,模式有:[ellipse_fit_rate, circle_fit_rate, idealcircle_fit_rate],給空字串會輸出全部
            }
        '''
        
    # 計算 MZC 真圓度
    def roundness_value(self, r_dict):
        mincircle_radius = self.minenclosing_circle(r_dict)
        maxcircle_radius = self.maxenclosing_circle(r_dict)
        r = mincircle_radius - maxcircle_radius
        return r
    
    # 最小外接圓
    def minenclosing_circle(self, require_dict :dict):
        job_id =  'mic_%s'%(str(require_dict['image_id'])) if require_dict['image_id'] else 'mic_%s'%(str(dt.datetime.now().timestamp()))
        if (require_dict['low_hsv']) and (require_dict['up_hsv']):
            mask, frame = self.get_maskimg(require_dict, job_id)
        else:
            mask, frame = self.get_otsu(require_dict, job_id)

        contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        # cnt = contours[0]
        cnt = [c for c in contours if cv2.minEnclosingCircle(c)[1] >= ((np.max(frame.shape)/2)-10)][0]

        (x, y), radius = cv2.minEnclosingCircle(cnt)
        self.center = (int(x), int(y))  # 最小內接圓圓心
        radius = int(radius)
        self.min_radius = radius
        if require_dict['output_diagnosis']:
            self.logger.info('------MinCloseCircle:%s------'%job_id)
            if len(contours) > 5:
                contours_path = self.main_task_folder_path / ('%s-contours'%job_id)
                contours_path.mkdir(parents=True, exist_ok=True)
                self.draw_contour(contours_path, job_id, frame, contours)
                self.logger.info('multiple contour image is save.')
            else:
                self.draw_contour(self.main_task_folder_path, job_id, frame, contours)
                self.logger.info('contour image is save.')
            if require_dict['low_hsv'] and require_dict['up_hsv']:
                self.logger.info('HSV: {low:%s, up:%s}'%(','.join([str(i) for i in require_dict['low_hsv']]), ','.join([str(i) for i in require_dict['up_hsv']])))
            else:
                self.logger.info('HSV is empty, use OTSU.')
            self.logger.info('contours: %d'%(len(contours)))
            self.logger.info('center: %d, %d ; radius: %d'%(int(x), int(y), int(radius)))
            self.draw_circle('minCircle', frame, job_id, self.center, radius)
            self.logger.info('minCircle image is save.')
        return radius
    
    # 最大內接圓
    def maxenclosing_circle(self, require_dict :dict):
        job_id =  'mxc_%s'%(str(require_dict['image_id'])) if require_dict['image_id'] else 'mxc_%s'%(str(dt.datetime.now().timestamp()))
        if (require_dict['low_hsv']) and (require_dict['up_hsv']):
            mask, frame = self.get_maskimg(require_dict, job_id)
        else:
            mask, frame = self.get_otsu(require_dict, job_id)

        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnt = [c for c in contours if cv2.minEnclosingCircle(c)[1] >= ((np.max(frame.shape)/2)-10)][0]

        # 計算輪廓距離
        raw_dist = np.empty(mask.shape, dtype=np.float32)
        for i in range(mask.shape[0]):
            for j in range(mask.shape[1]):
                raw_dist[i, j] = cv2.pointPolygonTest(cnt, (j, i), True)
        
        # 獲取最大值即內切圓半徑 & 圓中心座標
        minVal, maxVal, _, maxDistPt = cv2.minMaxLoc(raw_dist)
        minVal = abs(minVal)
        maxVal = abs(maxVal)
        radius = np.int(maxVal)
        center_of_circle = maxDistPt
        if require_dict['output_diagnosis']:
            self.logger.info('------MaxCloseCircle:%s------'%job_id)
            if len(contours) > 5:
                contours_path = self.main_task_folder_path / ('%s-contours'%job_id)
                contours_path.mkdir(parents=True, exist_ok=True)
                self.draw_contour(contours_path, job_id, frame, contours)
                self.logger.info('multiple contour image is save.')
            else:
                self.draw_contour(self.main_task_folder_path, job_id, frame, contours)
                self.logger.info('contour image is save.')
            self.logger.info('contours: %d'%(len(contours)))
            self.logger.info('center: %d, %d ; radius: %d'%(center_of_circle[0], center_of_circle[1], int(radius)))
            self.draw_circle('maxCircle', frame, job_id, center_of_circle, radius)
            self.logger.info('maxCircle image is save.')
            d_radius = self.min_radius - maxVal
            self.draw_roundness(frame, job_id, self.center, int(self.min_radius), center_of_circle, radius, f'R:{d_radius:.2f}')
        return radius
    
    # 擬和橢圓
    def fit_ellipse(self, require_dict :dict):
        return_dict = {s:0 for s in ['ellipse_fit_rate', 'circle_fit_rate', 'idealcircle_fit_rate']}
        magnification = require_dict['camera_zoom_magnification']
        target_distance = require_dict['target_distance']
        ratio = float(self.magnificationRate_config.get('OM',f'{magnification}M_X'))
        target_radius = int((target_distance / ratio) / 2)
        job_id =  'feli_%s'%(str(require_dict['image_id'])) if require_dict['image_id'] else 'feli_%s'%(str(dt.datetime.now().timestamp()))
        if (require_dict['low_hsv']) and (require_dict['up_hsv']):
            mask, frame = self.get_maskimg(require_dict, job_id)
        else:
            mask, frame = self.get_otsu(require_dict, job_id)
        min_frame_area = (frame.shape[0] * frame.shape[1]) - (frame.shape[0] * frame.shape[1]) * 0.2 # 裁切圖減少2成面積,避免最大輪廓抓到整張畫布
        contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        if require_dict['output_diagnosis']:
            self.logger.info(f'------FitEllipse:{job_id}------')
            self.logger.info('contours: %d'%(len(contours)))

        cnt_dict = {cv2.contourArea(cnt):c for c, cnt in enumerate(contours) if cv2.contourArea(cnt) <= min_frame_area}
        max_cnt = np.max(list(cnt_dict.keys()))
        max_index = cnt_dict[max_cnt]
        if require_dict['output_diagnosis']:
            self.logger.info('max_index: %d'%(max_index))

        # 真實輪廓->面積
        actual_cnt = contours[max_index]
        actual_area = cv2.contourArea(actual_cnt)

        # 擬和輪廓橢圓
        ellipse = cv2.fitEllipse(actual_cnt)
        ellipse_center = (int(ellipse[0][0]), int(ellipse[0][1]))  # 橢圓圓心座標
        a_radius = ellipse[1][0] / 2  # 短軸長度
        b_radius = ellipse[1][1] / 2 # 長軸長度
        max_radius = int(np.max([a_radius, b_radius]))

        if ('ellipse_fit_rate' in require_dict['fit_ellipse_select_mode']) or not require_dict['fit_ellipse_select_mode'] or require_dict['output_diagnosis']:
            # 擬和輪廓橢圓->面積
            poly = cv2.ellipse2Poly((int(ellipse[0][0]), int(ellipse[0][1])), (int(ellipse[1][0] / 2), int(ellipse[1][1] / 2)), 0, 0, 360, 1)
            ellipse_area = cv2.contourArea(poly)

            # 橢圓均圓率
            ellipse_fit_rate = (1 - abs((actual_area - ellipse_area)/ellipse_area)) * 100

            return_dict['ellipse_fit_rate'] = ellipse_fit_rate
        
        if ('circle_fit_rate' in require_dict['fit_ellipse_select_mode']) or not require_dict['fit_ellipse_select_mode'] or require_dict['output_diagnosis']:
            # 擬和輪廓橢圓"最大軸"輪廓->面積 (等同於最大內接圓)
            ellipse_circle_area = self.get_contourarea(ellipse_center, max_radius)

            # 正圓均圓率
            circle_fit_rate = (1 - abs((actual_area - ellipse_circle_area)/ellipse_circle_area)) * 100

            return_dict['circle_fit_rate'] = circle_fit_rate
        
        if ('idealcircle_fit_rate' in require_dict['fit_ellipse_select_mode']) or not require_dict['fit_ellipse_select_mode'] or require_dict['output_diagnosis']:
            # 擬和輪廓橢圓圓心+理想直徑畫圖輪廓->面積
            circle_area = self.get_contourarea(ellipse_center, target_radius)

            # 實圓率
            idealcircle_fit_rate = (1 - abs((actual_area - circle_area)/circle_area)) * 100

            return_dict['idealcircle_fit_rate'] = idealcircle_fit_rate
        
        if require_dict['output_diagnosis']:
            self.logger.info(f'actual area(實際圓面積):{actual_area:.2f}')
            self.logger.info(f'ellipse area(擬和橢圓面積):{ellipse_area:.2f}')
            self.logger.info(f'circle area(擬和正圓面積):{ellipse_circle_area:.2f}')
            self.logger.info(f'ideal circle area(理想製程正圓面積):{circle_area:.2f}')
            self.logger.info(f'ellipse rate(橢圓均圓率):{ellipse_fit_rate:.2f}%')
            self.logger.info(f'circle rate(正圓均圓率):{circle_fit_rate:.2f}%')
            self.logger.info(f'ideal circle rate(實圓率):{idealcircle_fit_rate:.2f}%')
            if len(contours) > 5:
                contours_path = self.main_task_folder_path / ('%s-contours'%job_id)
                contours_path.mkdir(parents=True, exist_ok=True)
                self.draw_contour(contours_path, job_id, frame, contours)
                self.logger.info('multiple contour image is save.')
            else:
                self.draw_contour(self.main_task_folder_path, job_id, frame, contours)
                self.logger.info('contour image is save.')
            self.logger.info('contours have(輪廓數量): %d'%(len(contours)))
            self.logger.info('center coordinates(擬和橢圓中心座標): %d, %d'%(ellipse_center[0], ellipse_center[1]))
            self.logger.info(f'radius(擬和橢圓長短軸長): a radius:{a_radius:.2f}, b radius:{b_radius:.2f}')
            self.logger.info(f'ideal radius(理想製程正圓軸長):{target_radius:.2f}')
            self.draw_ellipse(frame, job_id, ellipse)
            self.logger.info('fitCircle image is save.')
            self.draw_ellipse_fitrate(frame, job_id, ellipse, contours, max_index, f'E:{ellipse_fit_rate:.2f}%')
            self.draw_circle_fitrate('circle', frame, job_id, ellipse_center, max_radius, contours, max_index, f'C:{circle_fit_rate:.2f}%')
            self.draw_circle_fitrate('idealcircle', frame, job_id, ellipse_center, target_radius, contours, max_index, f'iC:{idealcircle_fit_rate:.2f}%')
        return return_dict

    def get_maskimg(self, mask_dict, task_id):
        source_img = mask_dict['source_img_np']
        x1,y1,x2,y2 = mask_dict['x1y1x2y2'] 
        crop_frame = source_img[int(y1):int(y2), int(x1):int(x2)] if not mask_dict['crop_offset'] else source_img[int(y1)+mask_dict['crop_offset'][0]:int(y2)+mask_dict['crop_offset'][1], int(x1)+mask_dict['crop_offset'][2]:int(x2)+mask_dict['crop_offset'][3]]
        hsv = cv2.cvtColor(crop_frame,cv2.COLOR_BGR2HSV)
        l_h, l_s, l_v = mask_dict['low_hsv']
        u_h, u_s, u_v = mask_dict['up_hsv']
        l_g = np.array([l_h, l_s, l_v])
        u_g = np.array([u_h,u_s,u_v])
        crop_mask = cv2.inRange(hsv,l_g,u_g)
        if mask_dict['output_diagnosis']:
            self.logger.info('HSV: {low:%s, up:%s}'%(','.join([str(i) for i in mask_dict['low_hsv']]), ','.join([str(i) for i in mask_dict['up_hsv']])))
            cropimg_path = self.main_task_folder_path / ('%s-crop.png'%task_id)
            cv2.imwrite(str(cropimg_path), crop_frame)
            self.logger.info('crop image is save.')
            maskimg_path = self.main_task_folder_path / ('%s-mask.png'%task_id)
            cv2.imwrite(str(maskimg_path), crop_mask)
            self.logger.info('mask image is save.')
        return crop_mask, crop_frame

    def get_otsu(self, mask_dict, task_id):
        source_img = mask_dict['source_img_np']
        x1,y1,x2,y2 = mask_dict['x1y1x2y2'] 
        crop_frame = source_img[int(y1):int(y2), int(x1):int(x2)] if not mask_dict['crop_offset'] else source_img[int(y1)+mask_dict['crop_offset'][0]:int(y2)+mask_dict['crop_offset'][1], int(x1)+mask_dict['crop_offset'][2]:int(x2)+mask_dict['crop_offset'][3]]
        image_gray = cv2.cvtColor(crop_frame, cv2.COLOR_BGR2GRAY) 
        ret, bin = cv2.threshold(image_gray, 0 ,255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)  #Otsu方法
        bin = cv2.bitwise_not(bin) # 黑白反轉
        if mask_dict['output_diagnosis']:
            cropimg_path = self.main_task_folder_path / ('%s-crop.png'%task_id)
            cv2.imwrite(str(cropimg_path), crop_frame)
            self.logger.info('crop image is save.')
            otsu_path = self.main_task_folder_path / ('%s-otsu.png'%task_id)
            cv2.imwrite(str(otsu_path), bin)
            self.logger.info('otsu image is save.')
        return bin, crop_frame
    
    def draw_contour(self, cntimage_save_mainpath, jobfile_name, target_image, cnts):
        for ind in range(len(cnts)): 
            copy_frame = target_image.copy()
            cv2.drawContours(copy_frame, cnts, ind, (0,0,255), 1)
            cnt_img_path = cntimage_save_mainpath / ('%s_cnt%d.png'%(jobfile_name, ind))
            cv2.imwrite(str(cnt_img_path), copy_frame)
    
    def draw_circle(self, circle_type, target_image, jobfile_name, circle_center, circle_radius):
        copy_frame = target_image.copy()
        cv2.circle(copy_frame, circle_center, circle_radius, (0, 255, 0), 1)
        circle_img_path = self.main_task_folder_path / ('%s_%s.png'%(jobfile_name, circle_type))
        cv2.imwrite(str(circle_img_path), copy_frame)
    
    def get_contourarea(self, circle_center, circle_radius):
        max_dis = int((circle_radius*2) + (circle_radius*0.2)) # 直徑+2成buffer
        frame_background = np.full((max_dis, max_dis, 3), 255, np.uint8) # 創建裁切背景
        cv2.circle(frame_background, (int(max_dis/2), int(max_dis/2)), circle_radius, (255, 0, 0), -1)
        frame_gray = cv2.cvtColor(frame_background, cv2.COLOR_BGR2GRAY)
        frame_gray_ret, frame_gray_img_bin = cv2.threshold(frame_gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU) # 二值化
        circle_contours, hierarchy = cv2.findContours(frame_gray_img_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        self.logger.info('circle contours: %d'%(len(circle_contours)))
        if len(circle_contours) > 1:
            circle_area = cv2.contourArea(circle_contours[1])
        else:
            circle_area = cv2.contourArea(circle_contours[0])
        return circle_area

    def draw_ellipse(self, target_image, jobfile_name, ellipse_para):
        copy_frame = target_image.copy()
        cv2.ellipse(copy_frame, ellipse_para, (0, 255, 0), 1)
        ellipse_img_path = self.main_task_folder_path / ('%s_fitCircle.png'%jobfile_name)
        eli_center = (int(ellipse_para[0][0]), int(ellipse_para[0][1]))
        cv2.circle(copy_frame, eli_center, 1, (0, 255, 0), 1)
        cv2.imwrite(str(ellipse_img_path), copy_frame)
    
    def draw_roundness(self, target_image, jobfile_name, circle_center1, circle_radius1, circle_center2, circle_radius2, txt_info):
        copy_frame = target_image.copy()
        cv2.circle(copy_frame, circle_center1, circle_radius1, (0, 255, 0), 1)
        cv2.circle(copy_frame, circle_center2, circle_radius2, (0, 0, 255), 1)
        cv2.putText(copy_frame, txt_info, (0, 10), cv2.FONT_HERSHEY_PLAIN,1, (255, 0, 0), 1, cv2.LINE_AA)
        img_path = self.main_task_folder_path / ('%s_roundness.png'%jobfile_name)
        cv2.imwrite(str(img_path), copy_frame)
    
    def draw_ellipse_fitrate(self, target_image, jobfile_name, ellipse_para, cnts, cnt_i, txt_info):
        copy_frame = target_image.copy()
        cv2.ellipse(copy_frame, ellipse_para, (0, 255, 0), 1)
        eli_center = (int(ellipse_para[0][0]), int(ellipse_para[0][1]))
        cv2.circle(copy_frame, eli_center, 1, (0, 255, 0), 1)
        cv2.drawContours(copy_frame, cnts, cnt_i, (0, 0, 255), 1)
        cv2.putText(copy_frame, txt_info, (0, 10), cv2.FONT_HERSHEY_PLAIN,1, (255, 0, 0), 1, cv2.LINE_AA)
        img_path = self.main_task_folder_path / ('%s_ellipsefitrate.png'%jobfile_name)
        cv2.imwrite(str(img_path), copy_frame)
    
    def draw_circle_fitrate(self, jobfile_type, target_image, jobfile_name, c_center, c_radius, cnts, cnt_i, txt_info):
        copy_frame = target_image.copy()
        cv2.drawContours(copy_frame, cnts, cnt_i, (0, 0, 255), 1)
        cv2.circle(copy_frame, c_center, c_radius, (255, 0, 0), 1)
        cv2.putText(copy_frame, txt_info, (0, 10), cv2.FONT_HERSHEY_PLAIN,1, (255, 0, 0), 1, cv2.LINE_AA)
        img_path = self.main_task_folder_path / ('%s_%sfitrate.png'%(jobfile_type, jobfile_name))
        cv2.imwrite(str(img_path), copy_frame)

class StyleLog(object):
    def __init__(self, loglevel, logger_name, logfile=None):
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.DEBUG)
        sh = self.__stream_hander(loglevel)
        if not self.logger.handlers: 
            self.logger.addHandler(sh)
        if logfile:
            fh = self.__file_hander(logfile)
            if len(self.logger.handlers) == 1:
                self.logger.addHandler(fh)

    def __file_hander(self, logfile):
        fh = logging.FileHandler(logfile , 'a', 'utf-8')
        fh.setLevel(logging.DEBUG)  
        formatterf = logging.Formatter('%(asctime)s [%(name)s] %(message)s')
        fh.setFormatter(formatterf)
        return fh

    def __stream_hander(self, loglevel):
        sh = logging.StreamHandler()
        sh.setLevel(loglevel)
        formatter = colorlog.ColoredFormatter(
            '%(log_color)s[%(asctime)s] %(message)s %(reset)s',
            datefmt="%Y-%m-%d %H:%M:%S",
            reset=True,
            log_colors={
                'DEBUG':    'cyan',
                'INFO':     'green',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'red,bg_white',
            },
            secondary_log_colors={},
            style='%'
        )
        sh.setFormatter(formatter)
        return sh

    def critical(self, msg):
        self.logger.critical(msg)

    def error(self, msg):
        self.logger.error(msg)

    def warning(self, msg):
        self.logger.warning(msg)

    def info(self, msg):
        self.logger.info(msg)

    def debug(self, msg):
        self.logger.debug(msg)

if __name__=="__main__":
    import json
    roundness = main()
    img_path = Path('C:/Users/ETCH/Desktop/val-clean')
    with open('C:/Users/ETCH/yolov5/datasets/val-v5clean.json', 'r', encoding='utf-8') as openfile:
        json_object = json.load(openfile)
    for i in json_object:
        roundness.logger.info('='*50)
        roundness.logger.info(f'Image ID:{i}')
        scale = int(i.split('-')[0])
        source_img_path = img_path/str('%s.png'%i)

        img_dict = {'image_id':i,
                    'source_img_np':cv2.imread(str(source_img_path)), 
                    'x1y1x2y2':json_object[i]['groundtrue_coordinates'],
                    'low_hsv':[],
                    'up_hsv':[],
                    'output_diagnosis':False,
                    'crop_offset':[5,5,5,5],
                    'camera_zoom_magnification':scale,
                    'target_distance':0.6,
                    'fit_ellipse_select_mode':[]
                    }
        dd = roundness.roundness_value(img_dict) # MZC 真圓度
        json_object[i]['roundness'] = dd
        dr = roundness.fit_ellipse(img_dict)
        ellipse_fit_rate = dr['ellipse_fit_rate']
        circle_fit_rate = dr['circle_fit_rate']
        idealcircle_fit_rate = dr['idealcircle_fit_rate']
        print(f"MZC真圓度:{dd:d}, 橢圓均圓率:{ellipse_fit_rate:.2f}, 正圓均圓率:{circle_fit_rate:.2f}, 實圓率:{idealcircle_fit_rate:.2f}")
        # json_object[i]['circularity'] = f'{dr:.2f}%'
    # with open('C:/Users/ETCH/yolov5/datasets/val-v5clean-roundness_otus.json', "w", encoding='utf-8') as outfile:
    #     outfile.write(json.dumps(json_object))
    # img_dict = {'image_id':'200-20230130143131',
    #                 'source_img_np':cv2.imread('C:/Users/ETCH/Desktop/yolov7-main/Dataset/valid/images/200-20230130143131.png'), 
    #                 'x1y1x2y2':[
    #                     253.95008,
    #                     130.92984,
    #                     441.80031999999994,
    #                     305.39976
    #                 ],
    #                 'low_hsv':[0,0,0],
    #                 'up_hsv':[255,255,65],
    #                 'output_diagnosis':True,
    #                 'crop_offset':[5,5,5,5],
    #                 'camera_zoom_scale':200,
    #                 'target_distance':0.6}

    # tr = roundness.fit_ellipse(img_dict)
    # print(tr)