# Diffuser Model version table


|  Item  | Clean<br />Diffuser | Dirty<br />Diffuser | Background | Precision/Recall | mAP.95 | Diff(clean) mean/stev | Diff(dirty) mean/stev | Train<br />Folder | Note                         |
| :------: | :-------------------: | :-------------------: | :----------: | :----------------: | :------: | :---------------------: | :---------------------: | :-----------------: | ------------------------------ |
|   v3   |         336         |         199         |     25     |  97.7% / 94.9%  |  82%  |     -0.27 / -1.22     |     -2.66 / -5.06     |        ??        | 加入Background訓練           |
|   v4   |         886         |         199         |     25     |   100% / 100%   | 96.6% |     -0.37 / 1.62     |     -3.55 / 3.43     |       exp40       | 加入乾淨x20~x190倍率         |
| v5.0s |         350         |          0          |     30     |   100% / 100%   | 98.1% |     -0.14 / 1.36     |         未測         |       exp41       | 只有乾淨的x20~x190           |
| v5.0m |         350         |          0          |     30     |   100% / 100%   | 99.3% |     -0.17 / 1.38     |         未測         |       exp42       | 只有乾淨的x20~x190           |
| v5.0l |         350         |          0          |     30     |   100% / 100%   | 97.7% |     -0.02 / 1.54     |         未測         |       exp43       | 只有乾淨的x20~x190           |
| v5.0Y7 |         350         |          0          |     30     |   100% / 100%   |  100%  |      0.2 / 1.38      |         未測         |      Y7-exp5      | 只有乾淨的x20~x190           |
| v5.1s |         350         |         436         |     60     |   100% / 100%   | 97.7% |     -0.32 / 0.96     |      0.49 / 2.66      |       exp44       | 純乾淨基礎上加上髒的x20~x250 |
| v5.1m |         350         |         436         |     60     |   100% / 100%   | 97.9% |     -0.14 / 0.96     |      0.19 / 2.57      |       exp45       | 純乾淨基礎上加上髒的x20~x250 |
| v5.1l |         350         |         436         |     60     |   100% / 99.4%   | 97.5% |     -0.70 / 1.16     |     -0.40 / 2.69     |       exp46       | 純乾淨基礎上加上髒的x20~x250 |
| v5.1Y7 |         350         |         436         |     60     |   99.4% / 100%   | 97.2% |     -0.10 / 0.85     |      0.12 / 2.59      |      Y7-exp6      | 純乾淨基礎上加上髒的x20~x250 |

# Emboss Model version table


| Item(version) | Emboss | Background | Precision/Recall | mAP.95 | mean/stev | Train<br />Folder |             Note             |
| :-------------: | :------: | :----------: | :----------------: | :------: | :---------: | :-----------------: | :----------------------------: |
|     v1.01     |  252  |     0     |    ??% / ??%    |  ??%  | ??? / ??? |        ??        | 500px & 1600px鏡頭,1代的模型 |
|     v1.02     |  628  |     0     |   100% / 100%   |  87%  | 4.66 / 16.73 |        exp48        | 3C樣本+dino lite鏡頭40~215倍  |
|     v1.03a     |  104  |     0     |   100% / 100%   |  81.2%  | 1.194 / 0.153 |        exp51        | 3C樣本+dino lite鏡頭+黑套筒,基於v1.02模型  |
|     v1.03b     |  828  |     0     |   99.3% / 98.6%   |  80.5%  | 1.190 / 0.170 |        exp52        | 全樣本+dino lite鏡頭+黑套筒,基於v1.02模型(Release)  |

# Roundness

import該套件需給magnificationRate.ini路徑(default:./magnificationRate.ini)，真圓計算方式有分4種，可根據情況選擇，主模組分2大功能

##### 1. 計算 MZC 真圓度：roundness_value('<require_dict>' :dict):

給予需求參數dict，返回「最小外接圓」與「最大內接圓」的半徑差值(pixel)。

ps 有提供「最小外接圓」與「最大內接圓」單獨計算:
「最小外接圓」：minenclosing_circle('<require_dict>' :dict)，返回半徑值(pixel)。
「最大內接圓」：maxenclosing_circle('<require_dict>' :dict)，返回半徑值(pixel)。

##### 2. 計算 均圓率：fit_ellipse('<require_dict>' :dict):

給予需求參數dict，返回「橢圓均圓率(ellipse_fit_rate)」&「正圓均圓率(circle_fit_rate)」、「實圓率(idealcircle_fit_rate)」為鍵的半徑值(pixel)字串。

ps 可根據'<require_dict>'內的fit_ellipse_select_mode鍵值選擇性輸出。

'<require_dict>'格式：

```
require_dict = {'image_id':'<str>',  如果為空會以當下時間戳為任務ID，主要是給diagnosis用
                'source_img_np':'<numpy.ndarray>', RBG格式np-array
                'x1y1x2y2':'<list>', 裁切座標4個,值可float
                'low_hsv':'<list>', HSV low, 前處理需用到HSV作為二值化要給,值的範圍0~255,不可float；如果hsv都為空會使用otsu演算
                'up_hsv':'<list>', HSV up, 前處理需用到HSV作為二值化要給,值的範圍0~255,不可float
                'output_diagnosis':'<bool>', Dbug用,True會輸出執行log和裁切目標在各計算方式下畫圖,用於分析用,建議false
                'crop_offset':'<list>',  裁切圖往四周延伸,建議5,比較好抓輪廓
                'camera_zoom_magnification':'int', 相機的倍率,如果有用到「實圓率」計算一定要給,會根據鏡頭倍率抓magnificationRate.ini內的係數
                'target_distance':'int' 理想孔徑直徑,如果有用到「實圓率」計算一定要給
                'fit_ellipse_select_mode':'<list>' 選擇均圓率輸出模式,模式有:[ellipse_fit_rate, circle_fit_rate, idealcircle_fit_rate],給空字串會輸出全部
                    }
```
