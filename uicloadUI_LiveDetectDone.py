from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QMainWindow
import sys
from PyQt5 import QtGui
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread, QTimer
import sys
import time
from pathlib import Path
import numpy as np
import cv2
import torch
import torch.backends.cudnn as cudnn
import os

# for yolo
from models.common import DetectMultiBackend
from utils.datasets import IMG_FORMATS, VID_FORMATS, LoadImages, LoadStreams, LoadWebcam
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr,
                           increment_path, non_max_suppression, print_args, scale_coords, strip_optimizer, xyxy2xywh)
from utils.plots import Annotator, colors, save_one_box
from utils.torch_utils import select_device, time_sync

Ui_MainWindow, QtBaseClass = uic.loadUiType('trytry.ui')

liveFlag = False
# yolov5
class DetThread(QThread):
    send_img = pyqtSignal(np.ndarray)
    # send_raw = pyqtSignal(np.ndarray)
    # send_statistic = pyqtSignal(dict)

    def __init__(self):
        super(DetThread, self).__init__()
        self.weights = './embosspin_v1.0.1best.pt'
        self.source = '1'    # camera setting 1
        self.conf_thres = 0.7

    @torch.no_grad()
    def run(self,
            imgsz=640,  # inference size (pixels)
            iou_thres=0.45,  # NMS IOU threshold
            max_det=1000,  # maximum detections per image
            device='',  # cuda device, i.e. 0 or 0,1,2,3 or cpu
            view_img=True,  # show results
            save_txt=False,  # save results to *.txt
            save_conf=False,  # save confidences in --save-txt labels
            save_crop=False,  # save cropped prediction boxes
            nosave=False,  # do not save images/videos
            classes=None,  # filter by class: --class 0, or --class 0 2 3
            agnostic_nms=False,  # class-agnostic NMS
            augment=False,  # augmented inference
            visualize=False,  # visualize features
            update=False,  # update all models
            project='runs/detect',  # save results to project/name
            name='exp',  # save results to project/name
            exist_ok=False,  # existing project/name ok, do not increment
            line_thickness=3,  # bounding box thickness (pixels)
            hide_labels=False,  # hide labels
            hide_conf=False,  # hide confidences
            half=False,  # use FP16 half-precision inference
            dnn=False,  # use OpenCV DNN for ONNX inference
            ):

        # Initialize
        device = select_device(device)
        half &= device.type != 'cpu'  # half precision only supported on CUDA

        # Load model
        model = DetectMultiBackend(self.weights, device=device, dnn=dnn)
        num_params = 0
        for param in model.parameters():
            num_params += param.numel()
        stride, names, pt, jit, onnx, engine = model.stride, model.names, model.pt, model.jit, model.onnx, model.engine
        imgsz = check_img_size(imgsz, s=stride)  # check image size
        names = model.module.names if hasattr(model, 'module') else model.names  # get class names
        if half:
            model.half()  # to FP16

        # Dataloader
        if self.source.isnumeric():
            view_img = check_imshow()
            cudnn.benchmark = True  # set True to speed up constant image size inference
            dataset = LoadWebcam(pipe='1', img_size=imgsz, stride=stride)
            bs = len(dataset)  # batch_size
        else:
            dataset = LoadImages(self.source, img_size=imgsz, stride=stride, auto=pt and not jit)
            bs = 1  # batch_size
        vid_path, vid_writer = [None] * bs, [None] * bs

        # Run inference
        # model.warmup(imgsz=(1, 3, *imgsz), half=half)  # warmup
        dt, seen = [0.0, 0.0, 0.0], 0
        for path, im, im0s, self.vid_cap, s in dataset:
            statistic_dic = {name: 0 for name in names}
            t1 = time_sync()
            im = torch.from_numpy(im).to(device)
            im = im.half() if half else im.float()  # uint8 to fp16/32
            im /= 255  # 0 - 255 to 0.0 - 1.0
            if len(im.shape) == 3:
                im = im[None]  # expand for batch dim
            t2 = time_sync()
            dt[0] += t2 - t1

            # Inference
            pred = model(im, augment=augment)
            t3 = time_sync()
            dt[1] += t3 - t2

            # NMS
            pred = non_max_suppression(pred, self.conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)
            dt[2] += time_sync() - t3

            for i, det in enumerate(pred):  # detections per image
                im0 = im0s.copy()
                annotator = Annotator(im0, line_width=line_thickness, example=str(names))
                if len(det):
                    det[:, :4] = scale_coords(im.shape[2:], det[:, :4], im0.shape).round()
                    for c in det[:, -1].unique():
                        n = (det[:, -1] == c).sum()  # detections per class
                        s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string

                    for *xyxy, conf, cls in reversed(det):
                        c = int(cls)  # integer class
                        statistic_dic[names[c]] += 1
                        label = None if hide_labels else (names[c] if hide_conf else f'{names[c]} {conf:.2f}')
                        annotator.box_label(xyxy, label, color=colors(c, True))


            time.sleep(1/40)
            self.send_img.emit(im0)
            # self.send_raw.emit(im0s if isinstance(im0s, np.ndarray) else im0s[0])
            # self.send_statistic.emit(statistic_dic)



class MainWindow(QtBaseClass, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.WIDTH = 640
        self.HEIGHT = 480
        self.resize(self.WIDTH, self.HEIGHT)

### << 不更新的區域 >>        
        # title bar
        self.setWindowTitle('AUO 行動AI品管')
        self.setWindowIcon(QtGui.QIcon('./qtImg/measure.png'))
        
        # logo
        logo = QtGui.QPixmap("./qtImg/measure.png")
        logo_resize = logo.scaled(60,60)
        self.label_logo.setPixmap(QtGui.QPixmap(logo_resize))

        # 下拉式選單: camera
        self.comboBox_camera.addItems(['500px', '1600px']) #self.label_iouText.setText(self.comboBox_camera.currentText())
        # 下拉式選單: model
        self.comboBox_model.addItems(['Emboss', 'Diffuser'])
        # 拉拉bar
        self.horizontalSlider_iou.valueChanged.connect(self.showHorizontalSliderValue)
        self.horizontalSlider_conf.valueChanged.connect(self.showHorizontalSliderValue)
        # 完成設置button
        self.pushButton_setParameter.clicked.connect(self.clickComboBoxSetValue)
        
### << Sub Window >>
        self.live_window = liveWindow()
        self.pushButton_live.clicked.connect(self.openlive)

### << 更新的區域 >>
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.run)
        self.timer.start(1000)
        self.total = 0
        self.timer = QTimer(self)

    def openlive(self):
        self.live_window.show()
        self.live_window.det_thread.start()
        global liveFlag
        liveFlag = True

    def run(self):
        global liveFlag
        if self.live_window.det_thread.isRunning() and not liveFlag:
            self.live_window.det_thread.terminate()
            if hasattr(self.live_window.det_thread, 'vid_cap'):
                if self.live_window.det_thread.vid_cap:
                    self.live_window.det_thread.vid_cap.release()
        self.total+=1  

    def checkDialog(self):
        msgBox = QMessageBox()
        msgBox.setWindowIcon(QtGui.QIcon('./qtImg/OKIcon2.png'))
        msgBox.setWindowTitle('提示訊息')
        msgBox.setText('設定成功 !        ')
        msgBox.setStandardButtons(QMessageBox.Ok)
        msgBoxIcon_Pixmap = QtGui.QPixmap('./qtImg/okIcon.png')
        msgBoxIcon_Pixmap = msgBoxIcon_Pixmap.scaled(38, 38, Qt.KeepAspectRatio)
        msgBox.setIconPixmap(msgBoxIcon_Pixmap)
        msgBox.setStyleSheet("QMessageBox{background-color: #E0E0E0;} "
        "QLabel{font-size: 28px;} "
        )
        reply = msgBox.exec()

    def clickComboBoxSetValue(self):
        txtPath = './qtImg/setParameter.txt'
        with open (txtPath,'w') as f:
            f.write(self.comboBox_camera.currentText() + "," + self.comboBox_model.currentText() + "," + str(self.horizontalSlider_iou.value()) + "," + str(self.horizontalSlider_conf.value()))
        f.close()
        self.checkDialog()

    def showHorizontalSliderValue(self):
        self.label_iouText.setText(str(self.horizontalSlider_iou.value()))
        self.label_confText.setText(str(self.horizontalSlider_conf.value()))

                
class liveWindow(QWidget):
    def __init__(self):
        super(liveWindow, self).__init__()
        self.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgb(70,70,70), stop:1 rgb(200,200,200));")
        self.resize(640, 480)
        self.setWindowTitle('AUO 行動AI品管')
        self.setWindowIcon(QtGui.QIcon('./qtImg/measure.png'))
    # setting logo image
        self.logo = QLabel(self)
        self.logo.setGeometry(48, 11, 70, 70)
        self.logo.setStyleSheet("border: 0px; color: rgb(255,255,255); background-color: transparent;")
        logo = QtGui.QPixmap("./qtImg/measure.png")
        logo_resize = logo.scaled(60,60)
        self.logo.setPixmap(QtGui.QPixmap(logo_resize))
    # setting title text
        self.title = QLabel('行動AI品管', self)
        self.title.setGeometry(142, 11, 487, 80)
        self.title.setStyleSheet("border: 0px; color: rgb(255,255,255); background-color: transparent; font-weight: bold;")
        self.title.setFont(QFont('微軟正黑體', 24))
    # show webcam
        self.label_result = QLabel(self)
        self.label_result.setGeometry(218, 98, 411, 346)

    # yolov5
        self.model = './embosspin_v1.0.1best.pt'
        self.det_thread = DetThread()
        self.det_thread.source = '1'
        self.det_thread.send_img.connect(lambda x: self.show_image(x, self.label_result))
        # self.cam_switch = QtWidgets.QAction()
        # self.cam_switch.triggered.connect(self.camera)

    def closeEvent(self, event):
        global liveFlag
        liveFlag = False
        event.accept()

    @staticmethod
    def show_image(img_src, label):
        try:
            ih, iw, _ = img_src.shape
            w = label.geometry().width()
            h = label.geometry().height()
            if iw > ih:
                scal = w / iw
                nw = w
                nh = int(scal * ih)
                img_src_ = cv2.resize(img_src, (nw, nh))
            else:
                scal = h / ih
                nw = int(scal * iw)
                nh = h
                img_src_ = cv2.resize(img_src, (nw, nh))
            frame = cv2.cvtColor(img_src_, cv2.COLOR_BGR2RGB)
            img = QImage(frame.data, frame.shape[1], frame.shape[0], frame.shape[2] * frame.shape[1], QImage.Format_RGB888)
            label.setPixmap(QPixmap.fromImage(img))
        except Exception as e:
            print(repr(e))


    def camera(self):
        if self.cam_switch.isChecked():
            self.det_thread.source = '1'
            print('Camera is turned on')
        # else:
        #     self.det_thread.terminate()
        #     if hasattr(self.det_thread, 'vid_cap'):
        #         self.det_thread.vid_cap.release()
        #     if self.RunProgram.isChecked():
        #         self.RunProgram.setChecked(False)
        #     print('Camera turned off')


   

if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    window = MainWindow()
    window.show()
    # subwindow = liveWindow()
    # subwindow.show()
    sys.exit(app.exec_())