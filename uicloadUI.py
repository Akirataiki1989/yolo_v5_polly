from unittest import result
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QMainWindow
import sys
from PyQt5 import QtGui
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread, QTimer
import sys
import time
from pathlib import Path
import numpy as np
import cv2
import torch
import torch.backends.cudnn as cudnn
import os

# for yolo
from models.common import DetectMultiBackend
from utils.datasets import IMG_FORMATS, VID_FORMATS, LoadImages, LoadStreams, LoadWebcam
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr,
                           increment_path, non_max_suppression, print_args, scale_coords, strip_optimizer, xyxy2xywh)
from utils.plots import Annotator, colors, save_one_box
from utils.torch_utils import select_device, time_sync

Ui_MainWindow, QtBaseClass = uic.loadUiType('trytry.ui')

# cnt = 0
liveFlag = False
# yolov5
class DetThread(QThread):
    send_img = pyqtSignal(np.ndarray)
    send_statistic = pyqtSignal(dict)

    def __init__(self):
        super(DetThread, self).__init__()
        self.weights = './embosspin_v1.0.1best.pt'
        self.source = '1'
        self.conf_thres = 0.7

    @torch.no_grad()
    def run(self,
            imgsz=640,  # inference size (pixels)
            iou_thres=0.45,  # NMS IOU threshold
            max_det=1000,  # maximum detections per image
            device='',  # cuda device, i.e. 0 or 0,1,2,3 or cpu
            view_img=True,  # show results
            save_txt=False,  # save results to *.txt
            save_conf=False,  # save confidences in --save-txt labels
            save_crop=False,  # save cropped prediction boxes
            nosave=False,  # do not save images/videos
            classes=None,  # filter by class: --class 0, or --class 0 2 3
            agnostic_nms=False,  # class-agnostic NMS
            augment=False,  # augmented inference
            visualize=False,  # visualize features
            update=False,  # update all models
            project='runs/detect',  # save results to project/name
            name='exp',  # save results to project/name
            exist_ok=False,  # existing project/name ok, do not increment
            line_thickness=3,  # bounding box thickness (pixels)
            hide_labels=False,  # hide labels
            hide_conf=False,  # hide confidences
            half=False,  # use FP16 half-precision inference
            dnn=False,  # use OpenCV DNN for ONNX inference
            ):

        # Initialize
        device = select_device(device)
        half &= device.type != 'cpu'  # half precision only supported on CUDA

        # Load model
        model = DetectMultiBackend(self.weights, device=device, dnn=dnn)
        num_params = 0
        for param in model.parameters():
            num_params += param.numel()
        stride, names, pt, jit, onnx, engine = model.stride, model.names, model.pt, model.jit, model.onnx, model.engine
        imgsz = check_img_size(imgsz, s=stride)  # check image size
        names = model.module.names if hasattr(model, 'module') else model.names  # get class names
        if half:
            model.half()  # to FP16

        # Dataloader
        if self.source.isnumeric():
            view_img = check_imshow()
            cudnn.benchmark = True  # set True to speed up constant image size inference
            dataset = LoadWebcam(pipe='1', img_size=imgsz, stride=stride)
            bs = len(dataset)  # batch_size
        else:
            dataset = LoadImages(self.source, img_size=imgsz, stride=stride, auto=pt and not jit)
            bs = 1  # batch_size
        vid_path, vid_writer = [None] * bs, [None] * bs

        # Run inference
        # model.warmup(imgsz=(1, 3, *imgsz), half=half)  # warmup
        dt, seen = [0.0, 0.0, 0.0], 0
        for path, im, im0s, self.vid_cap, s in dataset:
            statistic_dic = {name: 0 for name in names}
            t1 = time_sync()
            im = torch.from_numpy(im).to(device)
            im = im.half() if half else im.float()  # uint8 to fp16/32
            im /= 255  # 0 - 255 to 0.0 - 1.0
            if len(im.shape) == 3:
                im = im[None]  # expand for batch dim
            t2 = time_sync()
            dt[0] += t2 - t1

            # Inference
            pred = model(im, augment=augment)
            t3 = time_sync()
            dt[1] += t3 - t2

            # NMS
            pred = non_max_suppression(pred, self.conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)
            dt[2] += time_sync() - t3

            for i, det in enumerate(pred):  # detections per image
                im0 = im0s.copy()
                annotator = Annotator(im0, line_width=line_thickness, example=str(names))
                if len(det):
                    det[:, :4] = scale_coords(im.shape[2:], det[:, :4], im0.shape).round()
                    for c in det[:, -1].unique():
                        n = (det[:, -1] == c).sum()  # detections per class
                        s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string

                    for *xyxy, conf, cls in reversed(det):
                        circle_diameter_x = abs(int(xyxy[0]) - int(xyxy[2])) * 0.004726
                        circle_diameter_y = abs(int(xyxy[0]) - int(xyxy[2])) * 0.004726
                        self.avg_diameter = (circle_diameter_x+circle_diameter_y)/2
                        self.circle_area = ((circle_diameter_x/2)**2)*3.1415

                        c = int(cls)  # integer class
                        statistic_dic[names[c]] += 1
                        label = 'diameter:%.3fmm\ncircle area:%.3fmm2'%(self.avg_diameter,self.circle_area)
                        # label = None if hide_labels else (names[c] if hide_conf else f'{names[c]} conf:{conf:.2f}') + '\ndiameter:%.3fmm circle area:%.3fmm2'%(avg_diameter,circle_area)
                        annotator.box_label(xyxy, label, color=colors(c, True))

            time.sleep(1/40)
            self.send_img.emit(im0)
            self.send_statistic.emit(statistic_dic)


class Thread(QThread):
    changePixmap = pyqtSignal(QImage)
    def run(self):
        cap = cv2.VideoCapture(1)
        while True:
            ret, frame = cap.read()
            if ret:
                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h, bytesPerLine, QImage.Format_RGB888)
                p = convertToQtFormat.scaled(640, 480, Qt.KeepAspectRatio)
                self.changePixmap.emit(p)


class MainWindow(QtBaseClass, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.WIDTH = 640
        self.HEIGHT = 480
        self.resize(self.WIDTH, self.HEIGHT)

### << 不更新的區域 >>        
        # title bar
        self.setWindowTitle('AUO 行動AI品管')
        self.setWindowIcon(QtGui.QIcon('./qtImg/measure.png'))
        
        # logo
        logo = QtGui.QPixmap("./qtImg/measure.png")
        logo_resize = logo.scaled(60,60)
        self.label_logo.setPixmap(QtGui.QPixmap(logo_resize))

        # 下拉式選單: camera
        self.comboBox_camera.addItems(['500px', '1600px']) #self.label_iouText.setText(self.comboBox_camera.currentText())
        # 下拉式選單: model
        self.comboBox_model.addItems(['Emboss', 'Diffuser'])
        # 拉拉bar
        self.horizontalSlider_iou.valueChanged.connect(self.showHorizontalSliderValue)
        self.horizontalSlider_conf.valueChanged.connect(self.showHorizontalSliderValue)
        # 完成設置button
        self.pushButton_setParameter.clicked.connect(self.clickComboBoxSetValue)
        
### << Sub Window >>
        self.live_window = liveWindow()
        self.pushButton_live.clicked.connect(self.openlive)

### << Third Window >>
        self.offline_window = offlineWindow()
        self.pushButton_offline.clicked.connect(self.openOffline)

### << 更新的區域 >>
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.run)
        self.timer.start(1000)
        self.total = 0
        self.timer = QTimer(self)

    def openlive(self):
        self.live_window.show()
        self.live_window.det_thread.start()
        global liveFlag
        liveFlag = True

    def openOffline(self):
        self.offline_window.show()
        global liveFlag
        liveFlag = True

    def run(self):
        global liveFlag
        if self.live_window.det_thread.isRunning() and not liveFlag:
            self.live_window.det_thread.terminate()
            if hasattr(self.live_window.det_thread, 'vid_cap'):
                if self.live_window.det_thread.vid_cap:
                    self.live_window.det_thread.vid_cap.release()
        self.total+=1  

    def checkDialog(self):
        msgBox = QMessageBox()
        msgBox.setWindowIcon(QtGui.QIcon('./qtImg/OKIcon2.png'))
        msgBox.setWindowTitle('提示訊息')
        msgBox.setText('設定成功 !        ')
        msgBox.setStandardButtons(QMessageBox.Ok)
        msgBoxIcon_Pixmap = QtGui.QPixmap('./qtImg/okIcon.png')
        msgBoxIcon_Pixmap = msgBoxIcon_Pixmap.scaled(38, 38, Qt.KeepAspectRatio)
        msgBox.setIconPixmap(msgBoxIcon_Pixmap)
        msgBox.setStyleSheet("QMessageBox{background-color: #E0E0E0;} "
        "QLabel{font-size: 28px;} "
        )
        reply = msgBox.exec()

    def clickComboBoxSetValue(self):
        txtPath = './qtImg/setParameter.txt'
        with open (txtPath,'w') as f:
            f.write(self.comboBox_camera.currentText() + "," + self.comboBox_model.currentText() + "," + str(self.horizontalSlider_iou.value()) + "," + str(self.horizontalSlider_conf.value()))
        f.close()
        self.checkDialog()

    def showHorizontalSliderValue(self):
        self.label_iouText.setText(str(self.horizontalSlider_iou.value()))
        self.label_confText.setText(str(self.horizontalSlider_conf.value()))

                
class liveWindow(QWidget):
    def __init__(self):
        super(liveWindow, self).__init__()
        self.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgb(70,70,70), stop:1 rgb(200,200,200));")
        self.resize(640, 480)
        self.setWindowTitle('AUO 行動AI品管')
        self.setWindowIcon(QtGui.QIcon('./qtImg/measure.png'))
    # setting logo image
        self.logo = QLabel(self)
        self.logo.setGeometry(48, 11, 70, 70)
        self.logo.setStyleSheet("border: 0px; color: rgb(255,255,255); background-color: transparent;")
        logo = QtGui.QPixmap("./qtImg/measure.png")
        logo_resize = logo.scaled(60,60)
        self.logo.setPixmap(QtGui.QPixmap(logo_resize))
    # setting title text
        self.title = QLabel('行動AI品管', self)
        self.title.setGeometry(142, 11, 487, 80)
        self.title.setStyleSheet("border: 0px; color: rgb(255,255,255); background-color: transparent; font-weight: bold;")
        self.title.setFont(QFont('微軟正黑體', 24))
    # show webcam
        self.label_result = QLabel(' Load Model...', self)
        self.label_result.setGeometry(218, 98, 411, 372)
        self.label_result.setStyleSheet("color: rgb(255,255,255); font-weight: bold")
        self.label_result.setFont(QFont('Gill Sans MT', 20))

    # show detect info
        self.label_info = QLabel(self)
        self.label_info.setGeometry(11, 98, 200, 372)
        self.label_info.setStyleSheet("color: rgb(115,115,115); background-color:rgb(160,160,160)")
        # live status   
        self.label_liveStatus = QLabel('Live', self)
        self.label_liveStatus.setGeometry(20, 110, 121, 41)
        self.label_liveStatus.setStyleSheet("background-color:rgb(120,120,120);color:rgb(255,242,0);border-style: outset;border-width: 3px; font-weight: bold; border-radius: 45px;\
                                            border-top-color: rgb(60,60,60);border-right-color: rgb(40,40,40);border-left-color: rgb(60,60,60);border-bottom-color: rgb(40,40,40);")
        self.label_liveStatus.setFont(QFont('Gill Sans MT', 18))
        # text 1
        self.label_showDetect = QLabel('偵測結果', self)
        self.label_showDetect.setGeometry(18, 160, 187, 24) #18, 107, 187, 24
        self.label_showDetect.setStyleSheet("border: 0px; color: rgb(255,255,255); background-color: transparent; font-weight: bold")
        self.label_showDetect.setFont(QFont('微軟正黑體', 12))
        # result widget
        self.widget_showDetectResult = QtWidgets.QListWidget(self)
        self.widget_showDetectResult.setGeometry(20, 190, 181, 161)
        self.widget_showDetectResult.setStyleSheet("border: 2px; border-color: rgb(94,94,94); color: rgb(255,255,255); background-color: transparent")
        self.widget_showDetectResult.setFont(QFont('Gill Sans MT', 9))

    # yolov5
        self.model = './embosspin_v1.0.1best.pt'
        self.det_thread = DetThread()
        self.det_thread.source = '1'
        self.det_thread.send_img.connect(lambda x: self.show_image(x, self.label_result))
        self.det_thread.send_statistic.connect(self.show_statistic)

    def closeEvent(self, event):
        global liveFlag
        liveFlag = False
        event.accept()

    @staticmethod
    def show_image(img_src, label):
        try:
            ih, iw, _ = img_src.shape
            w = label.geometry().width()
            h = label.geometry().height()
            if iw > ih:
                scal = w / iw
                nw = w
                nh = int(scal * ih)
                img_src_ = cv2.resize(img_src, (nw, nh))
            else:
                scal = h / ih
                nw = int(scal * iw)
                nh = h
                img_src_ = cv2.resize(img_src, (nw, nh))
            frame = cv2.cvtColor(img_src_, cv2.COLOR_BGR2RGB)
            img = QImage(frame.data, frame.shape[1], frame.shape[0], frame.shape[2] * frame.shape[1], QImage.Format_RGB888)
            label.setPixmap(QPixmap.fromImage(img))
        except Exception as e:
            print(repr(e))

    def show_statistic(self, statistic_dic):
        try:
            self.widget_showDetectResult.clear()
            statistic_dic = sorted(statistic_dic.items(), key=lambda x: x[1], reverse=True)
            statistic_dic = [i for i in statistic_dic if i[1] > 0]
            results = [str(i[0]) + '：' + str(i[1]) for i in statistic_dic]
            # detect_result = [self.avg_diameter + ", " + self.circle_area]
            self.widget_showDetectResult.addItems(results)
            print(results)
        except Exception as e:
            print(repr(e))


class offlineWindow(QWidget):
    def __init__(self):
        super(offlineWindow, self).__init__()
        self.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgb(70,70,70), stop:1 rgb(200,200,200));")
        self.resize(640, 480)
        self.setWindowTitle('AUO 行動AI品管')
        self.setWindowIcon(QtGui.QIcon('./qtImg/measure.png'))
    # setting logo image
        self.logo = QLabel(self)
        self.logo.setGeometry(48, 11, 70, 70)
        self.logo.setStyleSheet("border: 0px; color: rgb(255,255,255); background-color: transparent;")
        logo = QtGui.QPixmap("./qtImg/measure.png")
        logo_resize = logo.scaled(60,60)
        self.logo.setPixmap(QtGui.QPixmap(logo_resize))
    # setting title text
        self.title = QLabel('行動AI品管', self)
        self.title.setGeometry(142, 11, 487, 80)
        self.title.setStyleSheet("border: 0px; color: rgb(255,255,255); background-color: transparent; font-weight: bold;")
        self.title.setFont(QFont('微軟正黑體', 24))
    # show detect info
        self.label_info = QLabel(self)
        self.label_info.setGeometry(11, 98, 200, 372)
        self.label_info.setStyleSheet("color: rgb(115,115,115); background-color:rgb(160,160,160)")
    # live status   
        self.label_liveStatus = QLabel('Offline', self)
        self.label_liveStatus.setGeometry(20, 110, 121, 41)
        self.label_liveStatus.setStyleSheet("background-color:rgb(120,120,120);color:rgb(255,242,0);border-style: outset;border-width: 3px; font-weight: bold; border-radius: 45px;\
                                            border-top-color: rgb(60,60,60);border-right-color: rgb(40,40,40);border-left-color: rgb(60,60,60);border-bottom-color: rgb(40,40,40);")
        self.label_liveStatus.setFont(QFont('Gill Sans MT', 18))
    # open camera button
        self.btn_takePic = QPushButton('Take Picture',self)
        self.btn_takePic.setGeometry(40, 180, 131, 41)
        self.btn_takePic.setStyleSheet("background-color:rgb(120,120,120);color:rgb(255,255,255);border-style: outset;border-width: 2px; border-radius:5px;")
        self.btn_takePic.setFont(QFont('Gill Sans MT', 12))
    # load folder image
        self.btn_loadImg = QPushButton('Load Image',self)
        self.btn_loadImg.setGeometry(40, 240, 131, 41)
        self.btn_loadImg.setStyleSheet("background-color:rgb(120,120,120);color:rgb(255,255,255);border-style: outset;border-width: 2px; border-radius:5px;")
        self.btn_loadImg.setFont(QFont('Gill Sans MT', 12))
    # load folder image
        self.btn_predict = QPushButton('Predict',self)
        self.btn_predict.setGeometry(40, 300, 131, 41)
        self.btn_predict.setStyleSheet("background-color:rgb(120,120,120);color:rgb(255,255,255);border-style: outset;border-width: 2px; border-radius:5px;")
        self.btn_predict.setFont(QFont('Gill Sans MT', 12))
    # calculate picture num
        self.label_calPic = QLabel('已拍攝張數:', self)
        self.label_calPic.setGeometry(20, 370, 171, 21)
        self.label_calPic.setStyleSheet("border: 0px; color: rgb(255,255,255); background-color: transparent; font-weight: bold")
        self.label_calPic.setFont(QFont('微軟正黑體', 12))

        # self.model = './embosspin_v1.0.1best.pt'
        # self.pic_thread = Thread()
        # self.pic_thread.source = '1'

    # show webcam
        self.label_result = QLabel(' Load Camera...', self)
        self.label_result.setGeometry(218, 98, 411, 372)
        self.label_result.setStyleSheet("color: rgb(255,255,255); font-weight: bold")
        self.label_result.setFont(QFont('Gill Sans MT', 20))
        th = Thread(self)
        th.changePixmap.connect(self.setImage)
        th.start()
        self.label_result.show()
        

    @pyqtSlot(QImage)
    def setImage(self, image):
        self.label_result.setPixmap(QPixmap.fromImage(image))
        # if self.btn_takePic.clicked == True:
        #     cv2.imwrite("./qtImg/sa"+cnt+".jpg",self.label_result)

if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())