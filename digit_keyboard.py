from PyQt5.QtWidgets import QWidget,QPushButton,QLineEdit,QGridLayout,QMainWindow, QComboBox, QMessageBox
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt,QSize,pyqtSignal

class keyboard_MainWindow(QMainWindow): 
    def __init__(self, current_pixel=0, window_title='', parent=None):
        super().__init__(parent)
        self.current_pixel = current_pixel

        self.setGeometry(300,300,250,250) 
        self.setWindowTitle(window_title)
        self.app_widget = keyboard_App(parent=self, current_pixel=self.current_pixel)
        self.setCentralWidget(self.app_widget)

class keyboard_App(QWidget):
    input_values = pyqtSignal(float)
    input_unit = pyqtSignal(str)
    def __init__(self, current_pixel=0, parent=None):
        super().__init__(parent)
        self.current_pixel = current_pixel
        self.initUI()
        self.first_time = True
        self.line_unit = ''
        
    def initUI(self):
        
        gridLayout = QGridLayout()
        self.display = QLineEdit(str(self.current_pixel))
        self.display.setFixedSize(QSize(230,40))
        self.display.setReadOnly(True)
        self.display.setAlignment(Qt.AlignRight)
        self.display.setFont(QFont("微軟正黑體",14,QFont.Bold))
        gridLayout.addWidget(self.display,0,0,1,3)

        self.unit_combobox = QComboBox()
        self.unit_combobox.addItems([' ', 'cm', 'mm', 'um'])
        self.unit_combobox.currentIndexChanged.connect(self.onComboBoxChanged)

        gridLayout.addWidget(self.unit_combobox,0,3,1,1)

        self.showNum = str(self.current_pixel)

        keys = ['7', '8', '9', 'Clear',
                '4', '5', '6', 'Del',
                '1', '2', '3', 'Enter',
                '0', '', '.', '']
        position = [(0, 0), (0, 1), (0, 2), (0, 3),
                    (1, 0), (1, 1), (1, 2), (1, 3),
                    (2, 0), (2, 1), (2, 2), (2, 2),
                    (3, 0), (3, 1), (3, 2), (3, 3)]
        for item in range(len(keys)):
            btn = QPushButton(keys[item])
            btn.setFixedSize(QSize(60, 40))
            btn.setFont(QFont("微軟雅黑",12,QFont.Bold))
            btn.clicked.connect(self.btnClicked)
            if keys[item] == "Enter":
                gridLayout.addWidget(btn, 3, 3, 2, 1) # 左侧部件在第3行第4列，占2行1列
                btn.setFixedSize(QSize(60, 90))
            elif keys[item] == "0":
                gridLayout.addWidget(btn, 4, 0, 1, 2)
                btn.setFixedSize(QSize(142, 40))
            elif keys[item] == "":
                continue
            else:
                gridLayout.addWidget(btn, position[item][0]+1, position[item][1], 1, 1)
        self.setLayout(gridLayout)

    def btnClicked(self):
        sender = self.sender()
        symbols = ["Clear","Del","Enter"]
        if sender.text() not in symbols:
            if self.first_time:
                self.showNum = ''
                self.first_time = False
            self.showNum += sender.text()
            self.display.setText(self.showNum)
        elif sender.text() == "Clear":
            self.display.setText("0")
            self.showNum = ""
        elif sender.text() == "Del":
            self.showNum = self.showNum[:-1]
            self.display.setText(self.showNum)
        elif sender.text() == "Enter":
            if self.line_unit == '':
                QMessageBox.warning(None, 'Unit empty warning.', 'Please select real line unit.',QMessageBox.Ok ,QMessageBox.Close)
            else:
                self.input_values.emit(float(self.showNum))
                self.input_unit.emit(self.line_unit)
                self.parent().close()

    def onComboBoxChanged(self):
        self.line_unit = self.unit_combobox.currentText()
