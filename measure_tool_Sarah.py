import os, sys, cv2, math
import pathlib
import configparser
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import utils.stylelogging as log

import qdarkstyle

main_set_file = open('captureAndSave/setParameter-n.txt', "r") # 500px,Diffuser,0.72,0.52
main_setting = main_set_file.read()

if main_setting: 
    # calibration_scale, calibration_scale_unit = main_setting.split(',')[-2:]
    # calibration_scale = float(calibration_scale)
    # calibration_scale_unit = calibration_scale_unit.strip()
    # 調整OM倍率
    config = configparser.ConfigParser()
    config.read(os.path.join(os.getcwd(),'magnificationRate.ini'))
    with open(os.path.join(os.getcwd(),'captureAndSave/setParameter.txt'), 'r') as f:
        data = f.read()
        magnification = data.split(',')[0] 
        magnificationx0 = magnification + '_X'
        magnificationy0 = magnification + '_Y'
        magnificationX = float(config.get('OM',magnificationx0))
        magnificationY = float(config.get('OM',magnificationy0))
        print(">>>MX:",magnificationX)
        print(">>>MY:",magnificationY)
    calibration_scale = (magnificationX + magnificationY)/2
    calibration_scale_unit = 'mm'
else:
    config = configparser.ConfigParser()
    config.read("measure_tool_config.ini",encoding='utf-8')

    calibration_scale = float(config.get('Calibration','value'))
    calibration_scale_unit = config.get('Calibration','unit')

    



    
class MainWindow(QMainWindow): 
    def __init__(self, parent=None):
        super().__init__(parent)
        
        # creating EmailBlast widget and setting it as central
        self.setWindowTitle("Edge Device Manual Messure Tool.")
        self.resize(1024, 580)
        # self.setWindowFlag(Qt.FramelessWindowHint) # 隱藏window title
        self.app_widget = App(parent=self)
        self.setCentralWidget(self.app_widget)

        self.logger = log.StyleLog(loglevel='DEBUG', logger_name='Messure-Tool', logfile='./log/log.log')
        self.show()
        
class paint_Label(QWidget):
    restore_enable_status = pyqtSignal(bool)
    previous_enable_status = pyqtSignal(bool)
    savefile_enable_status = pyqtSignal(bool)
    currentinfo_save_status = pyqtSignal(bool)
    def __init__(self, image_lines_info, ratio_value, _current_index=0, parent=None):
        super().__init__()

        self.startPoint = None
        self.endPoint = None
        self.line2endpoint = None
        self.line2startpoint = None
        self.drawline1event = True
        self.drawline2event = False

        self.zoom_info_dict = {'Zoom ratio':'','Image shape':'','current shape':'','clicked pos':'','Nor pos':'','Real pos':''}
        self.point2point_fun = False
        self.boundingbox_fun = False
        self.angle_fun = False
        self.save_biundingbox = False # 裁切image
        self.ratio_rate = 1
        self._current_index = _current_index
        self.image_lines_info = image_lines_info
        self.ratio_value = ratio_value
        self.current_index = 0

    def mousePressEvent(self, event):
        if (event.button() == Qt.LeftButton) and (self.point2point_fun or self.boundingbox_fun):
            self.startPoint = event.pos()
        if (event.button() == Qt.LeftButton) and self.angle_fun:
            if self.drawline1event and not self.drawline2event:
                self.startPoint = event.pos()
            if not self.drawline1event  and self.drawline2event:
                self.drawline1event = True

        # print("Press startPoint",self.startPoint)
        # print("Press endPoint",self.endPoint)
        # print("Press start2endpoint",self.line2startpoint)
        # print("Press line2endpoint",self.line2endpoint)
            
    def mouseMoveEvent(self, event):
        # Sarah add, 0118
        if self.boundingbox_fun:
            self.endPoint = event.pos() 
            self.update() 
        #
        
        if self.point2point_fun: # Sarah moidify, 0118 if self.point2point_fun or self.boundingbox_fun:
            if self.startPoint:
                self.endPoint = event.pos() 
                self.update() 
        if self.angle_fun:
            if self.drawline1event:
                self.endPoint = event.pos() 
                self.update() 
            if self.drawline2event:
                self.line2endpoint = event.pos() 
                self.update() 
            
        # if self.angle_fun:
        #     print("self.line1event",self.line1event)
        #     if self.line1event:
        #         self.endPoint = event.pos()
        #         self.update()                                
        #     else:
        #         self.line2endpoint = event.pos()
        #         self.update()
        # print("Move startPoint",self.startPoint)
        # print("Move endPoint",self.endPoint)
        # print("Move start2endpoint",self.line2startpoint)
        # print("Move line2endpoint",self.line2endpoint)
            
    def mouseReleaseEvent(self, event):
        global calibration_scale
        
        if self.point2point_fun:
            if self.startPoint and self.endPoint:
                line = QLineF(self.startPoint, self.endPoint) 
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'point2point',
                    'points': line, 
                    'distance': round(line.length(), 2)/self.ratio_rate * calibration_scale, 
                    'zoom_ratio': self.ratio_rate,
                    'startPoint_nor_x': line.x1()/self.qpixmap.width()/self.ratio_rate,
                    'startPoint_nor_y': line.y1()/self.qpixmap.height()/self.ratio_rate,
                    'endPoint_nor_x': line.x2()/self.qpixmap.width()/self.ratio_rate,
                    'endPoint_nor_y': line.y2()/self.qpixmap.height()/self.ratio_rate
                })
                self.update()
            self.startPoint = self.endPoint = None
            self.currentinfo_save_status.emit(False)
        elif self.boundingbox_fun:
            if self.startPoint and self.endPoint:
                box = QRect(self.startPoint, self.endPoint) 
                box_width = box.width()#/self.qpixmap.width()/self.ratio_rate
                box_height = box.height()#/self.qpixmap.width()/self.ratio_rate
                if (box_width > 0) or (box_height > 0): # 任一邊等於零不畫出來
                    self.image_lines_info[self._current_index]["paints"].append({
                        'func_type':'boundingbox',
                        'distance_width': box.width()/self.ratio_rate* calibration_scale, 
                        'distance_height': box.height()/self.ratio_rate* calibration_scale, 
                        'zoom_ratio': self.ratio_rate,
                        'startPoint_nor_x': box.topLeft().x()/self.qpixmap.width()/self.ratio_rate,
                        'startPoint_nor_y': box.topLeft().y()/self.qpixmap.height()/self.ratio_rate,
                        'nor_width': box_width/self.qpixmap.width()/self.ratio_rate,
                        'nor_height': box_height/self.qpixmap.height()/self.ratio_rate
                    })
                    if self.save_biundingbox:
                        x = int(box.topLeft().x()/self.ratio_rate)
                        y = int(box.topLeft().y()/self.ratio_rate)
                        w = abs(int(box_width/self.ratio_rate))
                        h = abs(int(box_height/self.ratio_rate))
                        self.crop_img_and_save(x,y,w,h)
                    self.update()
            self.startPoint = self.endPoint = None
            self.currentinfo_save_status.emit(False)
        elif self.angle_fun:
            if self.startPoint and self.endPoint:
                line = QLineF(self.startPoint, self.endPoint)
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'angle',
                    'line1': line, 
                    'line2': '', 
                    'angle_degree':'',
                    'zoom_ratio': self.ratio_rate,
                    'startPoint_nor_x': line.x1()/self.qpixmap.width()/self.ratio_rate,
                    'startPoint_nor_y': line.y1()/self.qpixmap.height()/self.ratio_rate,
                    'endPoint_nor_x': line.x2()/self.qpixmap.width()/self.ratio_rate,
                    'endPoint_nor_y': line.y2()/self.qpixmap.height()/self.ratio_rate,
                    'line2endPoint_nor_x': '',
                    'line2endPoint_nor_y': '' 
                })
                self.line2startpoint = self.startPoint
                self.line2endpoint = self.endPoint
                self.drawline1event = False
                self.drawline2event = True
                self.startPoint = self.endPoint = None
                self.setMouseTracking(True) 
                self.update()
            elif self.line2startpoint and self.line2endpoint: 
                line = QLineF(self.line2startpoint, self.line2endpoint)
                self.image_lines_info[self._current_index]["paints"][-1]['line2'] = line
                self.image_lines_info[self._current_index]["paints"][-1]['line2endPoint_nor_x'] = line.x2()/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['line2endPoint_nor_y'] = line.y2()/self.qpixmap.height()/self.ratio_rate
                self.line2startpoint = self.line2endpoint = None
                self.drawline2event = False
                self.setMouseTracking(False) 
                self.currentinfo_save_status.emit(False)
                self.update()
        # print(self.image_lines_info[self._current_index]["paints"])
        # print("Release startPoint",self.startPoint)
        # print("Release endPoint",self.endPoint)
        # print("Release start2endpoint",self.line2startpoint)
        # print("Release line2endpoint",self.line2endpoint)
        # print("line1event", self.drawline1event)
        # print("line2event", self.drawline2event)
        
        
        # print("box.topLeft()",box.topLeft())
        # print("box.topRight()",box.topRight())
        # print("box.bottomLeft()",box.bottomLeft())
        # print("box.bottomRight()",box.bottomRight())
        # print(self.image_lines_info[self._current_index])
        # print('w',self.image_lines_info[self._current_index]["paints"][0]['nor_width']*self.qpixmap.width())
        # print('h',self.image_lines_info[self._current_index]["paints"][0]['nor_height']*self.qpixmap.height())
    
     
    def paintEvent(self, event): # event.type()=12 :screen update necessary(QPaintEvent)
        global calibration_scale_unit
        
        painter = QPainter(self)
        painter.scale(self.ratio_rate, self.ratio_rate)
        painter.setRenderHint(painter.Antialiasing) # 抗鋸齒(狗牙)

        # dirtyRect = event.rect()
        # print("dirtyRect",dirtyRect)
        painter.drawPixmap(self.rect(), self.qpixmap, self.rect())
        
        drawpainter = QPainter(self.image)
        if self.point2point_fun:
            linePen = QPen(Qt.red, 3, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
            if self.startPoint and self.endPoint: # 拉線時預覽線條
                painter.drawLine(self.startPoint/self.ratio_rate, self.endPoint/self.ratio_rate)
                self.update()
        elif self.boundingbox_fun:
            linePen = QPen(Qt.red, 1, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
            # Sarah add, 0118
            if self.endPoint:
                painter.drawLine(QPoint(painter.window().left(), self.endPoint.y())/self.ratio_rate, QPoint(painter.window().right(), self.endPoint.y())/self.ratio_rate)
                painter.drawLine(QPoint(self.endPoint.x(), painter.window().top())/self.ratio_rate, QPoint(self.endPoint.x(), painter.window().bottom())/self.ratio_rate)
            # 

            if self.startPoint and self.endPoint: 
                painter.drawRect(QRect(self.startPoint/self.ratio_rate, self.endPoint/self.ratio_rate).normalized())
                self.update()
        elif self.angle_fun:
            linePen = QPen(Qt.red, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
            painter.setPen(linePen)
            if self.drawline1event and self.endPoint:
                painter.drawLine(self.startPoint/self.ratio_rate, self.endPoint/self.ratio_rate) # 底邊Line1
                self.angle_line1 = [self.startPoint.x(),self.startPoint.y(),self.endPoint.x(),self.endPoint.y()]
                self.update()
            if self.drawline2event and self.line2endpoint:
                painter.drawLine(self.line2startpoint/self.ratio_rate, self.line2endpoint/self.ratio_rate) # 頂邊Line2
                Angle = self.GetAngle(self.angle_line1, [self.line2startpoint.x(), self.line2startpoint.y(), self.line2endpoint.x(), self.line2endpoint.y()])
                angle_pos = QPoint(self.line2startpoint.x(), self.line2startpoint.y()) + QPoint(15, -10)
                painter.setPen(Qt.blue)
                painter.drawText(angle_pos, '%.2f°'%(Angle))
                self.update()
        if self.image_lines_info[self._current_index]["paints"]:
            # painter一次建立兩個，一個顯示在螢幕上，另一個對原圖畫圖，感覺很吃效能...暫時無解
            fm = QFontMetrics(QFont())
            txt_height = fm.height()
            for paint_Data in self.image_lines_info[self._current_index]["paints"]:    
                painter.setPen(linePen)
                drawpainter.setPen(linePen)
                if paint_Data['func_type'] == 'point2point':
                    x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                    y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                    w = int(self.qpixmap.width()*paint_Data['endPoint_nor_x'])
                    h = int(self.qpixmap.height()*paint_Data['endPoint_nor_y'])
                    pos_list = [x, y, w, h]
                    other_p2p_post_list = []
                    for p in self.image_lines_info[self._current_index]["paints"]:
                        if p['func_type'] == 'point2point':
                            x_ = int(self.qpixmap.width()*p['startPoint_nor_x'])
                            y_ = int(self.qpixmap.height()*p['startPoint_nor_y'])
                            w_ = int(self.qpixmap.width()*p['endPoint_nor_x'])
                            h_ = int(self.qpixmap.height()*p['endPoint_nor_y'])
                            pos_list_ = [x_, y_, w_, h_]
                            if pos_list != pos_list_:
                                other_p2p_post_list.append(pos_list_)
                    #print(x, y, w, h)
                    painter.drawLine(x, y, w, h)
                    drawpainter.drawLine(x, y, w, h)
                    painter.setPen(Qt.blue)
                    drawpainter.setPen(Qt.blue)
                    intersects_result = any([(lambda x: self.line_intersects_judge(pos_list,x))(x) for x in other_p2p_post_list])
                    line_center_post = QPoint((x+w)/2, (y+h)/2) - QPoint(0, 10) # 正常標籤會放在線的上中方
                    # 如果目前的line線條和其他P2P線條重疊,把標籤丟到線段尾端
                    if intersects_result:
                        line_center_post = QPoint(w, h) + QPoint(5, 0)
                    print(calibration_scale_unit)
                    painter.drawText(line_center_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                    drawpainter.drawText(line_center_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))

                elif paint_Data['func_type'] == 'boundingbox':
                    x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                    y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                    w = int(self.qpixmap.width()*paint_Data['nor_width'])
                    h = int(self.qpixmap.height()*paint_Data['nor_height'])

                    painter.drawRect(x, y, w, h)
                    drawpainter.drawRect(x, y, w, h)

                    painter.setPen(Qt.green)
                    drawpainter.setPen(Qt.green)

                    txt_width = fm.width('%.3f %s'%(paint_Data['distance_width'],calibration_scale_unit))
                    box_width_txt_x_post = x + int((w-txt_width)/2) if w-txt_width > 0 else x
                    box_height_txt_y_post = y + int((h-txt_height)/2) if h-txt_height > 0 else y

                    width_center_post = QPoint(box_width_txt_x_post, y) - QPoint(0, 5) # 長標籤放上面
                    height_center_post = QPoint(x + w, box_height_txt_y_post) + QPoint(5, 0) # 高標籤放右邊

                    painter.drawText(width_center_post, '%.2f %s'%(paint_Data['distance_width'],calibration_scale_unit))
                    painter.drawText(height_center_post, '%.2f %s'%(paint_Data['distance_height'],calibration_scale_unit))

                    drawpainter.drawText(width_center_post, '%.2f %s'%(paint_Data['distance_width'],calibration_scale_unit))
                    drawpainter.drawText(height_center_post, '%.2f %s'%(paint_Data['distance_height'],calibration_scale_unit))
                elif paint_Data['func_type'] == 'angle':
                    for i in range(1,3):
                        if paint_Data['line%d'%i]:
                            if i == 1:
                                x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                                y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                                w = int(self.qpixmap.width()*paint_Data['endPoint_nor_x'])
                                h = int(self.qpixmap.height()*paint_Data['endPoint_nor_y'])
                                line1 = [x, y, w, h]
                            elif i == 2:
                                x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                                y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                                w = int(self.qpixmap.width()*paint_Data['line2endPoint_nor_x'])
                                h = int(self.qpixmap.height()*paint_Data['line2endPoint_nor_y'])
                                Angle = self.GetAngle(line1, [x, y, w, h])
                                angle_pos = QPoint(x, y) + QPoint(15, -10)
                                painter.setPen(Qt.blue)
                                drawpainter.setPen(Qt.blue)
                                painter.drawText(angle_pos, '%.2f°'%(Angle))
                                drawpainter.drawText(angle_pos, '%.2f°'%(Angle))
                            painter.setPen(linePen)
                            drawpainter.setPen(linePen)
                            painter.drawLine(x, y, w, h)
                            drawpainter.drawLine(x, y, w, h)
                            
            if self.image_lines_info[self._current_index]["paints"]:
                self.previous_enable_status.emit(True)
                self.restore_enable_status.emit(True)
                self.savefile_enable_status.emit(True)
            else:
                self.previous_enable_status.emit(False)
                self.restore_enable_status.emit(False)
                self.savefile_enable_status.emit(False)
        
    def read_img(self, img_path):
        # self.image = QImage(img_path)
        self.origin_img =  cv2.imread(img_path)
        self.origin_image_height, self.origin_image_width, channel = self.origin_img.shape
        self.display_img = self.origin_img
        # self.bytesPerline = 3 * self.origin_image_width  # 設定「每一行」的影像佔用位置數量，目前因為有 3 個 channel，因此是 3 * width
        self.image = QImage(self.display_img, self.origin_image_width, self.origin_image_height,  QImage.Format_RGB888).rgbSwapped()
        self.qpixmap = QPixmap.fromImage(self.image)
        self.restore_enable_status.emit(False)
        self.savefile_enable_status.emit(False)
        
    def update_painter(self, ratio_value):
        self.ratio_value = ratio_value
        self.ratio_rate = pow(10, (self.ratio_value - 50)/50)
        qpixmap_height = self.origin_image_height*self.ratio_rate
        qpixmap_width = self.origin_image_width*self.ratio_rate
        #self.qpixmap.scaledToHeight(qpixmap_height)
        #self.qpixmap.scaledToWidth(qpixmap_width)
        # self.setFixedSize(self.origin_image_width*self.ratio_rate, qpixmap_height)
        if qpixmap_height > self.origin_image_height:
            self.setFixedSize(self.qpixmap.size()*self.ratio_rate)
        self.update_img_ratio()
        self.update()
        
    def update_img_ratio(self):
        self.update_text_ratio()
        self.update_text_img_shape()

    def update_text_ratio(self):
        self.zoom_info_dict['Zoom ratio'] = f"{int(100*self.ratio_rate)} %"

    def update_text_img_shape(self):
        current_text = f"({self.qpixmap.width()}, {self.qpixmap.height()})"
        origin_text = f"({self.origin_image_width}, {self.origin_image_height})"
        self.zoom_info_dict['Image shape'] = origin_text
        self.zoom_info_dict['current shape'] = current_text

    def update_text_clicked_position(self, x, y, event):
        # print(self.qpixmap.width(), self.qpixmap.height())
        self.zoom_info_dict['clicked pos'] = f"({x}, {y})"
        norm_x = x/self.qpixmap.width()
        norm_y = y/self.qpixmap.height()
        # print(f"(x, y) = ({x}, {y}), normalized (x, y) = ({norm_x}, {norm_y})")
        self.zoom_info_dict['Nor pos'] = f"({norm_x:.3f}, {norm_y:.3f})"
        self.zoom_info_dict['Real pos'] = f"({int(norm_x*self.image_width)}, {int(norm_y*self.image_height)})"
        return ''.join(['%s:%s\n'%(k,self.zoom_info_dict[k]) for k in self.zoom_info_dict.keys()])

    @property
    def current_index(self):
        return self._current_index

    @current_index.setter
    def current_index(self, index):
        if (index <= 0) & (len(list(self.image_lines_info.keys())) == 1):
            self.previous_enable = False
            self.next_enable = False
        elif (index == 0) & (len(list(self.image_lines_info.keys())) > 0):
            self.previous_enable = False
            self.next_enable = True
        elif index == (len(list(self.image_lines_info.keys())) - 1):
            self.previous_enable = True
            self.next_enable = False
        else:
            self.previous_enable = True
            self.next_enable = True

        if 0 <= index < len(list(self.image_lines_info.keys())):
            self._current_index = index
            self.image_path = self.image_lines_info[self._current_index]["file_path"]
            self.image_pathlib = pathlib.Path(self.image_path)
            self.read_img(self.image_path)
            self.update_painter(self.ratio_value)
    
    def save_panter_img(self):
        image_folder_path = str(self.image_pathlib.parents[0])
        save_img_folder_path = pathlib.Path(image_folder_path, 'measured')
        if not pathlib.Path(save_img_folder_path).exists():
            os.makedirs(save_img_folder_path)
        save_img_path = pathlib.Path(save_img_folder_path, self.image_pathlib.stem+'_m'+self.image_pathlib.suffix)
        self.image.save(str(save_img_path), "PNG")
        self.currentinfo_save_status.emit(True)
    
    def crop_img_and_save(self ,x,y,w,h):
        image_folder_path = str(self.image_pathlib.parents[0])
        save_img_folder_path = pathlib.Path(image_folder_path, 'crop')
        if not pathlib.Path(save_img_folder_path).exists():
            os.makedirs(save_img_folder_path)
        save_img_path = pathlib.Path(save_img_folder_path, self.image_pathlib.stem+'_c'+self.image_pathlib.suffix)
        count = 1
        while pathlib.Path(save_img_path).exists():
            save_img_path = pathlib.Path(save_img_folder_path, self.image_pathlib.stem+'_c%d'%count+self.image_pathlib.suffix)
            count += 1
        crop_img = self.origin_img[y:y+h, x:x+w]
        cv2.imwrite(str(save_img_path), crop_img)
        
    def painter_restore(self):
        self.image_lines_info[self._current_index]["paints"] = [] # 清空該張圖片所有量的東西
        self.restore_enable_status.emit(False)
        self.update_painter(self.ratio_value)
    
    def painter_previous(self):
        self.image_lines_info[self._current_index]["paints"] = self.image_lines_info[self._current_index]["paints"][:-1]
        self.update_painter(self.ratio_value)

    def line_intersects_judge(self, l1, L2):
        line1_x, line1_y, line1_w, line1_h = l1
        line2_x, line2_y, line2_w, line2_h = L2
        if min(line1_x,line1_w)<=max(line2_x,line2_w) and min(line2_y,line2_h)<=max(line1_y,line1_h) and min(line2_x,line2_w)<=max(line1_x,line1_w) and min(line1_y,line1_h)<=max(line2_y,line2_h):
            return True
        else:
            return False
    
    def GetAngle(self, line1_list, line2_list):
        dx1 = line1_list[0] - line1_list[2]
        dy1 = line1_list[1] - line1_list[3]
        dx2 = line2_list[0] - line2_list[2]
        dy2 = line2_list[1] - line2_list[3]
        angle1 = math.atan2(dy1, dx1)
        angle1 = float(angle1 * 180 / math.pi)
        #print("angle1",angle1)
        angle2 = math.atan2(dy2, dx2)
        angle2 = float(angle2 * 180 / math.pi)
        #print("angle2",angle2)
        if angle1 * angle2 >= 0:
            insideAngle = abs(angle1 - angle2)
        else:
            insideAngle = abs(angle1) + abs(angle2)
            if insideAngle > 180:
                insideAngle = 360 - insideAngle
        insideAngle = insideAngle % 180
        return insideAngle

class App(QWidget):
    def __init__(self,parent=None, image_list = []):
        global calibration_scale, calibration_scale_unit 
        super().__init__(parent)

        self.ratio_value = 50
        self.cal_zoom_ratio()
        self.current_info = {"Image No.":"0",
                             "Zoom Ratio":"{:.3f}".format(self.app_ratio_rate), 
                             "Pixel Sacle value":"{:.4f}".format(calibration_scale), 
                             "Pixel Sacle uint":'%s/pixel'%calibration_scale_unit,
                             "Image Save":"False"}
        self.image_list = image_list

        btn_font = QFont()
        btn_font.setFamily("微軟正黑體")
        btn_font.setPointSize(12)

        centralwidget = QWidget(self)
        verticalLayout_Widget = QWidget(centralwidget)
        verticalLayout_Widget.setGeometry(QRect(1, 5, 1024, 580))

        verticalLayout_main = QVBoxLayout(verticalLayout_Widget)
        verticalLayout_main.setContentsMargins(0, 0, 0, 0)
        horizontalLayout_main = QHBoxLayout() # 測量選項橫條

        self.openfile_btn = QPushButton(verticalLayout_Widget)
        self.openfile_btn.setIcon(QIcon('icon/openfile.png'))
        self.openfile_btn.setIconSize(QSize(30, 30))
        self.openfile_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.openfile_btn.clicked.connect(self.func_openfile)
        self.openfile_btn.setToolTip("Open file")
        horizontalLayout_main.addWidget(self.openfile_btn)

        self.openfolder_btn = QPushButton(verticalLayout_Widget)
        self.openfolder_btn.setIcon(QIcon('icon/openfolder.png'))
        self.openfolder_btn.setIconSize(QSize(30, 30))
        self.openfolder_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.openfolder_btn.clicked.connect(self.func_openfolder)
        self.openfolder_btn.setToolTip("Open folder")
        horizontalLayout_main.addWidget(self.openfolder_btn)

        self.savefile_btn = QPushButton(verticalLayout_Widget)
        self.savefile_btn.setIcon(QIcon('icon/savefile.png'))
        self.savefile_btn.setIconSize(QSize(30, 30))
        self.savefile_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.savefile_btn.setToolTip("Save")
        horizontalLayout_main.addWidget(self.savefile_btn)

        self.restore_btn = QPushButton(verticalLayout_Widget)
        self.restore_btn.setIcon(QIcon('icon/restore_btn.png'))
        self.restore_btn.setIconSize(QSize(30, 30))
        self.restore_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.restore_btn.setToolTip("Restore")
        self.restore_btn.setEnabled(False)
        horizontalLayout_main.addWidget(self.restore_btn)

        self.previous_btn = QPushButton(verticalLayout_Widget)
        self.previous_btn.setIcon(QIcon('icon/previous_btn.png'))
        self.previous_btn.setIconSize(QSize(30, 30))
        self.previous_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.previous_btn.setToolTip("Previous")
        self.previous_btn.setEnabled(False)
        horizontalLayout_main.addWidget(self.previous_btn)

        line_1 = QFrame(verticalLayout_Widget)
        line_1.setFrameShape(QFrame.VLine)
        line_1.setFrameShadow(QFrame.Sunken)
        horizontalLayout_main.addWidget(line_1)
        
        self.camera_btn = QPushButton(verticalLayout_Widget)
        self.camera_btn.setIcon(QIcon('icon/Screenshot.png'))
        self.camera_btn.setIconSize(QSize(30, 30))
        self.camera_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.camera_btn.clicked.connect(self.open_camera)
        self.camera_btn.setToolTip("Camera screenshot")
        horizontalLayout_main.addWidget(self.camera_btn)

        self.calibration_btn = QPushButton(verticalLayout_Widget)
        self.calibration_btn.setIcon(QIcon('icon/calibration.png'))
        self.calibration_btn.setIconSize(QSize(30, 30))
        self.calibration_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.calibration_btn.setToolTip("Calibration")
        horizontalLayout_main.addWidget(self.calibration_btn)
        self.calibration_btn.clicked.connect(self.calibration_dialog)

        line_2 = QFrame(verticalLayout_Widget)
        line_2.setFrameShape(QFrame.VLine)
        line_2.setFrameShadow(QFrame.Sunken)
        horizontalLayout_main.addWidget(line_2)

        self.point2point_btn = QPushButton(verticalLayout_Widget)
        self.point2point_btn.setIcon(QIcon('icon/slash.png'))
        self.point2point_btn.setIconSize(QSize(30, 30))
        self.point2point_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.point2point_btn.clicked.connect(self.func_point2point)
        self.point2point_btn.setToolTip("point-point distance")
        horizontalLayout_main.addWidget(self.point2point_btn)

        self.angle_btn = QPushButton(verticalLayout_Widget)
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.angle_btn.setIconSize(QSize(30, 30))
        self.angle_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.angle_btn.clicked.connect(self.func_angle)
        self.angle_btn.setToolTip("Angle")
        horizontalLayout_main.addWidget(self.angle_btn)

        self.boundingbox_btn = QPushButton(verticalLayout_Widget)
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.boundingbox_btn.setIconSize(QSize(30, 30))
        self.boundingbox_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.boundingbox_btn.clicked.connect(self.func_boundingbox)
        self.boundingbox_btn.setToolTip("Angle")
        horizontalLayout_main.addWidget(self.boundingbox_btn)

        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        horizontalLayout_main.addItem(spacerItem)
        verticalLayout_main.addLayout(horizontalLayout_main)

        self.pre_Button = QPushButton()
        self.pre_Button.setIcon(QIcon('icon/previous.png'))
        self.pre_Button.setIconSize(QSize(30, 30))
        self.pre_Button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.pre_Button.clicked.connect(self.handle_previous)
        self.next_Button = QPushButton()
        self.next_Button.setIcon(QIcon('icon/next.png'))
        self.next_Button.setIconSize(QSize(30, 30))
        self.next_Button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.next_Button.clicked.connect(self.handle_next)
        horizontalLayout_main.addWidget(self.pre_Button)
        horizontalLayout_main.addWidget(self.next_Button)
        
        scrollArea = QScrollArea(widgetResizable=True)
        scrollArea.setWidgetResizable(True)
        scrollArea.setBackgroundRole(QPalette.Dark)

        self.zoom_in_btn = QPushButton(centralwidget)
        self.zoom_in_btn.setGeometry(QRect(10, 150, 50, 50))
        self.zoom_in_btn.setIconSize(QSize(40, 40))
        self.zoom_in_btn.setIcon(QIcon('icon/zoom-in.png'))
        self.zoom_in_btn.setStyleSheet('border-radius: 10px; border: 2px groove gray;border-style: outset;')

        self.zoom_out_btn = QPushButton(centralwidget)
        self.zoom_out_btn.setGeometry(QRect(10, 400, 50, 50))
        self.zoom_out_btn.setIconSize(QSize(40, 40))
        self.zoom_out_btn.setIcon(QIcon('icon/zoom-out.png'))
        self.zoom_out_btn.setStyleSheet('border-radius: 10px; border: 2px groove gray;border-style: outset;')

        self.vertical_Slider = QSlider(centralwidget)
        self.vertical_Slider.setValue(self.ratio_value)
        self.vertical_Slider.setGeometry(QRect(28, 210, 15, 180))
        self.vertical_Slider.setStyleSheet('''
            QSlider::groove:vertical {border: 0px solid transparent;width: 8px;background: #bebebe;margin: 2px 0;}
            QSlider::handle:vertical {background-color: #353f53;height: 12px;width: 6px;border: 0px solid transparent;margin: 0 -2px;border-radius: 6px;}
            QSlider::handle:vertical:pressed {background-color: #353f53;height: 18px;width: 6px;border: 0px solid transparent;margin: 0 -5px;border-radius: 9px;}
            QSlider::handle:vertical:disabled {background-color: #bebebe;height: 8px;width: 2px;border: 0px solid transparent;margin: -1px -1px;border-radius: 4px;}''')

        self.vertical_Slider.valueChanged.connect(self.getslidervalue)

        # self.zoom_info = QLabel(centralwidget)
        # self.zoom_info.setGeometry(QRect(400, 50, 200, 200))
        # self.zoom_info.setAlignment(Qt.AlignRight | Qt.AlignTop)
        # self.zoom_info.setStyleSheet('color:blue')
        # self.zoom_info.setAttribute(Qt.WA_TranslucentBackground)

        self.measure_info = QLabel(centralwidget)
        self.measure_info.setGeometry(QRect(5, 50, 250, 100))
        self.measure_info.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.measure_info.setStyleSheet('color:blue')
        self.measure_info.setAttribute(Qt.WA_TranslucentBackground)

        self.zoom_in_btn.clicked.connect(self.func_zoom_in) 
        self.zoom_out_btn.clicked.connect(self.func_zoom_out)
        self.label = paint_Label(self.create_init_img_info(),self.ratio_value)
        # self.label.mousePressEvent = self.set_clicked_position 查修用,開啟後無法量測
        self.pre_Button.setEnabled(self.label.previous_enable)
        self.next_Button.setEnabled(self.label.next_enable)

        self.savefile_btn.clicked.connect(lambda: self.label.save_panter_img())
        self.restore_btn.clicked.connect(self.label.painter_restore) 
        self.previous_btn.clicked.connect(self.label.painter_previous) 
        self.label.savefile_enable_status.connect(self.savefile_btn_status)
        self.label.restore_enable_status.connect(self.restore_btn_status)
        self.label.previous_enable_status.connect(self.previous_btn_status)
        self.label.currentinfo_save_status.connect(self.measure_info_save_status)

        scrollArea.setWidget(self.label)
        verticalLayout_main.addWidget(scrollArea)

        self.setWindowOpacity(0.9) # 设置窗口透明度
        self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
    
    def cal_zoom_ratio(self):
        self.app_ratio_rate = pow(10, (self.ratio_value - 50)/50)
    
    def getslidervalue(self):  
        self.ratio_value = self.vertical_Slider.value()
        self.cal_zoom_ratio()
        self.current_info["Zoom Ratio"] = "{:.3f}".format(self.app_ratio_rate)
        self.update_measure_info()
        self.label.update_painter(self.ratio_value)

    def func_zoom_in(self):
        self.ratio_value = max(0, self.ratio_value + 1)
        self.cal_zoom_ratio()
        self.current_info["Zoom Ratio"] = "{:.3f}".format(self.app_ratio_rate)
        self.update_measure_info()
        self.vertical_Slider.setValue(self.ratio_value)
        self.label.update_painter(self.ratio_value)

    def func_zoom_out(self):
        self.ratio_value = min(100, self.ratio_value - 1)
        self.cal_zoom_ratio()
        self.current_info["Zoom Ratio"] = "{:.3f}".format(self.app_ratio_rate)
        self.update_measure_info()
        self.vertical_Slider.setValue(self.ratio_value)
        self.label.update_painter(self.ratio_value)

    # def set_clicked_position(self, event):
    #     x = event.pos().x()
    #     y = event.pos().y()
    #     click_info = self.label.update_text_clicked_position(x, y, event)
    #     self.zoom_info.setText(click_info)

    def func_point2point(self):
        self.label.setMouseTracking(False) # Sarah add, 0118
        self.label.point2point_fun = True
        self.label.boundingbox_fun = False
        self.label.angle_fun = False

    def func_boundingbox(self):
        self.label.setMouseTracking(True) # Sarah add, 0118
        self.label.boundingbox_fun = True
        self.label.point2point_fun = False
        self.label.angle_fun = False
        messageBox = QMessageBox()
        messageBox.setWindowTitle('BoundingBox mode message')
        messageBox.setText('Save BoundingBox crop image?')
        messageBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Close)
        yes_crop_input = messageBox.button(QMessageBox.Yes)
        no_crop_input = messageBox.button(QMessageBox.No)
        Q_close = messageBox.button(QMessageBox.Close)
        Q_close.setText("Cancle")
        messageBox.exec_()

        self.label.save_biundingbox = True if messageBox.clickedButton() == yes_crop_input else False
    
    def func_angle(self):
        self.label.setMouseTracking(False) # Sarah add, 0118
        self.label.boundingbox_fun = False
        self.label.point2point_fun = False
        self.label.angle_fun = True

    def func_openfolder(self):
        folder_path = QFileDialog.getExistingDirectory(self,"開啟資料夾","./", options=QFileDialog.DontUseNativeDialog)
        if folder_path:
            file_list = [os.path.join(folder_path, f).replace('\\','/') for f in os.listdir(folder_path) if f.split('.')[-1] in ['jpg','png','jpeg']]
            self.image_list += file_list
            image_lines_info = self.create_init_img_info()
            self.label.image_lines_info = image_lines_info
            self.label.current_index = 0
            self.image_list = [] # 清空image list
            self.pre_Button.setEnabled(self.label.previous_enable)
            self.next_Button.setEnabled(self.label.next_enable)
            self.current_info["Image No."] = "1"
            self.update_measure_info()
        
    def func_openfile(self):
        filename, filetype = QFileDialog.getOpenFileName(self,"開啟檔案","./", 'Image files (*.jpg *.png *.jpeg)', options=QFileDialog.DontUseNativeDialog)
        if filename:
            self.image_list.append(filename)
            image_lines_info = self.create_init_img_info()
            self.label.image_lines_info = image_lines_info
            self.label.current_index = 0
            self.image_list = [] # 清空image list
            self.pre_Button.setEnabled(self.label.previous_enable)
            self.next_Button.setEnabled(self.label.next_enable)
            self.current_info["Image No."] = "1"
            self.update_measure_info()
    
    def handle_next(self):
        self.label.current_index += 1
        self.current_info["Image No."] = "%d"%(self.label.current_index+1)
        self.update_measure_info()
        self.pre_Button.setEnabled(self.label.previous_enable)
        self.next_Button.setEnabled(self.label.next_enable)

    def handle_previous(self):
        self.label.current_index -= 1
        self.current_info["Image No."] = "%d"%(self.label.current_index+1)
        self.update_measure_info()
        self.pre_Button.setEnabled(self.label.previous_enable)
        self.next_Button.setEnabled(self.label.next_enable)
    
    def create_init_img_info(self):
        if self.image_list:
            self.savefile_btn.setEnabled(True)
            return {p:{"file_path":path,"save":False,"paints":[]} for p, path in enumerate(self.image_list)}
        else:
            self.savefile_btn.setEnabled(False)
            return {0:{"file_path":"icon/empty.png","save":False,"paints":[]}}

    def open_camera(self):
        import manual_camera

        self.Camera_w = manual_camera.Camera_MainWindow()
        self.Camera_w.app_widget.submitClicked.connect(self.receive_camera_screenshot)
        self.Camera_w.show()
    
    def receive_camera_screenshot(self, camera_img_list):
        self.image_list = camera_img_list
        # print("camera_img_list",camera_img_list)
        image_lines_info = self.create_init_img_info()
        self.label.image_lines_info = image_lines_info
        self.label.current_index = 0
        self.image_list = [] # 清空image list
        self.pre_Button.setEnabled(self.label.previous_enable)
        self.next_Button.setEnabled(self.label.next_enable)

    def calibration_dialog(self):
        global calibration_scale

        messageBox = QMessageBox()
        messageBox.setWindowTitle('Calibration mode message')
        messageBox.setText('Select one mode.')
        messageBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Close)
        Q_value_input = messageBox.button(QMessageBox.Yes)
        Q_value_input.setText("Manual input")
        Q_camera_input = messageBox.button(QMessageBox.No)
        Q_camera_input.setText("Camera calibration")
        Q_close = messageBox.button(QMessageBox.Close)
        Q_close.setText("Cancle")
        messageBox.exec_()

        if messageBox.clickedButton() == Q_value_input:
            import digit_keyboard

            self.keyboard_w = digit_keyboard.keyboard_MainWindow(current_pixel=calibration_scale, window_title="像素比例值輸入")
            self.keyboard_w.app_widget.input_values.connect(self.receive_calibration_value)
            self.keyboard_w.app_widget.input_unit.connect(self.receive_calibration_unit)
            self.keyboard_w.show()
        elif messageBox.clickedButton() == Q_camera_input:
            import calibration_pixelscale

            self.calibration_w = calibration_pixelscale.MainWindow()
            self.calibration_w.calibration_scale.connect(self.receive_calibration_value)
            self.calibration_w.calibration_unit.connect(self.receive_calibration_unit)
            self.calibration_w.show()

    def receive_calibration_value(self, calibration_value):
        global calibration_scale
        config = configparser.ConfigParser()
        config.read(os.path.join(os.getcwd(),'magnificationRate.ini'))
        calibration_scale = calibration_value
        self.current_info["Pixel Sacle value"] = "{:.4f}".format(calibration_scale)
        config['Calibration']['value'] = str(calibration_scale)
        with open('measure_tool_config.ini', 'w', encoding='utf-8') as configfile: 
            config.write(configfile)
        self.update_measure_info()
        # print("calibration_scale:",calibration_scale)

    def receive_calibration_unit(self, calibration_unit):
        global calibration_scale_unit
        config = configparser.ConfigParser()
        config.read(os.path.join(os.getcwd(),'magnificationRate.ini'))
        calibration_scale_unit = calibration_unit
        # config['Calibration']['value'] = str(calibration_scale)
        config['Calibration']['unit'] = calibration_unit
        self.current_info["Pixel Sacle uint"] = '%s/pixel'%calibration_scale_unit
        with open('measure_tool_config.ini', 'w', encoding='utf-8') as configfile: 
            config.write(configfile)
        self.update_measure_info()

    def savefile_btn_status(self, bool_status):
        self.savefile_btn.setEnabled(bool_status)

    def restore_btn_status(self, bool_status):
        self.restore_btn.setEnabled(bool_status)
    
    def previous_btn_status(self, bool_status):
        self.previous_btn.setEnabled(bool_status)
    
    def update_measure_info(self):
        self.measure_info.setText('\n'.join(["%s: %s"%(k, str(self.current_info[k])) for k in self.current_info]))

    def measure_info_save_status(self, bool_status):
        self.current_info["Image Save"] = bool_status
        self.measure_info.setText('\n'.join(["%s: %s"%(k, str(self.current_info[k])) for k in self.current_info]))


        
if __name__=="__main__":
    app = QApplication(sys.argv)
    app_icon = QIcon("icon/main.png")
    app.setWindowIcon(app_icon)
    app_window = MainWindow()
    sys.exit(app.exec_())