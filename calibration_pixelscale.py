import cv2
import numpy as np
import configparser
from PyQt5.QtWidgets import  *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import utils.stylelogging as log
import qdarkstyle

real_line_length = 0 # 虛擬鍵盤初始值,真實線長
distance = 200 # 校正線的長度(單位:pixel)
line_center_post = QPoint(310, 240) # 初始線的中心座標
force_straight = False

class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)
    camera_status = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self._run_flag = True
        
    def run(self):
        cap = cv2.VideoCapture(0)
        while self._run_flag:
            ret, cv_img = cap.read()
            if ret:
                self.camera_status.emit('connect')
                self.change_pixmap_signal.emit(cv_img)
            else:
                self.camera_status.emit('disconnect')
                self.stop()
        cap.release()
    
    def stop(self):
        """Sets run flag to False and waits for thread to finish"""
        self._run_flag = False
        self.wait()

class switch_btn(QPushButton):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.setCheckable(True)
        self.setMinimumWidth(120)
        self.setMinimumHeight(50)

    def paintEvent(self, event):
        self.status = "stop" if self.isChecked() else "live"
        bg_color = Qt.red if self.isChecked() else Qt.green

        radius = 20 # 控制btn大小?
        width = 50 # 控制btn長度
        center = self.rect().center()

        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.translate(center)
        painter.setBrush(QColor(0,0,0))

        pen = QPen(Qt.black)
        pen.setWidth(2)
        painter.setPen(pen)

        painter.drawRoundedRect(QRect(-width, -radius, 2*width, 2*radius), radius, radius)
        painter.setBrush(QBrush(bg_color))
        sw_rect = QRect(-radius, -radius, width + radius, 2*radius)
        if not self.isChecked():
            sw_rect.moveLeft(-width)
        painter.drawRoundedRect(sw_rect, radius, radius)
        painter.drawText(sw_rect, Qt.AlignCenter, self.status)

class CustomItem(QGraphicsEllipseItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setFlag(self.ItemIsMovable)
        self.setFlag(self.ItemSendsGeometryChanges)
        self.line = None
        self.isPoint = None
        self.distance_txt = None

    def addLine(self, line, ispoint):
        self.line = line
        self.isPoint = ispoint

    def addtxt(self, distance_txt):
        self.distance_txt = distance_txt

    def itemChange(self, change , value):
        if change == self.ItemPositionChange and self.scene():
            newPos = value
            self.moveLineToCenter(newPos)
        return super(CustomItem, self).itemChange(change, value)

    def moveLineToCenter(self, newPos):
        global distance, line_center_post, force_straight

        xOffset = self.rect().x() + self.rect().width()/2
        yOffset = self.rect().y() + self.rect().height()/2
        newCenterPos = QPointF(newPos.x()+xOffset, newPos.y()+yOffset) 
        self.p1 = newCenterPos if self.isPoint else self.line.line().p1() # p1->right point
        self.p2 = self.line.line().p2() if self.isPoint else newCenterPos # p2->left point
        #print('before', self.p1, self.p2)
        if force_straight:
            self.p2 = QPointF(self.p2.x(), 250)
            self.p1 = QPointF(self.p1.x(), 250)
        #print('after', self.p1, self.p2)
        line_ = QLineF(self.p1, self.p2)
        self.line.setLine(line_)
        distance = round(line_.length(), 2)
        line_center_post = QPoint((self.p1.x()+self.p2.x())/2, (self.p1.y()+self.p2.y())/2)
        self.distance_txt.setPlainText("{:.2f} px".format(distance))
        self.distance_txt.setFont(QFont("微軟正黑體",10,QFont.Bold))
        text_width = self.distance_txt.boundingRect().width() # 字的寬度 pixel
        self.distance_txt.setPos(line_center_post - QPoint(text_width/2, 0)) # QPoint(x, y)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1024, 600)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QRect(5, 5, 1016, 566))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.pause_brn = switch_btn(self.verticalLayoutWidget)
        self.pause_brn.setIcon(QIcon('icon/pause_btn.png'))
        self.pause_brn.setIconSize(QSize(50, 50))
        self.pause_brn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.horizontalLayout.addWidget(self.pause_brn)

        self.straight_brn = QPushButton(self.verticalLayoutWidget)
        self.straight_brn.setIcon(QIcon('icon/Y_axis_fix_btn.png'))
        self.straight_brn.setIconSize(QSize(50, 50))
        self.straight_brn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.horizontalLayout.addWidget(self.straight_brn)

        self.input_btn = QPushButton(self.verticalLayoutWidget)
        self.input_btn.setIcon(QIcon('icon/input_btn.png'))
        self.input_btn.setIconSize(QSize(50, 50))
        self.input_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.horizontalLayout.addWidget(self.input_btn)

        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)

        self.exit_btn = QPushButton(self.verticalLayoutWidget)
        self.exit_btn.setIcon(QIcon('icon/exit.png'))
        self.exit_btn.setIconSize(QSize(50, 50))
        self.exit_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.horizontalLayout.addWidget(self.exit_btn)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_video = QHBoxLayout(self.verticalLayoutWidget)

        self.graphicsView = QGraphicsView(self.verticalLayoutWidget)
        self.horizontalLayout_video.addWidget(self.graphicsView)

        self.calibration_label = QLabel(self.verticalLayoutWidget)
        self.calibration_label.setStyleSheet('color:green')
        self.calibration_label.setFont(QFont('Arial', 12))
        self.calibration_label.setText('''----------------Calibration use note------------------\n1. Live/stop switch button control screen graph.\n2. Calibration input button determine pixel scale.\n3. Pull and Hold right click to move\n    line two edge point,fit real line length.\n4. Force line straight button, will fix y axis\n    untill next open.
        ''')
        
        self.horizontalLayout_video.addWidget(self.calibration_label)

        self.verticalLayout.addLayout(self.horizontalLayout_video)

        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        MainWindow.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))

class MainWindow(QMainWindow, Ui_MainWindow):
    calibration_scale = pyqtSignal(float)
    calibration_unit = pyqtSignal(str)
    def __init__(self, parent=None):
        super().__init__(parent)
        global distance, line_center_post

        self.logger = log.StyleLog(loglevel='DEBUG', logger_name='Calibration', logfile='./log/log.log')
        self.logger.info('Open Calibration window.')
        self.setupUi(self)
        self.setWindowFlag(Qt.FramelessWindowHint)

        self.camera_live = True
        self.pen_line = QPen()
        self.pen_line.setStyle(Qt.SolidLine)
        self.pen_line.setWidth(3)
        self.pen_line.setBrush(Qt.red)
        self.pen_line.setCapStyle(Qt.PenCapStyle.RoundCap)
        self.pen_point = QPen()
        self.pen_point.setStyle(Qt.DotLine)
        self.pen_point.setWidth(2)
        self.pen_point.setBrush(Qt.red)
        self.pen_point.setCapStyle(Qt.PenCapStyle.RoundCap)
        self.left_point_c = QPointF(200.0, 240.0)
        self.left_point_s = QSizeF(20.0,20.0)
        self.right_point_c = QPointF(400.0, 240.0)
        self.right_point_s = QSizeF(20.0,20.0)
        self.line_c = QPointF(210,250)
        self.line_s = QPointF(410,250)

        self.pause_brn.clicked.connect(self.video_pause)
        self.scene = QGraphicsScene(self)

        self.exit_btn.clicked.connect(self.video_close)
        self.input_btn.clicked.connect(self.input_dialog)
        self.straight_brn.clicked.connect(self.fix_Y_axis)
        
        self.thread = VideoThread()
        self.thread.change_pixmap_signal.connect(self.update_image)
        self.thread.camera_status.connect(self.camera_disconect_message)
        self.logger.info('Camera connect start.')
        self.thread.start()

        # current_dir = os.path.dirname(os.path.realpath(__file__))
        # filename = os.path.join(current_dir, "20221005173309.png")
        # image = cv2.imread(filename)
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # height, width = image.shape
        # image_disp = QImage(image.data, width, height, QImage.Format_Grayscale8)
        # pixMap = QPixmap.fromImage(image_disp)
        #self.pixmap_item = self.scene.addPixmap(pixMap)

        left_point = CustomItem(QRectF(self.left_point_c, self.left_point_s)) # left, top, width, height
        left_point.setPen(self.pen_point)
        self.scene.addItem(left_point)

        right_point = CustomItem(QRectF(self.right_point_c, self.right_point_s))
        right_point.setPen(self.pen_point)
        self.scene.addItem(right_point)

        text = self.scene.addText("{:.2f} px".format(distance))
        text.setDefaultTextColor(QColor(Qt.blue))
        text.setFont(QFont("微軟正黑體", 12, QFont.Bold))
        text_width = text.boundingRect().width() # 字的寬度 pixel
        text.setPos(line_center_post - QPoint(text_width/2, 10)) # QPoint(x, y)

        line = self.scene.addLine(QLineF(self.line_c, self.line_s), self.pen_line)
        left_point.addLine(line, True)
        right_point.addLine(line, False)
        left_point.addtxt(text)
        right_point.addtxt(text)

        right_point.setZValue(1)
        left_point.setZValue(1)
        line.setZValue(2)
        text.setZValue(3)
        
        self.graphicsView.setScene(self.scene)

        self.setWindowOpacity(0.9) # 设置窗口透明度
        self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    @pyqtSlot(np.ndarray)
    def update_image(self, cv_img):
        """Updates the image_label with a new opencv image"""
        if self.camera_live:
            qt_img = self.convert_cv_qt(cv_img)
            # self.video_label.setPixmap(qt_img)
            self.pixmap_item = self.scene.addPixmap(qt_img)
    
    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        if self.camera_live:
            rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
            self.temp_rgb_image = rgb_image
            h, w, ch = rgb_image.shape
            bytes_per_line = ch * w
            p = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)
            # convert_to_Qt_format = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)
            # p = convert_to_Qt_format.scaled(1019, 520, Qt.KeepAspectRatio)
            return QPixmap.fromImage(p)

    def video_pause(self):
        self.logger.info('Switch camera status.')
        self.camera_live = False if self.camera_live else True

    def input_dialog(self):
        global real_line_length
        import digit_keyboard 
        
        self.keyboard_w = digit_keyboard.keyboard_MainWindow(current_pixel=real_line_length, window_title="實際值輸入")
        self.keyboard_w.app_widget.input_values.connect(self.receive_input_values)
        self.keyboard_w.app_widget.input_unit.connect(self.receive_input_unit)
        self.logger.info('Open keyboard current real_line_length:%s.'%str(real_line_length))
        self.keyboard_w.show()

    def receive_input_values(self, receive_data):
        global distance
        self.logger.info('Keyboard Enter real_line_length:%s.'%str(receive_data))
        self.calibration_scale.emit(receive_data/distance)

    def receive_input_unit(self, receive_unit):
        self.logger.info('Keyboard Enter real_line_unit:%s.'%str(receive_unit))
        self.calibration_unit.emit(receive_unit)
        QMessageBox.information(None, 'Application Message', 'Correction successfull,return measure tool！',QMessageBox.Ok)
        self.close()

    def video_close(self):
        self.logger.info('Calibration window close.')
        self.thread.stop()
        self.close()

    def camera_disconect_message(self, camera_info):
        if camera_info == 'disconnect':
            self.logger.info('Camera disconnect.')
            self.input_btn.setEnabled(False)
            self.pause_brn.setEnabled(False)
            QMessageBox.warning(None, 'Warning Message', 'Camera disconnect！',QMessageBox.Ok)

    def fix_Y_axis(self):
        global force_straight
        force_straight = True

            
# if __name__ == "__main__":
#     import sys

#     app = QApplication(sys.argv)
#     w = MainWindow()
#     w.show()
#     sys.exit(app.exec_())