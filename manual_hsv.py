import cv2
from PySide2.QtGui import QImage,QPixmap,QFont
from PySide2 import QtWidgets, QtCore, QtWidgets
import qdarkstyle

class HSV_MainWindow(QtWidgets.QMainWindow): 
    def __init__(self, parent=None, img_path=''):
        super().__init__(parent)
        self.setWindowTitle("HSV Editor")
        self.resize(1024, 600)
        self.app_widget = HSV_App(parent=self, App_img_path=img_path)
        self.setCentralWidget(self.app_widget)

class HSV_App(QtWidgets.QWidget):
    def __init__(self, parent=None, App_img_path=''):
        super().__init__(parent)

        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(5, 5, 1010, 590))

        self.horizontalLayout = QtWidgets.QHBoxLayout(self.verticalLayoutWidget)
        self.horizontalLayout.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_7 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout.addWidget(self.label_7)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setSpacing(6)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.horizontalSlider = QtWidgets.QSlider(self.verticalLayoutWidget)
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setObjectName("horizontalSlider")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.horizontalSlider)
        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.horizontalSlider_2 = QtWidgets.QSlider(self.verticalLayoutWidget)
        self.horizontalSlider_2.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_2.setObjectName("horizontalSlider_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.horizontalSlider_2)
        self.label_3 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.horizontalSlider_3 = QtWidgets.QSlider(self.verticalLayoutWidget)
        self.horizontalSlider_3.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_3.setObjectName("horizontalSlider_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.horizontalSlider_3)
        self.label_4 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.horizontalSlider_4 = QtWidgets.QSlider(self.verticalLayoutWidget)
        self.horizontalSlider_4.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_4.setObjectName("horizontalSlider_4")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.horizontalSlider_4)
        self.label_5 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.horizontalSlider_5 = QtWidgets.QSlider(self.verticalLayoutWidget)
        self.horizontalSlider_5.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_5.setObjectName("horizontalSlider_5")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.horizontalSlider_5)
        self.label_6 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_6.setObjectName("label_6")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_6)
        self.horizontalSlider_6 = QtWidgets.QSlider(self.verticalLayoutWidget)
        self.horizontalSlider_6.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_6.setObjectName("horizontalSlider_6")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.horizontalSlider_6)
        self.lineEdit = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lineEdit.setObjectName("lineEdit")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.SpanningRole, self.lineEdit)
        self.horizontalLayout.addLayout(self.formLayout)

        ft=QFont()
        ft.setPointSize(12)
    
        self.horizontalSlider.setMaximum(255)
        self.horizontalSlider_2.setMaximum(255)
        self.horizontalSlider_3.setMaximum(255)
        self.horizontalSlider_4.setMaximum(255)
        self.horizontalSlider_5.setMaximum(255)
        self.horizontalSlider_6.setMaximum(255)
    
        self.horizontalSlider.setValue(255)
        self.horizontalSlider_3.setValue(255)
        self.horizontalSlider_5.setValue(255)
        self.label.setText("Upper H:"+str(self.horizontalSlider.value()))
        self.label_2.setText("lower H:"+str(self.horizontalSlider_2.value()))
        self.label_3.setText("Upper S:"+str(self.horizontalSlider_3.value()))
        self.label_4.setText("lower S:"+str(self.horizontalSlider_4.value()))
        self.label_5.setText("Upper V:"+str(self.horizontalSlider_5.value()))
        self.label_6.setText("lower V:"+str(self.horizontalSlider_6.value()))
        self.label.setFont(ft)
        self.label_2.setFont(ft)
        self.label_3.setFont(ft)
        self.label_4.setFont(ft)
        self.label_5.setFont(ft)
        self.label_6.setFont(ft)
        self.lineEdit.setText("HSV min: HSV max:")
        self.lineEdit.setFont(ft)
        
        self.img = cv2.imread(App_img_path)
        self.label_image_show(self.img)
    
        self.horizontalSlider.valueChanged[int].connect(self.horizontalSlider1_changeValue)
        self.horizontalSlider_2.valueChanged[int].connect(self.horizontalSlider2_changeValue)
        self.horizontalSlider_3.valueChanged[int].connect(self.horizontalSlider3_changeValue)
        self.horizontalSlider_4.valueChanged[int].connect(self.horizontalSlider4_changeValue)
        self.horizontalSlider_5.valueChanged[int].connect(self.horizontalSlider5_changeValue)
        self.horizontalSlider_6.valueChanged[int].connect(self.horizontalSlider6_changeValue)

        self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
    
    def label_image_show(self,img):
        if(len(img.shape)==2):
            img = cv2.merge([img,img,img])
    
        height, width, bytesPerComponent = img.shape
        bytesPerLine = 3 * width     
        cv2.cvtColor(img, cv2.COLOR_BGR2RGB, img)
        QImg = QImage(img.data, width, height, bytesPerLine, QImage.Format_RGB888)     
        pixmap = QPixmap.fromImage(QImg)
        self.label_7.setPixmap(pixmap)
        self.label_7.update()
    
    def horizontalSlider1_changeValue(self):
        self.label.setText("Upper H:"+str(self.horizontalSlider.value()))
        self.change_lineedit_value()
        self.HSV_img_change()
    def horizontalSlider2_changeValue(self):
        self.label_2.setText("lower H:"+str(self.horizontalSlider_2.value()))
        self.change_lineedit_value()
        self.HSV_img_change()
    def horizontalSlider3_changeValue(self):
        self.label_3.setText("Upper S:"+str(self.horizontalSlider_3.value()))
        self.change_lineedit_value()
        self.HSV_img_change()
    def horizontalSlider4_changeValue(self):
        self.label_4.setText("lower S:"+str(self.horizontalSlider_4.value()))
        self.change_lineedit_value()
        self.HSV_img_change()
    def horizontalSlider5_changeValue(self):
        self.label_5.setText("Upper V:"+str(self.horizontalSlider_5.value()))
        self.change_lineedit_value()
        self.HSV_img_change()
    def horizontalSlider6_changeValue(self):
        self.label_6.setText("lower V:"+str(self.horizontalSlider_6.value()))
        self.change_lineedit_value()
        self.HSV_img_change()
    
    def change_lineedit_value(self):
        self.lineEdit.setText("HSVmin:["+str(self.horizontalSlider_2.value())+","+str(self.horizontalSlider_4.value())+","+str(self.horizontalSlider_6.value())+"] HSVmax:["+str(self.horizontalSlider.value())+","+str(self.horizontalSlider_3.value())+","+str(int(self.horizontalSlider_5.value()))+"]")
    
    def HSV_img_change(self):
        img_target = cv2.inRange(self.img, (self.horizontalSlider_2.value(),self.horizontalSlider_4.value(),self.horizontalSlider_6.value()),
                               (self.horizontalSlider.value(),self.horizontalSlider_3.value(),self.horizontalSlider_5.value()))
   
        img_specifiedColor = cv2.bitwise_and(self.img, self.img,mask=img_target)
        self.label_image_show(img_specifiedColor)

# if __name__ == '__main__':
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     a = HSV_MainWindow(img_path='C:/Users/ETCH/Desktop/diffuser_background/20221013104403.png')
#     a.show()
#     sys.exit(app.exec_())
    # w = Threshold_Value_Edit(img_path='C:/Users/ETCH/Desktop/diffuser_background/20221013104403.png')
    # # w.__init__()
    # w.show()
    # sys.exit(app.exec_())