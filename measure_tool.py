import os, sys, cv2, math, time, pathlib, configparser
import numpy as np
import pandas as pd
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *
import utils.stylelogging as log
# import coefficientLogic as coeffLogic
# from outputData import OutputData, makeDirsWithMode777 # 0508 Sarah add
# import testTool # 0530 Sarah add
# 如果setParameter.txt文件存在就會引用"倍率換算係數"公式得到pixel scale,如果不存在就會沿用本程式預定地measure_tool_config.ini設定文件
# 開啟"手動拍照"(manual camera)可以輸入相機倍率(magnification),返回的數值就會根據"倍率換算係數"公式得到pixel scale,並回寫measure_tool_config.ini
# 如果開啟"校正"(calibration pixelscale)功能裡面的手動對焦校正尺,計算後的pixel scale會應用到主程式(measure_tool),並回寫measure_tool_config.ini,主程式再開啟後的pixel scale會優先載入setParameter.txt設定
# OutputModule = OutputData('manual') # 。0508 Sarah add
# # main_set_file = open('captureAndSave/setParameter.txt', "r") # 60,Emboss,0.4,0.55,0.65,mm # 0508 Sarah comment out
# # main_setting = main_set_file.read() # 0508 Sarah comment out

# 0508 Sarah modify
# if OutputModule.systemSettingDict:
#     magnification = int(OutputModule.systemSettingDict.get('magnification'))
#     previousNum, nextNum = coeffLogic.findNearest(magnification)
#     magnificationX, magnificationY = coeffLogic.calculateCoefficient(previousNum, nextNum)
#     calibration_scale = (magnificationX + magnificationY)/2
#     calibration_scale_unit = OutputModule.systemSettingDict.get('unit')
    
#     measuretool_config = configparser.ConfigParser()
#     measuretool_config.read("measure_tool_config.ini",encoding='utf-8')
# else:
measuretool_config = configparser.ConfigParser()
measuretool_config.read("measure_tool_config.ini",encoding='utf-8')

calibration_scale = float(measuretool_config.get('Calibration','value'))
calibration_scale_unit = measuretool_config.get('Calibration','unit')
measure_lable_size = int(measuretool_config.get('SET','measure_lable_size'))

pathlib.Path('./log').mkdir(parents=True, exist_ok=True)
pathlib.Path('./log').chmod(0o777)
logger = log.StyleLog(loglevel='DEBUG', logger_name='Messure-Tool', logfile='./log/log.log')
pathlib.Path('./log/log.log').chmod(0o777)

theme = measuretool_config.get('Style','theme')
info_font_color = measuretool_config.get('Style','info_font_color')
if theme == 'Default':
    import qdarkstyle
    
class MainWindow(QMainWindow): 
    def __init__(self, parent=None):
        super().__init__(parent)
        
        # # 0530 Sarah add
        # if not testTool.confirmSerialNumbers():
        #     sys.exit()

        self.setWindowTitle("AUO - Manual Messure") # 0414 Polly modified
        self.resize(1024, 580)
        self.app_widget = App(parent=self)
        self.setCentralWidget(self.app_widget)

        self.show()
  
class paint_Label(QWidget):
    # 0411 Polly PySide2
    restore_enable_status = Signal(bool)
    previous_enable_status = Signal(bool)
    savefile_enable_status = Signal(bool)
    currentinfo_save_status = Signal(bool)
    zoom_enable_status = Signal(bool)
    def __init__(self, image_lines_info, ratio_value, _current_index=0, parent=None):
        super().__init__()

        self.startPoint = None
        self.endPoint = None
        self.line2endpoint = None
        self.line2startpoint = None
        self.drawline1event = True
        self.drawline2event = False
        self.drawpointevent = True
        self.circle_startPoint = None
        self.circle_endPoint = None
        self.circle_w = None
        self.circle_h = None
        self.drawline_event = True
        self.line_startPoint = None
        self.line_endPoint = None
        self.drawcircle1_event = True
        self.circle1_startPoint = None
        self.circle1_endPoint = None
        self.circle2_startPoint = None
        self.circle2_endPoint = None
        self.circle1_w = None
        self.circle1_h = None
        self.circle2_w = None
        self.circle2_h = None
        self.center = None
        self.radius = None
        self.radius2 = None
        self.draw_curvPoint_index = 0

        self.zoom_info_dict = {'Zoom ratio':'','Image shape':'','current shape':'','clicked pos':'','Nor pos':'','Real pos':''}
        self.point2point_fun = False
        self.boundingbox_fun = False
        self.angle_fun = False
        self.point2CC_fun = False
        self.line2CC_fun = False
        self.line2point_fun = False
        self.CC2CC_fun = False
        self.Ellipse_diameter_fun = False
        self.ConC_diameter_fun = False
        self.radius_of_curv_fun = False

        self.project_name = None
        self.save_CSV = False

        self.save_biundingbox = False # 裁切image
        self.ratio_rate = 1
        self._current_index = _current_index
        self.image_lines_info = image_lines_info
        self.ratio_value = ratio_value
        self.current_index = 0
        self.lineColor = Qt.red # 0213 Sarah modify
        self.linesize = 2
        self.preview_lineColor = Qt.red # 預覽"直線"顏色
        self.preview_linesize = 2 # 預覽"直線"粗細
        self.boxColor = Qt.green
        self.boxsize = 2
        self.box_previewColor = Qt.white # 畫框輔助線顏色
        self.box_previewsize = 1 # 畫框輔助線粗細
        self.circle_previewColor = Qt.red # 畫預覽圓顏色
        self.circle_previewsize = 2 # 畫預覽圓粗細
        self.circle_Color = Qt.black # 畫圓顏色
        self.circle_size = 3 # 畫圓粗細
        self.angleColor = Qt.red
        self.labelColor = Qt.blue
        self.pointColor = Qt.black
        self.pointsize = 12
        self.measure_lable_size = measure_lable_size # label size
        self.update_pen_color()
        
    def update_pen_color(self):
        self.preview_linePen = QPen(self.preview_lineColor, self.preview_linesize, Qt.DashLine, Qt.RoundCap, Qt.RoundJoin)
        self.linePen = QPen(self.lineColor, self.linesize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        self.box_preview_Pen = QPen(self.box_previewColor, self.box_previewsize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        self.box_Pen = QPen(self.boxColor, self.boxsize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        self.circle_preview_pen = QPen(self.circle_previewColor, self.circle_previewsize, Qt.DashLine, Qt.RoundCap, Qt.RoundJoin)
        self.circle_pen = QPen(self.circle_Color, self.circle_size, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        self.point_pen = QPen(self.pointColor, self.pointsize, Qt.DotLine, Qt.RoundCap)

    def mousePressEvent(self, event):
        if (event.button() == Qt.LeftButton) and (self.point2point_fun or self.boundingbox_fun or self.Ellipse_diameter_fun):
            self.startPoint = event.pos()
        if (event.button() == Qt.LeftButton) and self.angle_fun:
            if self.drawline1event and not self.drawline2event:
                self.startPoint = event.pos()
            if not self.drawline1event  and self.drawline2event:
                self.drawline1event = True
        if (event.button() == Qt.LeftButton) and self.point2CC_fun:
            self.zoom_enable_status.emit(False)
            if self.drawpointevent:
                self.point_startPoint = event.pos()
            else:
                self.circle_startPoint = event.pos()
        if (event.button() == Qt.LeftButton) and self.line2CC_fun:
            self.zoom_enable_status.emit(False)
            if self.drawline_event:
                self.line_startPoint = event.pos()
            else:
                self.circle_startPoint = event.pos()
        if (event.button() == Qt.LeftButton) and self.line2point_fun:
            self.zoom_enable_status.emit(False)
            if self.drawline_event:
                self.line_startPoint = event.pos()
            else:
                self.startPoint = event.pos()
        if (event.button() == Qt.LeftButton) and self.CC2CC_fun:
            self.zoom_enable_status.emit(False)
            if self.drawcircle1_event:
                self.circle1_startPoint = event.pos()
            else:
                self.circle2_startPoint = event.pos()
        if (event.button() == Qt.LeftButton) and self.ConC_diameter_fun and self.drawcircle1_event:
            self.center = event.pos()
        if (event.button() == Qt.LeftButton) and self.radius_of_curv_fun:
            self.startPoint = event.pos()
            self.draw_curvPoint_index += 1
            
    def mouseMoveEvent(self, event):
        if self.boundingbox_fun:
            self.endPoint = event.pos() 
            self.update() 
        if self.point2point_fun: # Sarah moidify, 0118 if self.point2point_fun or self.boundingbox_fun:
            if self.startPoint:
                self.endPoint = event.pos() 
                self.update()  
        if self.angle_fun:
            if self.drawline1event:
                self.endPoint = event.pos() 
                self.update() 
            if self.drawline2event:
                self.line2endpoint = event.pos() 
                self.update() 
        if self.point2CC_fun:
            if not self.drawpointevent and self.circle_startPoint:
                self.circle_endPoint = event.pos() 
                line = QLineF(self.circle_startPoint, self.circle_endPoint)
                self.circle_centerPoint = QPoint((line.x1()+line.x2())/2, (line.y1()+line.y2())/2)
                self.circle_w = QPoint(self.circle_endPoint).x() - QPoint(self.circle_startPoint).x()
                self.circle_h = QPoint(self.circle_endPoint).y() - QPoint(self.circle_startPoint).y()
                self.update() 
        if self.line2CC_fun:
            # 畫線
            if self.drawline_event and not self.circle_startPoint:
                self.line_endPoint = event.pos() 
                self.update() 
            # 畫圓
            elif not self.drawline_event and self.circle_startPoint:
                self.circle_endPoint = event.pos()
                line = QLineF(self.circle_startPoint, self.circle_endPoint)
                self.circle_centerPoint = QPoint((line.x1()+line.x2())/2, (line.y1()+line.y2())/2)
                self.circle_w = QPoint(self.circle_endPoint).x() - QPoint(self.circle_startPoint).x()
                self.circle_h = QPoint(self.circle_endPoint).y() - QPoint(self.circle_startPoint).y()
                self.update() 
        if self.line2point_fun:
            if self.drawline_event and not self.startPoint and self.line_startPoint: # 畫線
                self.line_endPoint = event.pos() 
                self.update()
            elif not self.drawline_event:
                self.startPoint = event.pos()
                self.update()
        if self.CC2CC_fun:
            if self.drawcircle1_event and self.circle1_startPoint and not self.circle2_startPoint: # 第一個圓
                self.circle1_endPoint = event.pos() 
                line = QLineF(self.circle1_startPoint, self.circle1_endPoint)
                self.circle1_centerPoint = QPoint((line.x1()+line.x2())/2, (line.y1()+line.y2())/2)
                self.circle1_w = QPoint(self.circle1_endPoint).x() - QPoint(self.circle1_startPoint).x()
                self.circle1_h = QPoint(self.circle1_endPoint).y() - QPoint(self.circle1_startPoint).y()
                self.update()
            elif not self.drawcircle1_event and self.circle1_startPoint and self.circle2_startPoint: # 第二個圓,成立條件第一個圓事件結束,有第一個 & 第二個圓起點
                self.circle2_endPoint = event.pos()
                line = QLineF(self.circle2_startPoint, self.circle2_endPoint)
                self.circle2_centerPoint = QPoint((line.x1()+line.x2())/2, (line.y1()+line.y2())/2)
                self.circle2_w = QPoint(self.circle2_endPoint).x() - QPoint(self.circle2_startPoint).x()
                self.circle2_h = QPoint(self.circle2_endPoint).y() - QPoint(self.circle2_startPoint).y()
                self.update()
        if self.Ellipse_diameter_fun:
            self.endPoint = event.pos()
            if self.startPoint:
                line = QLineF(self.startPoint, self.endPoint)
                self.circle_centerPoint = QPoint((line.x1()+line.x2())/2, (line.y1()+line.y2())/2)
                self.circle_w = QPoint(self.endPoint).x() - QPoint(self.startPoint).x()
                self.circle_h = QPoint(self.endPoint).y() - QPoint(self.startPoint).y()
                self.update()
        if self.ConC_diameter_fun and self.center:
            r = (event.pos().x() - self.center.x()) ** 2 + (event.pos().y() - self.center.y()) ** 2
            self.radius = r ** 0.5 
            self.update()
            
    def mouseReleaseEvent(self, event):
        global calibration_scale

        if self.point2point_fun:
            if self.startPoint and self.endPoint:
                line = QLineF(self.startPoint, self.endPoint) 
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'point2point',
                    'points': line, 
                    'distance': round(line.length(), 2)/self.ratio_rate * calibration_scale, 
                    'zoom_ratio': self.ratio_rate,
                    'startPoint_nor_x': line.x1()/self.qpixmap.width()/self.ratio_rate,
                    'startPoint_nor_y': line.y1()/self.qpixmap.height()/self.ratio_rate,
                    'endPoint_nor_x': line.x2()/self.qpixmap.width()/self.ratio_rate,
                    'endPoint_nor_y': line.y2()/self.qpixmap.height()/self.ratio_rate,
                    'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                 'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                 'angleColor':self.angleColor},
                    'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                })
                self.update()
            self.startPoint = self.endPoint = None
            self.currentinfo_save_status.emit(False)
        elif self.boundingbox_fun:
            if self.startPoint and self.endPoint:
                box = QRect(self.startPoint, self.endPoint) 
                box_width = box.width()#/self.qpixmap.width()/self.ratio_rate
                box_height = box.height()#/self.qpixmap.width()/self.ratio_rate
                if (box_width > 0) or (box_height > 0): # 任一邊等於零不畫出來
                    self.image_lines_info[self._current_index]["paints"].append({
                        'func_type':'boundingbox',
                        'distance_width': box.width()/self.ratio_rate* calibration_scale, 
                        'distance_height': box.height()/self.ratio_rate* calibration_scale, 
                        'zoom_ratio': self.ratio_rate,
                        'startPoint_nor_x': box.topLeft().x()/self.qpixmap.width()/self.ratio_rate,
                        'startPoint_nor_y': box.topLeft().y()/self.qpixmap.height()/self.ratio_rate,
                        'nor_width': box_width/self.qpixmap.width()/self.ratio_rate,
                        'nor_height': box_height/self.qpixmap.height()/self.ratio_rate,
                        'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                    'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                    'angleColor':self.angleColor},
                        'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                    })
                    if self.save_biundingbox:
                        x = int(box.topLeft().x()/self.ratio_rate)
                        y = int(box.topLeft().y()/self.ratio_rate)
                        w = abs(int(box_width/self.ratio_rate))
                        h = abs(int(box_height/self.ratio_rate))
                        self.crop_img_and_save(x,y,w,h)
                    self.update()
            # self.setMouseTracking(False)
            self.startPoint = self.endPoint = None
            self.currentinfo_save_status.emit(False)
        elif self.angle_fun:
            if self.startPoint and self.endPoint:
                line = QLineF(self.startPoint, self.endPoint)
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'angle',
                    'line1': line, 
                    'line2': '', 
                    'angle_degree':'',
                    'zoom_ratio': self.ratio_rate,
                    'startPoint_nor_x': line.x1()/self.qpixmap.width()/self.ratio_rate,
                    'startPoint_nor_y': line.y1()/self.qpixmap.height()/self.ratio_rate,
                    'endPoint_nor_x': line.x2()/self.qpixmap.width()/self.ratio_rate,
                    'endPoint_nor_y': line.y2()/self.qpixmap.height()/self.ratio_rate,
                    'line2endPoint_nor_x': '',
                    'line2endPoint_nor_y': '',
                    'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                 'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                 'angleColor':self.angleColor},
                    'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                })
                self.line2startpoint = self.startPoint
                self.line2endpoint = self.endPoint
                self.drawline1event = False
                self.drawline2event = True
                self.startPoint = self.endPoint = None
                self.setMouseTracking(True) 
                self.update()
            elif self.line2startpoint and self.line2endpoint: 
                line = QLineF(self.line2startpoint, self.line2endpoint)
                self.image_lines_info[self._current_index]["paints"][-1]['line2'] = line
                self.image_lines_info[self._current_index]["paints"][-1]['line2endPoint_nor_x'] = line.x2()/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['line2endPoint_nor_y'] = line.y2()/self.qpixmap.height()/self.ratio_rate
                self.line2startpoint = self.line2endpoint = None
                self.drawline2event = False
                self.setMouseTracking(False) 
                self.currentinfo_save_status.emit(False)
                self.update()
        elif self.point2CC_fun:
            if self.drawpointevent:
                point = QPoint(self.point_startPoint)
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'point2CC',
                    'zoom_ratio': self.ratio_rate,
                    'startPoint_nor_x': point.x()/self.qpixmap.width()/self.ratio_rate,
                    'startPoint_nor_y': point.y()/self.qpixmap.height()/self.ratio_rate,
                    'circle_center_nor_x': '',
                    'circle_center_nor_y': '',
                    'circle_nor_w': '',
                    'circle_nor_h': '',
                    'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                 'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                 'angleColor':self.angleColor},
                    'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                })
                self.drawpointevent = False
            if self.circle_w: # 0210先進課發現如果畫圓時點一下立即放開就會報錯(self.circle_w=None),改成有值再進程式
                self.image_lines_info[self._current_index]["paints"][-1]['circle_center_nor_x'] = self.circle_centerPoint.x()/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle_center_nor_y'] = self.circle_centerPoint.y()/self.qpixmap.height()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle_nor_w'] = self.circle_w/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle_nor_h'] = self.circle_h/self.qpixmap.height()/self.ratio_rate
                line = QLineF(self.point_startPoint, self.circle_centerPoint) 
                self.image_lines_info[self._current_index]["paints"][-1]['distance'] = round(line.length(), 2)/self.ratio_rate * calibration_scale
                self.drawpointevent = True
                self.circle_startPoint = self.circle_endPoint = self.point_startPoint = None
                self.circle_w = self.circle_h = None
                self.zoom_enable_status.emit(True)
                self.currentinfo_save_status.emit(False)
            self.update()
        elif self.line2CC_fun:
            if self.drawline_event and self.line_startPoint and self.line_endPoint:
                start_point = QPoint(self.line_startPoint)
                end_point = QPoint(self.line_endPoint)
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'line2CC',
                    'zoom_ratio': self.ratio_rate,
                    'startPoint_nor_x': start_point.x()/self.qpixmap.width()/self.ratio_rate,
                    'startPoint_nor_y': start_point.y()/self.qpixmap.height()/self.ratio_rate,
                    'endPoint_nor_x': end_point.x()/self.qpixmap.width()/self.ratio_rate,
                    'endPoint_nor_y': end_point.y()/self.qpixmap.height()/self.ratio_rate,
                    'circle_center_nor_x': '',
                    'circle_center_nor_y': '',
                    'circle_nor_w': '',
                    'circle_nor_h': '',
                    'intersection_nor_x': '',
                    'intersection_nor_y': '',
                    'distance': '',
                    'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                 'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                 'angleColor':self.angleColor},
                    'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                })
                self.drawline_event = False
                
            elif not self.drawline_event and self.circle_startPoint and self.circle_endPoint:
                self.image_lines_info[self._current_index]["paints"][-1]['circle_center_nor_x'] = self.circle_centerPoint.x()/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle_center_nor_y'] = self.circle_centerPoint.y()/self.qpixmap.height()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle_nor_w'] = self.circle_w/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle_nor_h'] = self.circle_h/self.qpixmap.height()/self.ratio_rate
                p1 = [QPoint(self.line_startPoint).x(), QPoint(self.line_startPoint).y()] # line起始點
                p2  = [QPoint(self.line_endPoint).x(), QPoint(self.line_endPoint).y()] # line終點
                p3 = [self.circle_centerPoint.x(), self.circle_centerPoint.y()] # 圓心
                line2CC_dis = self.point_2_line_distance(p1,p2,p3)
                p4x, p4y = self.get_point2line_intersection(p1,p2,p3)
                self.image_lines_info[self._current_index]["paints"][-1]['intersection_nor_x'] = p4x/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['intersection_nor_y'] = p4y/self.qpixmap.height()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['distance'] = round(line2CC_dis, 2)/self.ratio_rate * calibration_scale
                self.drawline_event = True
                self.line_startPoint = self.line_endPoint = None
                self.circle_startPoint = self.circle_endPoint = None
                self.circle_w = self.circle_h = None
                self.zoom_enable_status.emit(True)
                self.currentinfo_save_status.emit(False)
            self.update()
        elif self.line2point_fun:
            if self.drawline_event and self.line_startPoint and self.line_endPoint:
                line_start_point = QPoint(self.line_startPoint)
                line_end_point = QPoint(self.line_endPoint)
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'line2point',
                    'zoom_ratio': self.ratio_rate,
                    'line_startPoint_nor_x': line_start_point.x()/self.qpixmap.width()/self.ratio_rate,
                    'line_startPoint_nor_y': line_start_point.y()/self.qpixmap.height()/self.ratio_rate,
                    'line_endPoint_nor_x': line_end_point.x()/self.qpixmap.width()/self.ratio_rate,
                    'line_endPoint_nor_y': line_end_point.y()/self.qpixmap.height()/self.ratio_rate,
                    'startPoint_nor_x': '',
                    'startPoint_nor_y': '',
                    'intersectionPoint_nor_x': '',
                    'intersectionPoint_nor_y': '',
                    'distance': '',
                    'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                 'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                 'angleColor':self.angleColor},
                    'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                })
                self.drawline_event = False
                self.setMouseTracking(True) 
            elif not self.drawline_event and self.startPoint:
                point = QPoint(self.startPoint)
                self.image_lines_info[self._current_index]["paints"][-1]['startPoint_nor_x'] = point.x()/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['startPoint_nor_y'] = point.y()/self.qpixmap.height()/self.ratio_rate
                p1 = [QPoint(self.line_startPoint).x(), QPoint(self.line_startPoint).y()] # line起始點
                p2 = [QPoint(self.line_endPoint).x(), QPoint(self.line_endPoint).y()] # line終點
                p3 = [point.x(), point.y()] # 點
                line2point_dis = self.point_2_line_distance(p1,p2,p3)
                p4x, p4y = self.get_point2line_intersection(p1,p2,p3)
                self.image_lines_info[self._current_index]["paints"][-1]['distance'] = round(line2point_dis, 2)/self.ratio_rate * calibration_scale
                self.image_lines_info[self._current_index]["paints"][-1]['intersectionPoint_nor_x'] = p4x/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['intersectionPoint_nor_y'] = p4y/self.qpixmap.height()/self.ratio_rate
                self.drawline_event = True
                self.line_startPoint = self.line_endPoint = None
                self.startPoint = None
                self.zoom_enable_status.emit(True)
                self.currentinfo_save_status.emit(False)
                self.setMouseTracking(False) 
            self.update()
        elif self.CC2CC_fun:
            if self.drawcircle1_event and self.circle1_startPoint and self.circle1_endPoint and not self.circle2_startPoint: # 第一個圓
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'CC2CC',
                    'zoom_ratio': self.ratio_rate,
                    'circle1_center_nor_x': self.circle1_centerPoint.x()/self.qpixmap.width()/self.ratio_rate,
                    'circle1_center_nor_y': self.circle1_centerPoint.y()/self.qpixmap.height()/self.ratio_rate,
                    'circle1_nor_w': self.circle1_w/self.qpixmap.width()/self.ratio_rate,
                    'circle1_nor_h': self.circle1_h/self.qpixmap.height()/self.ratio_rate,
                    'circle2_center_nor_x': '',
                    'circle2_center_nor_y': '',
                    'circle2_nor_w': '',
                    'circle2_nor_h': '',
                    'distance': '',
                    'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                 'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                 'angleColor':self.angleColor},
                    'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                })
                self.drawcircle1_event = False
            elif not self.drawcircle1_event and self.circle1_startPoint and self.circle1_endPoint and self.circle2_startPoint and self.circle2_endPoint: # 第二個圓
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_center_nor_x'] = self.circle2_centerPoint.x()/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_center_nor_y'] = self.circle2_centerPoint.y()/self.qpixmap.height()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_nor_w'] = self.circle2_w/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_nor_h'] = self.circle2_h/self.qpixmap.height()/self.ratio_rate
                line = QLineF(self.circle1_centerPoint, self.circle2_centerPoint)
                self.image_lines_info[self._current_index]["paints"][-1]['distance'] = line.length()/self.ratio_rate * calibration_scale

                self.drawcircle1_event = True
                self.circle1_startPoint = self.circle1_endPoint = None
                self.circle2_startPoint = self.circle2_endPoint = None
                self.zoom_enable_status.emit(True)
                self.currentinfo_save_status.emit(False)
            self.update()
        elif self.Ellipse_diameter_fun:
            if self.startPoint and self.endPoint:
                diameter_rightPoint = QPoint(self.circle_centerPoint.x() + self.circle_w, self.circle_centerPoint.y())
                diameter_leftPoint = QPoint(self.circle_centerPoint.x() - self.circle_w, self.circle_centerPoint.y())
                diameter_topPoint = QPoint(self.circle_centerPoint.x(), self.circle_centerPoint.y() - self.circle_h)
                diameter_bottomPoint = QPoint(self.circle_centerPoint.x(), self.circle_centerPoint.y() + self.circle_h)
                label_post_a = QPoint(int((self.circle_centerPoint.x()+diameter_rightPoint.x())/2), int((self.circle_centerPoint.y()+diameter_rightPoint.y())/2)) - QPoint(-5, 10)
                label_post_b = QPoint(int((self.circle_centerPoint.x()+diameter_topPoint.x())/2), int((self.circle_centerPoint.y()+diameter_topPoint.y())/2)) - QPoint(-5, 10)
                line_a = QLineF(diameter_rightPoint, diameter_leftPoint)
                line_b = QLineF(diameter_topPoint, diameter_bottomPoint)
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'Ellipse_diameter',
                    'zoom_ratio': self.ratio_rate,
                    'circle_center_nor_x': self.circle_centerPoint.x()/self.qpixmap.width()/self.ratio_rate,
                    'circle_center_nor_y': self.circle_centerPoint.y()/self.qpixmap.height()/self.ratio_rate,
                    'circle_nor_w': self.circle_w/self.qpixmap.width()/self.ratio_rate,
                    'circle_nor_h': self.circle_h/self.qpixmap.height()/self.ratio_rate,
                    'diameter_right_nor':[(self.circle_centerPoint.x() + self.circle_w)/self.qpixmap.width()/self.ratio_rate, self.circle_centerPoint.y()/self.qpixmap.height()/self.ratio_rate],
                    'diameter_left_nor':[(self.circle_centerPoint.x() - self.circle_w)/self.qpixmap.width()/self.ratio_rate, self.circle_centerPoint.y()/self.qpixmap.height()/self.ratio_rate],
                    'diameter_top_nor':[self.circle_centerPoint.x()/self.qpixmap.width()/self.ratio_rate, (self.circle_centerPoint.y() - self.circle_h)/self.qpixmap.height()/self.ratio_rate],
                    'diameter_bottom_nor':[self.circle_centerPoint.x()/self.qpixmap.width()/self.ratio_rate, (self.circle_centerPoint.y() + self.circle_h)/self.qpixmap.height()/self.ratio_rate],
                    'label_position_a_nor':[label_post_a.x()/self.qpixmap.width()/self.ratio_rate, label_post_a.y()/self.qpixmap.height()/self.ratio_rate],
                    'label_position_b_nor':[label_post_b.x()/self.qpixmap.width()/self.ratio_rate, label_post_b.y()/self.qpixmap.height()/self.ratio_rate],
                    'diameter_a': line_a.length()/self.ratio_rate * calibration_scale,
                    'diameter_b': line_b.length()/self.ratio_rate * calibration_scale,
                    'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                 'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                 'angleColor':self.angleColor},
                    'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                })
                self.startPoint = self.endPoint = None
                self.currentinfo_save_status.emit(False)
                self.update()
        elif self.ConC_diameter_fun:
            if self.drawcircle1_event and self.radius: # 第一個圓
                right_point = QPoint(int(self.center.x()+self.radius), self.center.y())
                left_point = QPoint(int(self.center.x()-self.radius), self.center.y())
                label_post = QPoint(int((right_point.x()+left_point.x())/2), int((right_point.y()+left_point.y())/2)) - QPoint(0, 10)
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'ConC_diameter',
                    'zoom_ratio': self.ratio_rate,
                    'circle_center_nor_x': self.center.x()/self.qpixmap.width()/self.ratio_rate,
                    'circle_center_nor_y': self.center.y()/self.qpixmap.height()/self.ratio_rate,
                    'circle1_nor_w': self.radius/self.qpixmap.width()/self.ratio_rate,
                    'circle1_nor_h': self.radius/self.qpixmap.height()/self.ratio_rate,
                    'circle1_nor_right': [right_point.x()/self.qpixmap.width()/self.ratio_rate, right_point.y()/self.qpixmap.height()/self.ratio_rate],
                    'circle1_nor_left': [left_point.x()/self.qpixmap.width()/self.ratio_rate, left_point.y()/self.qpixmap.height()/self.ratio_rate],
                    'circle2_nor_w': '',
                    'circle2_nor_h': '',
                    'circle2_nor_right': '',
                    'circle2_nor_left': '',
                    'circle2_nor_stretch_right': '',
                    'circle2_nor_stretch_left': '',
                    'circle1_diameter': self.radius/self.ratio_rate * calibration_scale*2,
                    'circle2_diameter': '',
                    'label_position_circle1_nor':[label_post.x()/self.qpixmap.width()/self.ratio_rate, label_post.y()/self.qpixmap.height()/self.ratio_rate],
                    'label_position_circle2_nor':'',
                    'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                 'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                 'angleColor':self.angleColor},
                    'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                })
                self.drawcircle1_event = False
                self.setMouseTracking(True)
                self.zoom_enable_status.emit(False)
            elif not self.drawcircle1_event and self.radius and self.center: # 第二個圓
                # 確認結束點在圓內或圓內
                right_point = QPoint(self.center.x()+self.radius, self.center.y())
                left_point = QPoint(self.center.x()-self.radius, self.center.y())
                stretch_right_dottom_point = QPoint((self.center.x()+self.radius), int(self.center.y()+self.radius)) # 右邊延伸虛線
                stretch_left_dottom_point = QPoint((self.center.x()-self.radius), int(self.center.y()+self.radius)) # 左邊延伸虛線
                label_post = QPoint(int((stretch_right_dottom_point.x()+stretch_left_dottom_point.x())/2), int((stretch_right_dottom_point.y()+stretch_left_dottom_point.y())/2)) - QPoint(0, 10)
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_diameter'] = self.radius/self.ratio_rate * calibration_scale*2
                self.image_lines_info[self._current_index]["paints"][-1]['label_position_circle2_nor'] = [label_post.x()/self.qpixmap.width()/self.ratio_rate, label_post.y()/self.qpixmap.height()/self.ratio_rate]
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_nor_w'] = self.radius/self.qpixmap.width()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_nor_h'] = self.radius/self.qpixmap.height()/self.ratio_rate
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_nor_right'] = [right_point.x()/self.qpixmap.width()/self.ratio_rate, right_point.y()/self.qpixmap.height()/self.ratio_rate]
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_nor_left'] = [left_point.x()/self.qpixmap.width()/self.ratio_rate, left_point.y()/self.qpixmap.height()/self.ratio_rate]
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_nor_stretch_right'] = [stretch_right_dottom_point.x()/self.qpixmap.width()/self.ratio_rate, stretch_right_dottom_point.y()/self.qpixmap.height()/self.ratio_rate]
                self.image_lines_info[self._current_index]["paints"][-1]['circle2_nor_stretch_left'] = [stretch_left_dottom_point.x()/self.qpixmap.width()/self.ratio_rate, stretch_left_dottom_point.y()/self.qpixmap.height()/self.ratio_rate]
                self.drawcircle1_event = True
                self.setMouseTracking(False)
                self.center = self.radius = None
                self.zoom_enable_status.emit(True)
            self.update()
        elif self.radius_of_curv_fun and self.startPoint and (self.draw_curvPoint_index != 0):
            if self.draw_curvPoint_index == 1:
                self.image_lines_info[self._current_index]["paints"].append({
                    'func_type':'radius_of_curv',
                    'zoom_ratio': self.ratio_rate,
                    'curv1Point_nor': [self.startPoint.x()/self.qpixmap.width()/self.ratio_rate, self.startPoint.y()/self.qpixmap.height()/self.ratio_rate],
                    'curv2Point_nor': '',
                    'curv3Point_nor': '',
                    'circle_center_nor': '',
                    'circle_nor_w': '',
                    'circle_nor_h': '',
                    'circle_radius': '',
                    'label_position_nor':'',
                    'color_pen':{'preview_linePen':self.preview_linePen, 'linePen':self.linePen, 'box_preview_Pen':self.box_preview_Pen,
                                 'box_Pen':self.box_Pen, 'circle_preview_pen':self.circle_preview_pen, 'circle_pen':self.circle_pen, 'point_pen':self.point_pen, 'labelColor':self.labelColor,
                                 'angleColor':self.angleColor},
                    'measure_time': time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                })
            else:
                self.image_lines_info[self._current_index]["paints"][-1][f'curv{self.draw_curvPoint_index:d}Point_nor'] = [self.startPoint.x()/self.qpixmap.width()/self.ratio_rate, self.startPoint.y()/self.qpixmap.height()/self.ratio_rate]
                if self.draw_curvPoint_index == 3:
                    self.draw_curvPoint_index = 0
                    self.startPoint = None
            self.update()
     
    def paintEvent(self, event): # event.type()=12 :screen update necessary(QPaintEvent)
        global calibration_scale_unit
        
        painter = QPainter(self)
        painter.scale(self.ratio_rate, self.ratio_rate)
        painter.setRenderHint(painter.Antialiasing) # 抗鋸齒(狗牙)

        painter.drawPixmap(self.rect(), self.qpixmap, self.rect())

        fm = QFontMetrics(QFont())
        txt_height = fm.height()

        # 設定lable字符串大小
        self.label_font = QFont()
        self.label_font.setPointSize(self.measure_lable_size)
        painter.setFont(self.label_font)
        
        drawpainter = QPainter(self.image)
        if self.point2point_fun:
            painter.setPen(self.preview_linePen)
            if self.startPoint and self.endPoint: # 拉線時預覽線條
                painter.drawLine(self.startPoint/self.ratio_rate, self.endPoint/self.ratio_rate)
                line = QLineF(self.startPoint, self.endPoint)
                line_center_post = QPoint(int((QPoint(self.startPoint).x()+QPoint(self.endPoint).x())/2), int((QPoint(self.startPoint).y()+QPoint(self.endPoint).y())/2)) - QPoint(0, 10)
                painter.setPen(self.labelColor)
                painter.drawText(line_center_post, '%.3f %s'%(round(line.length(), 3)/self.ratio_rate * calibration_scale,calibration_scale_unit))
                self.update()
        elif self.boundingbox_fun:
            painter.setPen(self.box_preview_Pen)
            if self.endPoint:
                painter.drawLine(QPoint(painter.window().left(), self.endPoint.y())/self.ratio_rate, QPoint(painter.window().right(), self.endPoint.y())/self.ratio_rate)
                painter.drawLine(QPoint(self.endPoint.x(), painter.window().top())/self.ratio_rate, QPoint(self.endPoint.x(), painter.window().bottom())/self.ratio_rate)
                self.update()
            if self.startPoint and self.endPoint: 
                painter.drawRect(QRect(self.startPoint/self.ratio_rate, self.endPoint/self.ratio_rate).normalized())
                box = QRect(self.startPoint, self.endPoint) 
                box_width = box.width()
                box_height = box.height()
                x = int(box.topLeft().x())
                y = int(box.topLeft().y())
                w = abs(int(box_width))
                h = abs(int(box_height))
                txt_width = fm.width('%.3f %s'%(box_width,calibration_scale_unit))
                box_width_txt_x_post = x + int((w-txt_width)/2) if w-txt_width > 0 else x
                box_height_txt_y_post = y + int((h-txt_height)/2) if h-txt_height > 0 else y
                width_center_post = QPoint(box_width_txt_x_post, y) - QPoint(0, 5) # 長標籤放上面
                height_center_post = QPoint(x + w, box_height_txt_y_post) + QPoint(5, 0) # 高標籤放右邊
                pixel_width_center_post = QPoint(box_width_txt_x_post, y) - QPoint(0, 20) # 0928 Sarah add, 長標籤放上面 for pixel
                pixel_height_center_post = QPoint(x + w, box_height_txt_y_post) + QPoint(5, -15) # 0928 Sarah add, 高標籤放右邊 for pixel
                painter.setPen(self.labelColor)
                painter.drawText(width_center_post, '%.2f %s'%(round(box_width, 2)/self.ratio_rate * calibration_scale,calibration_scale_unit))
                painter.drawText(height_center_post, '%.2f %s'%(round(box_height, 2)/self.ratio_rate * calibration_scale,calibration_scale_unit))
                painter.drawText(pixel_width_center_post, '%d %s'%(round(box_width, 2)/self.ratio_rate,'pixels')) # 0928 Sarah add, for pixel value
                painter.drawText(pixel_height_center_post, '%d %s'%(round(box_height, 2)/self.ratio_rate,'pixels')) # 0928 Sarah add, for pixel value
                self.update()
        elif self.angle_fun:
            painter.setPen(self.preview_linePen)
            if self.drawline1event and self.endPoint and self.startPoint:
                painter.drawLine(self.startPoint/self.ratio_rate, self.endPoint/self.ratio_rate) # 底邊Line1
                self.angle_line1 = [self.startPoint.x(),self.startPoint.y(),self.endPoint.x(),self.endPoint.y()]
                self.update()
            if self.drawline2event and self.line2endpoint:
                painter.drawLine(self.line2startpoint/self.ratio_rate, self.line2endpoint/self.ratio_rate) # 頂邊Line2
                Angle = self.GetAngle(self.angle_line1, [self.line2startpoint.x(), self.line2startpoint.y(), self.line2endpoint.x(), self.line2endpoint.y()])
                angle_pos = QPoint(self.line2startpoint.x(), self.line2startpoint.y()) + QPoint(15, -10)
                painter.setPen(self.labelColor)
                painter.drawText(angle_pos, '%.2f°'%(Angle))
                self.update()
        elif self.point2CC_fun:
            if not self.drawpointevent and self.circle_startPoint and self.circle_endPoint: # 畫圈預覽
                painter.setPen(self.circle_preview_pen)
                painter.drawEllipse(self.circle_centerPoint/self.ratio_rate, self.circle_w/self.ratio_rate, self.circle_h/self.ratio_rate)
                painter.setPen(self.preview_linePen)
                painter.drawLine(self.point_startPoint/self.ratio_rate, self.circle_centerPoint/self.ratio_rate) # point到圓心
                point2CC_dis = round(QLineF(self.point_startPoint/self.ratio_rate, self.circle_centerPoint/self.ratio_rate).length(), 2)/self.ratio_rate * calibration_scale
                label_post = QPoint((self.circle_centerPoint.x()/self.ratio_rate+self.point_startPoint.x()/self.ratio_rate)/2, (self.circle_centerPoint.y()/self.ratio_rate+self.point_startPoint.y()/self.ratio_rate)/2) - QPoint(0, 10)
                painter.setPen(self.labelColor)
                painter.drawText(label_post, '%.3f %s'%(point2CC_dis,calibration_scale_unit))
                self.update()
        elif self.line2CC_fun:
            if self.drawline_event and self.line_startPoint and self.line_endPoint:
                painter.setPen(self.preview_linePen)
                painter.drawLine(self.line_startPoint/self.ratio_rate, self.line_endPoint/self.ratio_rate)
                self.update()
            if not self.drawline_event and self.circle_startPoint and self.circle_endPoint: # 畫圈預覽
                painter.setPen(self.circle_preview_pen)
                painter.drawEllipse(self.circle_centerPoint/self.ratio_rate, self.circle_w/self.ratio_rate, self.circle_h/self.ratio_rate)
                p1 = [QPoint(self.line_startPoint).x(), QPoint(self.line_startPoint).y()] # line起始點
                p2  = [QPoint(self.line_endPoint).x(), QPoint(self.line_endPoint).y()] # line終點
                p3 = [int(self.circle_centerPoint.x()), int(self.circle_centerPoint.y())] # 圓心
                p4x, p4y = self.get_point2line_intersection(p1,p2,p3)
                painter.setPen(self.preview_linePen)
                painter.drawLine(self.circle_centerPoint/self.ratio_rate, QPoint(p4x, p4y)/self.ratio_rate) # 交匯point到圓心
                line2CC_dis = round(self.point_2_line_distance(p1,p2,p3), 2)/self.ratio_rate * calibration_scale
                label_post = QPoint((self.circle_centerPoint.x()/self.ratio_rate+p4x)/2, (self.circle_centerPoint.y()/self.ratio_rate+p4y)/2) - QPoint(0, 10)
                painter.setPen(self.labelColor)
                painter.drawText(label_post, '%.3f %s'%(line2CC_dis,calibration_scale_unit))
                self.update()
        elif self.line2point_fun:
            painter.setPen(self.preview_linePen)
            if self.drawline_event and self.line_startPoint and self.line_endPoint:
                painter.drawLine(self.line_startPoint/self.ratio_rate, self.line_endPoint/self.ratio_rate)
                self.update()
            elif not self.drawline_event and self.startPoint:
                p1 = [QPoint(self.line_startPoint).x(), QPoint(self.line_startPoint).y()] # line起始點
                p2 = [QPoint(self.line_endPoint).x(), QPoint(self.line_endPoint).y()] # line終點
                p3 = [QPoint(self.startPoint).x(), QPoint(self.startPoint).y()] # 點
                line2point_dis = round(self.point_2_line_distance(p1,p2,p3), 2)/self.ratio_rate * calibration_scale
                p4x, p4y = self.get_point2line_intersection(p1,p2,p3)
                painter.drawLine(self.startPoint/self.ratio_rate, QPoint(p4x, p4y)/self.ratio_rate)
                label_post = QPoint((self.startPoint.x()/self.ratio_rate+p4x/self.ratio_rate)/2, (self.startPoint.y()/self.ratio_rate+p4y/self.ratio_rate)/2) - QPoint(0, 10)
                painter.setPen(self.labelColor)
                painter.drawText(label_post, '%.3f %s'%(line2point_dis,calibration_scale_unit))
                self.update()
        elif self.CC2CC_fun:
            painter.setPen(self.circle_preview_pen)
            if self.drawcircle1_event and self.circle1_startPoint and self.circle1_endPoint and not self.circle2_startPoint:
                painter.drawEllipse(self.circle1_centerPoint/self.ratio_rate, self.circle1_w/self.ratio_rate, self.circle1_h/self.ratio_rate)
                self.update()
            elif not self.drawcircle1_event and self.circle1_startPoint and self.circle1_endPoint and self.circle2_startPoint and self.circle2_endPoint: # 第二個圓
                painter.drawEllipse(self.circle2_centerPoint/self.ratio_rate, self.circle2_w/self.ratio_rate, self.circle2_h/self.ratio_rate)
                painter.setPen(self.preview_linePen)
                painter.drawLine(self.circle1_centerPoint/self.ratio_rate, self.circle2_centerPoint/self.ratio_rate)
                painter.setPen(self.labelColor)
                line = QLineF(self.circle1_centerPoint, self.circle2_centerPoint)
                label_post = QPoint((self.circle1_centerPoint.x()/self.ratio_rate+self.circle2_centerPoint.x()/self.ratio_rate)/2, (self.circle1_centerPoint.y()/self.ratio_rate+self.circle2_centerPoint.y()/self.ratio_rate)/2) - QPoint(0, 10)
                CC2CC_dis = round(line.length(), 2)/self.ratio_rate * calibration_scale
                painter.drawText(label_post, '%.3f %s'%(CC2CC_dis,calibration_scale_unit))
                self.update()
        elif self.Ellipse_diameter_fun:
            if self.startPoint and self.endPoint:
                painter.setPen(self.circle_preview_pen)
                painter.drawEllipse(self.circle_centerPoint/self.ratio_rate, self.circle_w/self.ratio_rate, self.circle_h/self.ratio_rate)
                painter.setPen(self.preview_linePen)
                diameter_rightPoint = QPoint(int((self.circle_centerPoint.x() + self.circle_w)/self.ratio_rate), int(self.circle_centerPoint.y()/self.ratio_rate))
                diameter_leftPoint = QPoint(int((self.circle_centerPoint.x() - self.circle_w)/self.ratio_rate), int(self.circle_centerPoint.y()/self.ratio_rate))
                diameter_topPoint = QPoint(int(self.circle_centerPoint.x()/self.ratio_rate), int((self.circle_centerPoint.y() - self.circle_h)/self.ratio_rate))
                diameter_bottomPoint = QPoint(int(self.circle_centerPoint.x()/self.ratio_rate), int((self.circle_centerPoint.y() + self.circle_h)/self.ratio_rate))
                painter.drawLine(diameter_rightPoint, diameter_leftPoint)
                painter.drawLine(diameter_topPoint, diameter_bottomPoint)
                painter.setPen(self.labelColor)
                label_post_a = QPoint(int((self.circle_centerPoint.x()/self.ratio_rate+diameter_rightPoint.x())/2), int((self.circle_centerPoint.y()/self.ratio_rate+diameter_rightPoint.y())/2)) - QPoint(-5, 10)
                label_post_b = QPoint(int((self.circle_centerPoint.x()/self.ratio_rate+diameter_topPoint.x())/2), int((self.circle_centerPoint.y()/self.ratio_rate+diameter_topPoint.y())/2)) - QPoint(-5, 10)
                diameter_a_dis = round(self.circle_w*2, 2)/self.ratio_rate * calibration_scale
                diameter_b_dis = round(self.circle_h*2, 2)/self.ratio_rate * calibration_scale
                painter.drawText(label_post_a, 'a=%.3f %s'%(diameter_a_dis,calibration_scale_unit))
                painter.drawText(label_post_b, 'b=%.3f %s'%(diameter_b_dis,calibration_scale_unit))
                self.update()
        elif self.ConC_diameter_fun:
            painter.setPen(self.circle_preview_pen)
            if self.center and self.radius and self.drawcircle1_event:
                painter.drawEllipse(self.center/self.ratio_rate, self.radius/self.ratio_rate, self.radius/self.ratio_rate)
                painter.setPen(self.preview_linePen)
                right_point = QPoint(int((self.center.x()+self.radius)/self.ratio_rate), self.center.y()/self.ratio_rate)
                left_point = QPoint(int((self.center.x()-self.radius)/self.ratio_rate), self.center.y()/self.ratio_rate)
                painter.drawLine(right_point, left_point) # 第一圈直徑
                label_post = QPoint(int((right_point.x()+left_point.x())/2), int((right_point.y()+left_point.y())/2)) - QPoint(0, 10)
                painter.drawText(label_post, '%.3f %s'%((self.radius*2)/self.ratio_rate * calibration_scale,calibration_scale_unit))
                self.update()
            elif self.center and self.radius and not self.drawcircle1_event:
                painter.drawEllipse(self.center/self.ratio_rate, self.radius/self.ratio_rate, self.radius/self.ratio_rate)
                painter.setPen(self.preview_linePen)
                right_point = QPoint(int((self.center.x()+self.radius)/self.ratio_rate), int((self.center.y()+self.radius)/self.ratio_rate))
                left_point = QPoint(int((self.center.x()-self.radius)/self.ratio_rate), int((self.center.y()+self.radius)/self.ratio_rate))
                painter.drawLine(right_point, left_point) # 第二圈直徑
                stretch_right_top_point = QPoint(int((self.center.x()+self.radius)/self.ratio_rate), int(self.center.y()/self.ratio_rate))
                stretch_right_dottom_point = QPoint(int((self.center.x()+self.radius)/self.ratio_rate), int((self.center.y()+self.radius)/self.ratio_rate))
                painter.drawLine(stretch_right_top_point, stretch_right_dottom_point) # 右邊延伸虛線
                stretch_left_top_point = QPoint(int((self.center.x()-self.radius)/self.ratio_rate), int(self.center.y()/self.ratio_rate))
                stretch_left_dottom_point = QPoint(int((self.center.x()-self.radius)/self.ratio_rate), int((self.center.y()+self.radius)/self.ratio_rate))
                painter.drawLine(stretch_left_top_point, stretch_left_dottom_point) # 左邊延伸虛線
                label_post = QPoint(int((stretch_right_dottom_point.x()+stretch_left_dottom_point.x())/2), int((stretch_right_dottom_point.y()+stretch_left_dottom_point.y())/2)) - QPoint(0, 10)
                painter.drawText(label_post, '%.3f %s'%((self.radius*2)/self.ratio_rate * calibration_scale,calibration_scale_unit))
                self.update()
        elif self.radius_of_curv_fun and (self.draw_curvPoint_index == 2):
            pass

        if self.image_lines_info[self._current_index]["paints"]:
            # painter一次建立兩個，一個顯示在螢幕上，另一個對原圖畫圖，感覺很吃效能...暫時無解
            for paint_Data in self.image_lines_info[self._current_index]["paints"]:    
                if paint_Data['func_type'] == 'point2point':
                    painter.setPen(paint_Data['color_pen']['linePen']) 
                    drawpainter.setPen(paint_Data['color_pen']['linePen'])
                    x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                    y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                    w = int(self.qpixmap.width()*paint_Data['endPoint_nor_x'])
                    h = int(self.qpixmap.height()*paint_Data['endPoint_nor_y'])
                    pos_list = [x, y, w, h]
                    other_p2p_post_list = []
                    for p in self.image_lines_info[self._current_index]["paints"]:
                        if p['func_type'] == 'point2point':
                            x_ = int(self.qpixmap.width()*p['startPoint_nor_x'])
                            y_ = int(self.qpixmap.height()*p['startPoint_nor_y'])
                            w_ = int(self.qpixmap.width()*p['endPoint_nor_x'])
                            h_ = int(self.qpixmap.height()*p['endPoint_nor_y'])
                            pos_list_ = [x_, y_, w_, h_]
                            if pos_list != pos_list_:
                                other_p2p_post_list.append(pos_list_)
                    painter.drawLine(x, y, w, h)
                    drawpainter.drawLine(x, y, w, h)
                    painter.setPen(paint_Data['color_pen']['labelColor'])
                    drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                    intersects_result = any([(lambda x: self.line_intersects_judge(pos_list,x))(x) for x in other_p2p_post_list])
                    line_center_post = QPoint(int((x+w)/2), int((y+h)/2)) - QPoint(0, 10) # 正常標籤會放在線的上中方
                    # 如果目前的line線條和其他P2P線條重疊,把標籤丟到線段尾端
                    if intersects_result:
                        line_center_post = QPoint(w, h) + QPoint(5, 0)
                    painter.drawText(line_center_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                    drawpainter.drawText(line_center_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))

                elif paint_Data['func_type'] == 'boundingbox':
                    painter.setPen(paint_Data['color_pen']['linePen'])
                    drawpainter.setPen(paint_Data['color_pen']['linePen'])
                    x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                    y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                    w = int(self.qpixmap.width()*paint_Data['nor_width'])
                    h = int(self.qpixmap.height()*paint_Data['nor_height'])
                    painter.setPen(paint_Data['color_pen']['box_Pen'])
                    drawpainter.setPen(paint_Data['color_pen']['box_Pen'])
                    painter.drawRect(x, y, w, h)
                    drawpainter.drawRect(x, y, w, h)
                    painter.setPen(paint_Data['color_pen']['labelColor'])
                    drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                    txt_width = fm.width('%.3f %s'%(paint_Data['distance_width'],calibration_scale_unit))
                    box_width_txt_x_post = x + int((w-txt_width)/2) if w-txt_width > 0 else x
                    box_height_txt_y_post = y + int((h-txt_height)/2) if h-txt_height > 0 else y
                    width_center_post = QPoint(box_width_txt_x_post, y) - QPoint(0, 5) # 長標籤放上面
                    height_center_post = QPoint(x + w, box_height_txt_y_post) + QPoint(5, 0) # 高標籤放右邊
                    pixel_width_center_post = QPoint(box_width_txt_x_post, y) - QPoint(0, 20) # 0928 Sarah add, 長標籤放上面 for display pixel
                    pixel_height_center_post = QPoint(x + w, box_height_txt_y_post) + QPoint(5, -15) # 0928 Sarah add, 高標籤放右邊 for display pixel
                    painter.drawText(width_center_post, '%.2f %s'%(paint_Data['distance_width'],calibration_scale_unit))
                    painter.drawText(height_center_post, '%.2f %s'%(paint_Data['distance_height'],calibration_scale_unit))
                    painter.drawText(pixel_width_center_post, '%d %s'%(int(paint_Data['distance_width'] / calibration_scale),'pixels')) # 0928 Sarah add, for display pixel
                    painter.drawText(pixel_height_center_post, '%d %s'%(int(paint_Data['distance_height'] / calibration_scale),'pixels'))# 0928 Sarah add, for display pixel
                    drawpainter.drawText(width_center_post, '%.2f %s'%(paint_Data['distance_width'],calibration_scale_unit))
                    drawpainter.drawText(height_center_post, '%.2f %s'%(paint_Data['distance_height'],calibration_scale_unit))
                    drawpainter.drawText(pixel_width_center_post, '%d %s'%(int(paint_Data['distance_width'] / calibration_scale),'pixels'))# 0928 Sarah add, for display pixel
                    drawpainter.drawText(pixel_height_center_post, '%d %s'%(int(paint_Data['distance_height'] / calibration_scale),'pixels'))# 0928 Sarah add, for display pixel
                elif paint_Data['func_type'] == 'angle':
                    painter.setPen(paint_Data['color_pen']['linePen'])
                    drawpainter.setPen(paint_Data['color_pen']['linePen'])
                    for i in range(1,3):
                        if paint_Data['line%d'%i]:
                            if i == 1:
                                x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                                y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                                w = int(self.qpixmap.width()*paint_Data['endPoint_nor_x'])
                                h = int(self.qpixmap.height()*paint_Data['endPoint_nor_y'])
                                line1 = [x, y, w, h]
                            elif i == 2:
                                x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                                y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                                w = int(self.qpixmap.width()*paint_Data['line2endPoint_nor_x'])
                                h = int(self.qpixmap.height()*paint_Data['line2endPoint_nor_y'])
                                Angle = self.GetAngle(line1, [x, y, w, h])
                                angle_pos = QPoint(x, y) + QPoint(15, -10)
                                painter.setPen(paint_Data['color_pen']['labelColor'])
                                drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                                painter.drawText(angle_pos, '%.2f°'%(Angle))
                                drawpainter.drawText(angle_pos, '%.2f°'%(Angle))
                            painter.setPen(paint_Data['color_pen']['angleColor'])
                            drawpainter.setPen(paint_Data['color_pen']['angleColor'])
                            painter.drawLine(x, y, w, h)
                            drawpainter.drawLine(x, y, w, h)
                elif paint_Data['func_type'] == 'point2CC':
                    painter.setPen(paint_Data['color_pen']['point_pen'])
                    drawpainter.setPen(paint_Data['color_pen']['point_pen'])
                    point_x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                    point_y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                    painter.drawPoint(point_x, point_y)
                    drawpainter.drawPoint(point_x, point_y)
                    if paint_Data['circle_nor_h']:
                        circle_center_x = int(self.qpixmap.width()*paint_Data['circle_center_nor_x'])
                        circle_center_y = int(self.qpixmap.height()*paint_Data['circle_center_nor_y'])
                        circle_w = int(self.qpixmap.width()*paint_Data['circle_nor_w'])
                        circle_h = int(self.qpixmap.height()*paint_Data['circle_nor_h'])
                        painter.setPen(paint_Data['color_pen']['circle_pen'])
                        drawpainter.setPen(paint_Data['color_pen']['circle_pen'])
                        painter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle_w, circle_h)
                        drawpainter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle_w, circle_h)
                        painter.setPen(paint_Data['color_pen']['linePen'])
                        drawpainter.setPen(paint_Data['color_pen']['linePen'])
                        painter.drawLine(QPoint(point_x, point_y), QPoint(circle_center_x, circle_center_y))
                        drawpainter.drawLine(QPoint(point_x, point_y), QPoint(circle_center_x, circle_center_y))
                        line_center_post = QPoint((point_x+circle_center_x)/2, (point_y+circle_center_y)/2) - QPoint(0, 10)
                        painter.setPen(paint_Data['color_pen']['labelColor'])
                        drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                        painter.drawText(line_center_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                        drawpainter.drawText(line_center_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                elif paint_Data['func_type'] == 'line2CC':
                    painter.setPen(paint_Data['color_pen']['linePen'])
                    drawpainter.setPen(paint_Data['color_pen']['linePen'])
                    x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                    y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                    w = int(self.qpixmap.width()*paint_Data['endPoint_nor_x'])
                    h = int(self.qpixmap.height()*paint_Data['endPoint_nor_y'])
                    painter.drawLine(x, y, w, h)
                    drawpainter.drawLine(x, y, w, h)
                    if paint_Data['circle_nor_h']:
                        circle_center_x = int(self.qpixmap.width()*paint_Data['circle_center_nor_x'])
                        circle_center_y = int(self.qpixmap.height()*paint_Data['circle_center_nor_y'])
                        p4x = int(self.qpixmap.width()*paint_Data['intersection_nor_x'])
                        p4y = int(self.qpixmap.height()*paint_Data['intersection_nor_y'])
                        circle_w = int(self.qpixmap.width()*paint_Data['circle_nor_w'])
                        circle_h = int(self.qpixmap.height()*paint_Data['circle_nor_h'])
                        # 畫圓
                        painter.setPen(paint_Data['color_pen']['circle_pen'])
                        drawpainter.setPen(paint_Data['color_pen']['circle_pen'])
                        painter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle_w, circle_h)
                        drawpainter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle_w, circle_h)
                        # 畫最短交點與圓心線
                        painter.setPen(paint_Data['color_pen']['linePen'])
                        drawpainter.setPen(paint_Data['color_pen']['linePen'])
                        painter.drawLine(QPoint(circle_center_x, circle_center_y), QPoint(p4x, p4y)) # 交匯point到圓心
                        drawpainter.drawLine(QPoint(circle_center_x, circle_center_y), QPoint(p4x, p4y))
                        painter.setPen(paint_Data['color_pen']['labelColor'])
                        drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                        label_post = QPoint((circle_center_x+p4x)/2, (circle_center_y+p4y)/2) - QPoint(0, 10)
                        painter.drawText(label_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                        drawpainter.drawText(label_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                elif paint_Data['func_type'] == 'line2point':
                    painter.setPen(paint_Data['color_pen']['linePen'])
                    drawpainter.setPen(paint_Data['color_pen']['linePen'])
                    x = int(self.qpixmap.width()*paint_Data['line_startPoint_nor_x'])
                    y = int(self.qpixmap.height()*paint_Data['line_startPoint_nor_y'])
                    w = int(self.qpixmap.width()*paint_Data['line_endPoint_nor_x'])
                    h = int(self.qpixmap.height()*paint_Data['line_endPoint_nor_y'])
                    painter.drawLine(x, y, w, h)
                    drawpainter.drawLine(x, y, w, h)
                    if paint_Data['startPoint_nor_x']:
                        startPoint_x = int(self.qpixmap.width()*paint_Data['startPoint_nor_x'])
                        startPoint_y = int(self.qpixmap.height()*paint_Data['startPoint_nor_y'])
                        intersectionPoint_x = int(self.qpixmap.width()*paint_Data['intersectionPoint_nor_x'])
                        intersectionPoint_y = int(self.qpixmap.height()*paint_Data['intersectionPoint_nor_y'])
                        painter.drawLine(QPoint(intersectionPoint_x, intersectionPoint_y), QPoint(startPoint_x, startPoint_y)) # 交匯point到圓心
                        drawpainter.drawLine(QPoint(intersectionPoint_x, intersectionPoint_y), QPoint(startPoint_x, startPoint_y))
                        painter.setPen(paint_Data['color_pen']['point_pen'])
                        drawpainter.setPen(paint_Data['color_pen']['point_pen'])
                        painter.drawPoint(startPoint_x, startPoint_y)
                        drawpainter.drawPoint(startPoint_x, startPoint_y)
                        painter.setPen(paint_Data['color_pen']['labelColor'])
                        drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                        label_post = QPoint((intersectionPoint_x+startPoint_x)/2, (intersectionPoint_y+startPoint_y)/2) - QPoint(0, 10)
                        painter.drawText(label_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                        drawpainter.drawText(label_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                elif paint_Data['func_type'] == 'CC2CC':
                    painter.setPen(paint_Data['color_pen']['circle_pen'])
                    drawpainter.setPen(paint_Data['color_pen']['circle_pen'])
                    circle_center1_x = int(self.qpixmap.width()*paint_Data['circle1_center_nor_x'])
                    circle_center1_y = int(self.qpixmap.height()*paint_Data['circle1_center_nor_y'])
                    circle1_w = int(self.qpixmap.width()*paint_Data['circle1_nor_w'])
                    circle1_h = int(self.qpixmap.height()*paint_Data['circle1_nor_h'])
                    painter.drawEllipse(QPoint(circle_center1_x, circle_center1_y), circle1_w, circle1_h)
                    drawpainter.drawEllipse(QPoint(circle_center1_x, circle_center1_y), circle1_w, circle1_h)
                    if paint_Data['circle2_nor_h']:
                        circle_center2_x = int(self.qpixmap.width()*paint_Data['circle2_center_nor_x'])
                        circle_center2_y = int(self.qpixmap.height()*paint_Data['circle2_center_nor_y'])
                        circle2_w = int(self.qpixmap.width()*paint_Data['circle2_nor_w'])
                        circle2_h = int(self.qpixmap.height()*paint_Data['circle2_nor_h'])
                        painter.drawEllipse(QPoint(circle_center2_x, circle_center2_y), circle2_w, circle2_h)
                        drawpainter.drawEllipse(QPoint(circle_center2_x, circle_center2_y), circle2_w, circle2_h)
                        painter.setPen(paint_Data['color_pen']['labelColor'])
                        drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                        label_post = QPoint((circle_center1_x+circle_center2_x)/2, (circle_center1_y+circle_center2_y)/2) - QPoint(0, 10)
                        painter.drawText(label_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                        drawpainter.drawText(label_post, '%.3f %s'%(paint_Data['distance'],calibration_scale_unit))
                        painter.setPen(paint_Data['color_pen']['linePen'])
                        drawpainter.setPen(paint_Data['color_pen']['linePen'])
                        painter.drawLine(QPoint(circle_center1_x, circle_center1_y), QPoint(circle_center2_x, circle_center2_y)) # 兩圓圓心距離
                        drawpainter.drawLine(QPoint(circle_center1_x, circle_center1_y), QPoint(circle_center2_x, circle_center2_y))
                elif paint_Data['func_type'] == 'Ellipse_diameter':
                    painter.setPen(paint_Data['color_pen']['circle_pen'])
                    drawpainter.setPen(paint_Data['color_pen']['circle_pen'])
                    circle_center_x = int(self.qpixmap.width()*paint_Data['circle_center_nor_x'])
                    circle_center_y = int(self.qpixmap.height()*paint_Data['circle_center_nor_y'])
                    circle_w = int(self.qpixmap.width()*paint_Data['circle_nor_w'])
                    circle_h = int(self.qpixmap.height()*paint_Data['circle_nor_h'])
                    painter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle_w, circle_h)
                    drawpainter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle_w, circle_h)
                    painter.setPen(paint_Data['color_pen']['linePen'])
                    drawpainter.setPen(paint_Data['color_pen']['linePen'])
                    diameter_rightPoint_x = int(self.qpixmap.width()*paint_Data['diameter_right_nor'][0])
                    diameter_rightPoint_y = int(self.qpixmap.height()*paint_Data['diameter_right_nor'][1])
                    diameter_leftPoint_x = int(self.qpixmap.width()*paint_Data['diameter_left_nor'][0])
                    diameter_leftPoint_y = int(self.qpixmap.height()*paint_Data['diameter_left_nor'][1])
                    diameter_topPoint_x = int(self.qpixmap.width()*paint_Data['diameter_top_nor'][0])
                    diameter_topPoint_y = int(self.qpixmap.height()*paint_Data['diameter_top_nor'][1])
                    diameter_bottomPoint_x = int(self.qpixmap.width()*paint_Data['diameter_bottom_nor'][0])
                    diameter_bottomPoint_y = int(self.qpixmap.height()*paint_Data['diameter_bottom_nor'][1])
                    painter.drawLine(QPoint(diameter_rightPoint_x, diameter_rightPoint_y), QPoint(diameter_leftPoint_x, diameter_leftPoint_y ))
                    painter.drawLine(QPoint(diameter_topPoint_x, diameter_topPoint_y), QPoint(diameter_bottomPoint_x, diameter_bottomPoint_y))
                    drawpainter.drawLine(QPoint(diameter_rightPoint_x, diameter_rightPoint_y), QPoint(diameter_leftPoint_x, diameter_leftPoint_y ))
                    drawpainter.drawLine(QPoint(diameter_topPoint_x, diameter_topPoint_y), QPoint(diameter_bottomPoint_x, diameter_bottomPoint_y))
                    painter.setPen(paint_Data['color_pen']['labelColor'])
                    label_post_a = QPoint(int(self.qpixmap.width()*paint_Data['label_position_a_nor'][0]), int(self.qpixmap.height()*paint_Data['label_position_a_nor'][1]))
                    label_post_b = QPoint(int(self.qpixmap.width()*paint_Data['label_position_b_nor'][0]), int(self.qpixmap.height()*paint_Data['label_position_b_nor'][1]))
                    painter.drawText(label_post_a, 'a=%.3f %s'%(paint_Data['diameter_a'],calibration_scale_unit))
                    painter.drawText(label_post_b, 'b=%.3f %s'%(paint_Data['diameter_b'],calibration_scale_unit))
                    drawpainter.drawText(label_post_a, 'a=%.3f %s'%(paint_Data['diameter_a'],calibration_scale_unit))
                    drawpainter.drawText(label_post_b, 'b=%.3f %s'%(paint_Data['diameter_b'],calibration_scale_unit))
                elif paint_Data['func_type'] == 'ConC_diameter':
                    painter.setPen(paint_Data['color_pen']['circle_pen'])
                    drawpainter.setPen(paint_Data['color_pen']['circle_pen'])
                    circle_center_x = int(self.qpixmap.width()*paint_Data['circle_center_nor_x'])
                    circle_center_y = int(self.qpixmap.height()*paint_Data['circle_center_nor_y'])
                    circle1_w = int(self.qpixmap.width()*paint_Data['circle1_nor_w'])
                    circle1_h = int(self.qpixmap.height()*paint_Data['circle1_nor_h'])
                    painter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle1_w, circle1_h)
                    drawpainter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle1_w, circle1_h)
                    painter.setPen(paint_Data['color_pen']['linePen'])
                    drawpainter.setPen(paint_Data['color_pen']['linePen'])
                    painter.drawLine(QPoint(int(self.qpixmap.width()*paint_Data['circle1_nor_right'][0]), int(self.qpixmap.height()*paint_Data['circle1_nor_right'][1])), QPoint(int(self.qpixmap.width()*paint_Data['circle1_nor_left'][0]), int(self.qpixmap.height()*paint_Data['circle1_nor_left'][1])))
                    drawpainter.drawLine(QPoint(int(self.qpixmap.width()*paint_Data['circle1_nor_right'][0]), int(self.qpixmap.height()*paint_Data['circle1_nor_right'][1])), QPoint(int(self.qpixmap.width()*paint_Data['circle1_nor_left'][0]), int(self.qpixmap.height()*paint_Data['circle1_nor_left'][1])))
                    painter.setPen(paint_Data['color_pen']['labelColor'])
                    drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                    label_post_circle1 = QPoint(int(self.qpixmap.width()*paint_Data['label_position_circle1_nor'][0]), int(self.qpixmap.height()*paint_Data['label_position_circle1_nor'][1]))
                    drawpainter.drawText(label_post_circle1, '%.3f %s'%(paint_Data['circle1_diameter'],calibration_scale_unit))
                    painter.drawText(label_post_circle1, '%.3f %s'%(paint_Data['circle1_diameter'],calibration_scale_unit))
                    
                    if paint_Data['circle2_nor_w']:
                        painter.setPen(paint_Data['color_pen']['circle_pen'])
                        drawpainter.setPen(paint_Data['color_pen']['circle_pen'])
                        circle2_w = int(self.qpixmap.width()*paint_Data['circle2_nor_w'])
                        circle2_h = int(self.qpixmap.height()*paint_Data['circle2_nor_h'])
                        painter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle2_w, circle2_h)
                        drawpainter.drawEllipse(QPoint(circle_center_x, circle_center_y), circle2_w, circle2_h)
                        painter.setPen(paint_Data['color_pen']['linePen'])
                        drawpainter.setPen(paint_Data['color_pen']['linePen'])
                        painter.drawLine(QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_stretch_right'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_stretch_right'][1])), QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_stretch_left'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_stretch_left'][1])))
                        drawpainter.drawLine(QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_stretch_right'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_stretch_right'][1])), QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_stretch_left'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_stretch_left'][1])))
                        painter.setPen(paint_Data['color_pen']['labelColor'])
                        drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                        label_post_circle2 = QPoint(int(self.qpixmap.width()*paint_Data['label_position_circle2_nor'][0]), int(self.qpixmap.height()*paint_Data['label_position_circle2_nor'][1]))
                        painter.drawText(label_post_circle2, '%.3f %s'%(paint_Data['circle2_diameter'],calibration_scale_unit))
                        drawpainter.drawText(label_post_circle2, '%.3f %s'%(paint_Data['circle2_diameter'],calibration_scale_unit))
                        adie_linePen = QPen(Qt.black, self.preview_linesize, Qt.DashLine, Qt.RoundCap, Qt.RoundJoin)
                        painter.setPen(adie_linePen)
                        drawpainter.setPen(adie_linePen)
                        # 右邊延伸虛線
                        painter.drawLine(QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_right'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_right'][1])), 
                                         QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_stretch_right'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_stretch_right'][1]))) 
                        drawpainter.drawLine(QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_right'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_right'][1])), 
                                             QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_stretch_right'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_stretch_right'][1]))) 
                        # 左邊延伸虛線
                        painter.drawLine(QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_left'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_left'][1])), 
                                         QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_stretch_left'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_stretch_left'][1]))) 
                        drawpainter.drawLine(QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_left'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_left'][1])), 
                                             QPoint(int(self.qpixmap.width()*paint_Data['circle2_nor_stretch_left'][0]), int(self.qpixmap.height()*paint_Data['circle2_nor_stretch_left'][1]))) 
                elif paint_Data['func_type'] == 'radius_of_curv':
                    painter.setPen(paint_Data['color_pen']['point_pen'])
                    drawpainter.setPen(paint_Data['color_pen']['point_pen'])
                    for i in range(1,4):
                        if paint_Data[f'curv{i:d}Point_nor']:
                            curvPoint_x = int(self.qpixmap.width()*paint_Data[f'curv{i:d}Point_nor'][0])
                            curvPoint_y = int(self.qpixmap.height()*paint_Data[f'curv{i:d}Point_nor'][1])
                            painter.drawPoint(curvPoint_x, curvPoint_y)
                            drawpainter.drawPoint(curvPoint_x, curvPoint_y)
                            if i==3:
                                p1 = [self.qpixmap.width()*paint_Data['curv1Point_nor'][0], self.qpixmap.height()*paint_Data['curv1Point_nor'][1]]
                                p2 = [self.qpixmap.width()*paint_Data['curv2Point_nor'][0], self.qpixmap.height()*paint_Data['curv2Point_nor'][1]]
                                p3 = [self.qpixmap.width()*paint_Data['curv3Point_nor'][0], self.qpixmap.height()*paint_Data['curv3Point_nor'][1]]
                                # print(p1,p2,p3)
                                cc, r = self.get_circlecenter_and_radius(p1,p2,p3)
                                if cc:
                                    # print("c0:",cc[0], "c1:",cc[1], "r:",r)
                                    label_post = QPoint(int((cc[0]+p3[0])/2), int((cc[1]+p3[1])/2)) - QPoint(0, 10)
                                    painter.drawPoint(cc[0], cc[1])
                                    painter.setPen(paint_Data['color_pen']['circle_pen'])
                                    drawpainter.setPen(paint_Data['color_pen']['circle_pen'])
                                    painter.drawEllipse(QPoint(cc[0], cc[1]), r, r)
                                    drawpainter.drawEllipse(QPoint(cc[0], cc[1]), r, r)
                                    painter.setPen(paint_Data['color_pen']['linePen'])
                                    drawpainter.setPen(paint_Data['color_pen']['linePen'])
                                    painter.drawLine(QPoint(cc[0], cc[1]), QPoint(p3[0], p3[1])) 
                                    drawpainter.drawLine(QPoint(cc[0], cc[1]), QPoint(p3[0], p3[1])) 
                                    painter.setPen(paint_Data['color_pen']['labelColor'])
                                    drawpainter.setPen(paint_Data['color_pen']['labelColor'])
                                    painter.drawText(label_post, 'r=%.3f %s'%(r * calibration_scale,calibration_scale_unit))
                                    drawpainter.drawText(label_post, 'r=%.3f %s'%(r * calibration_scale,calibration_scale_unit))

            if self.image_lines_info[self._current_index]["paints"]:
                self.previous_enable_status.emit(True)
                self.restore_enable_status.emit(True)
                self.savefile_enable_status.emit(True)
            else:
                self.previous_enable_status.emit(False)
                self.restore_enable_status.emit(False)
                self.savefile_enable_status.emit(False)
    
    def get_circlecenter_and_radius(self,p1,p2,p3):
        # https://stackoverflow.com/questions/28910718/give-3-points-and-a-plot-circle
        temp = p2[0] * p2[0] + p2[1] * p2[1]
        bc = (p1[0] * p1[0] + p1[1] * p1[1] - temp) / 2
        cd = (temp - p3[0] * p3[0] - p3[1] * p3[1]) / 2
        det = (p1[0] - p2[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p2[1])
        
        if abs(det) < 1.0e-6:
            return (None, np.inf)
        
        # Center of circle
        cx = (bc*(p2[1] - p3[1]) - cd*(p1[1] - p2[1])) / det
        cy = ((p1[0] - p2[0]) * cd - (p2[0] - p3[0]) * bc) / det
        
        radius = np.sqrt((cx - p1[0])**2 + (cy - p1[1])**2)
        return ((cx, cy), radius)
    
    def point_2_line_distance(self, p1,p2,p3):
        nom = abs((p2[0]-p1[0])*(p1[1]-p3[1])-(p1[0]-p3[0])*(p2[1]-p1[1]))
        denom = math.sqrt((p2[0]-p1[0])**2+(p2[1]-p1[1])**2)
        return nom/denom

    def get_point2line_intersection(self, p1,p2,p3):
        x1, y1 = p1
        x2, y2 = p2
        x3, y3 = p3
        dx, dy = x2-x1, y2-y1
        det = dx*dx + dy*dy
        a = (dy*(y3-y1)+dx*(x3-x1))/det
        return x1+a*dx, y1+a*dy
        
    def read_img(self, img_path):
        self.origin_img =  cv2.imread(img_path)
        self.origin_image_height, self.origin_image_width, channel = self.origin_img.shape
        self.display_img = self.origin_img
        # self.bytesPerline = 3 * self.origin_image_width  # 設定「每一行」的影像佔用位置數量，目前因為有 3 個 channel，因此是 3 * width
        self.image = QImage(self.display_img, self.origin_image_width, self.origin_image_height,  QImage.Format_RGB888).rgbSwapped()
        self.qpixmap = QPixmap.fromImage(self.image)
        self.restore_enable_status.emit(False)
        self.savefile_enable_status.emit(False)
        
    def update_painter(self, ratio_value):
        self.ratio_value = ratio_value
        self.ratio_rate = pow(10, (self.ratio_value - 50)/50)
        qpixmap_height = self.origin_image_height*self.ratio_rate
        qpixmap_width = self.origin_image_width*self.ratio_rate
        if qpixmap_height > self.origin_image_height:
            self.setFixedSize(self.qpixmap.size()*self.ratio_rate)
        self.update_img_ratio()
        self.update()
        
    def update_img_ratio(self):
        self.update_text_ratio()
        self.update_text_img_shape()

    def update_text_ratio(self):
        self.zoom_info_dict['Zoom ratio'] = f"{int(100*self.ratio_rate)} %"

    def update_text_img_shape(self):
        current_text = f"({self.qpixmap.width()}, {self.qpixmap.height()})"
        origin_text = f"({self.origin_image_width}, {self.origin_image_height})"
        self.zoom_info_dict['Image shape'] = origin_text
        self.zoom_info_dict['current shape'] = current_text

    def update_text_clicked_position(self, x, y, event):
        self.zoom_info_dict['clicked pos'] = f"({x}, {y})"
        norm_x = x/self.qpixmap.width()
        norm_y = y/self.qpixmap.height()
        self.zoom_info_dict['Nor pos'] = f"({norm_x:.3f}, {norm_y:.3f})"
        self.zoom_info_dict['Real pos'] = f"({int(norm_x*self.image_width)}, {int(norm_y*self.image_height)})"
        return ''.join(['%s:%s\n'%(k,self.zoom_info_dict[k]) for k in self.zoom_info_dict.keys()])

    @property
    def current_index(self):
        return self._current_index

    @current_index.setter
    def current_index(self, index):
        if (index <= 0) & (len(list(self.image_lines_info.keys())) == 1):
            self.previous_enable = False
            self.next_enable = False
        elif (index == 0) & (len(list(self.image_lines_info.keys())) > 0):
            self.previous_enable = False
            self.next_enable = True
        elif index == (len(list(self.image_lines_info.keys())) - 1):
            self.previous_enable = True
            self.next_enable = False
        else:
            self.previous_enable = True
            self.next_enable = True

        if 0 <= index < len(list(self.image_lines_info.keys())):
            self._current_index = index
            self.image_path = self.image_lines_info[self._current_index]["file_path"]
            self.image_pathlib = pathlib.Path(self.image_path)
            self.read_img(self.image_path)
            self.update_painter(self.ratio_value)
    
    def save_panter_img(self):
        image_folder_path = str(self.image_pathlib.parents[0])
        save_img_folder_path = pathlib.Path(image_folder_path, 'measured')
        if not pathlib.Path(save_img_folder_path).exists():
            save_img_folder_path.mkdir(parents=True)
            save_img_folder_path.chmod(mode=0o777)
        
        # 20231012 增加量測資料保存至csv
        # 20231024 圖片改成全存
        is_save = False
        df_dict = {"INDEX":[], "IMAGEPATH":[], "TIME":[], "MEASURE_TYPE":[], "DISTANCE":[]}
        if not self.project_name and self.save_CSV:
            self.project_name = '%d'%(int(time.time()*1000))
        for index, index_value in self.image_lines_info.items():
            if index_value["paints"]:
                logger.debug(f"Save csv paints data: {str(index_value['paints'])}")
                img_path_str = index_value["file_path"]
                # 存圖
                img_Path = pathlib.Path(img_path_str)
                save_img_path = pathlib.Path(save_img_folder_path, img_Path.stem+'_m'+img_Path.suffix)
                self.image.save(str(save_img_path), "PNG")
                save_img_path.chmod(mode=0o777)
                logger.info(f"Save img success, path={save_img_path}.")
                if self.save_CSV:
                    paint_Data = index_value["paints"][-1]
                    measure_type = paint_Data['func_type']
                    if measure_type in ['point2point', 'point2CC', 'line2CC', 'line2point', 'CC2CC']:
                        measure_time = paint_Data['measure_time']
                        distance = np.around(paint_Data['distance'], 3)
                    df_dict["INDEX"].append(index+1)
                    df_dict["TIME"].append(measure_time)
                    df_dict["IMAGEPATH"].append(save_img_path.resolve().as_posix())
                    df_dict["MEASURE_TYPE"].append(measure_type)
                    df_dict["DISTANCE"].append(distance)
                is_save = True
        if self.save_CSV:
            df = pd.DataFrame(df_dict)
            csv_path = pathlib.Path(image_folder_path) / f'{self.project_name}.csv'
            try:
                df.to_csv(csv_path, index=False)
                csv_path.chmod(mode=0o777)
                logger.info(f"Save「{self.project_name}」CSV success.")
            except PermissionError:
                QMessageBox.warning(None, 'Warning Message', f'The CSV file 「{self.project_name}」 is currently in use!',QMessageBox.Ok)
                logger.warning(f"Save「{self.project_name}」CSV on use.")
        if is_save:
            self.currentinfo_save_status.emit(True)

    def crop_img_and_save(self ,x,y,w,h):
        save_img_folder_path = pathlib.Path('./targetImage') # 0504 Polly modified
        if not save_img_folder_path.exists():
            save_img_folder_path.mkdir(parents=True)
            save_img_folder_path.chmod(mode=0o777)       
        save_img_path = pathlib.Path(save_img_folder_path, self.image_pathlib.stem+'_c'+self.image_pathlib.suffix)
        count = 1
        while pathlib.Path(save_img_path).exists():
            save_img_path = pathlib.Path(save_img_folder_path, self.image_pathlib.stem+'_c%d'%count+self.image_pathlib.suffix)
            count += 1

        # 0222 Sarah add, choose where to save image and define the file name
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()",str(save_img_path),"Images (*.png  *.jpg)", options=options)
        if fileName:
            if fileName.rsplit('.', 1)[-1] != 'jpg' and fileName.rsplit('.', 1)[-1] != 'png':
                fileName = fileName + '.png'
            crop_img = self.origin_img[y:y+h, x:x+w]
            cv2.imwrite(fileName, crop_img)
            os.chmod(fileName, mode=0o777)
        
    def painter_restore(self):
        self.image = QImage(self.display_img, self.origin_image_width, self.origin_image_height,  QImage.Format_RGB888).rgbSwapped()
        self.image_lines_info[self._current_index]["paints"] = [] # 清空該張圖片所有量的東西
        self.restore_enable_status.emit(False)
        self.currentinfo_save_status.emit(False)
        self.update_painter(self.ratio_value)
    
    def painter_previous(self):
        self.image = QImage(self.display_img, self.origin_image_width, self.origin_image_height,  QImage.Format_RGB888).rgbSwapped()
        self.image_lines_info[self._current_index]["paints"] = self.image_lines_info[self._current_index]["paints"][:-1]
        self.currentinfo_save_status.emit(False)
        self.update_painter(self.ratio_value)

    def line_intersects_judge(self, l1, L2):
        line1_x, line1_y, line1_w, line1_h = l1
        line2_x, line2_y, line2_w, line2_h = L2
        if min(line1_x,line1_w)<=max(line2_x,line2_w) and min(line2_y,line2_h)<=max(line1_y,line1_h) and min(line2_x,line2_w)<=max(line1_x,line1_w) and min(line1_y,line1_h)<=max(line2_y,line2_h):
            return True
        else:
            return False
    
    def GetAngle(self, line1_list, line2_list):
        dx1 = line1_list[0] - line1_list[2]
        dy1 = line1_list[1] - line1_list[3]
        dx2 = line2_list[0] - line2_list[2]
        dy2 = line2_list[1] - line2_list[3]
        angle1 = math.atan2(dy1, dx1)
        angle1 = float(angle1 * 180 / math.pi)
        #print("angle1",angle1)
        angle2 = math.atan2(dy2, dx2)
        angle2 = float(angle2 * 180 / math.pi)
        #print("angle2",angle2)
        if angle1 * angle2 >= 0:
            insideAngle = abs(angle1 - angle2)
        else:
            insideAngle = abs(angle1) + abs(angle2)
            if insideAngle > 180:
                insideAngle = 360 - insideAngle
        insideAngle = insideAngle % 180
        return insideAngle

class style_Button(QPushButton):
    def __init__(self, *args, **kwargs):
        super(style_Button, self).__init__(*args, **kwargs)

    def setPixmap(self, pixmap):
        self.pixmap = pixmap

    def sizeHint(self):
        parent_size = QPushButton.sizeHint(self)
        return QSize(parent_size.width() + self.pixmap.width(), max(parent_size.height(), self.pixmap.height()))

    def paintEvent(self, event):
        QPushButton.paintEvent(self, event)

        pos_x = 15 # hardcoded horizontal margin
        pos_y = (self.height() - self.pixmap.height()) / 2

        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing, True)
        painter.setRenderHint(QPainter.SmoothPixmapTransform, True)
        painter.drawPixmap(int(pos_x), int(pos_y), self.pixmap)

class App(QWidget):
    def __init__(self,parent=None, image_list = []):
        global calibration_scale, calibration_scale_unit 
        super().__init__(parent)

        self.ratio_value = 50
        self.cal_zoom_ratio()
        self.current_info = {"Image No.":"0",
                             "Zoom Ratio":"{:.3f}".format(self.app_ratio_rate), 
                             "Pixel Sacle value":"{:.4f}".format(calibration_scale), 
                             "Pixel Sacle uint":'%s/pixel'%calibration_scale_unit,
                             "Image Save":None}
        self.image_list = image_list
        self.output_csv = measuretool_config.getboolean("SET", "save_csv")

        btn_font = QFont()
        btn_font.setFamily("微軟正黑體")
        btn_font.setPointSize(12)

        centralwidget = QWidget(self)
        verticalLayout_Widget = QWidget(centralwidget)
        verticalLayout_Widget.setGeometry(QRect(1, 5, 1024, 580))

        verticalLayout_main = QVBoxLayout(verticalLayout_Widget)
        verticalLayout_main.setContentsMargins(0, 0, 0, 0)
        horizontalLayout_main = QHBoxLayout() # 測量選項橫條

        self.menu_btn = QPushButton(verticalLayout_Widget)
        self.menu_btn.setIcon(QIcon('icon/menu_btn.png'))
        self.menu_btn.setIconSize(QSize(30, 30))
        self.menu_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.menu_btn.setToolTip("Menu")
        # 0411 Polly issue --> AttributeError: 'triggered()' is not a Qt property or a signal
        menu = QMenu(self.menu_btn) # menu = QMenu(self.menu_btn, triggered=self.menu_actionClicked)
        menu.triggered.connect(self.menu_actionClicked) # 0411 new added for trigger
        munu_dict = {"Palette":{"Icon":"icon/palette_btn.png",
                                 "sub_menu":["Line Color","Box Color","Box aids Color","Angle Color","Label Color","Info Color"]},
                    "Label size":{"Icon":"icon/label_size_btn.png",
                                 "sub_menu":[]},
                    "Theme":{"Icon":"icon/theme_btn.png",
                                 "sub_menu":["Default", "Adaptic","Darkeum","Diplaytap","Dtor","EasyCode","Eclippy","Geoo","Obit","PicPax","TCobra","Webmas","Wstartpage"]},
                    "CSV":{"Icon":"icon/csv_btn.png",
                                 "sub_menu":[]}
                    }
        for menu_item in munu_dict.keys():
            if munu_dict[menu_item]["sub_menu"]:
                menu_item_main  = menu.addMenu(QIcon(munu_dict[menu_item]["Icon"]), menu_item)
                for sub in munu_dict[menu_item]["sub_menu"]:
                    sub_item = QAction(sub, self)
                    if menu_item == "Theme" and sub == theme:
                        sub_item.setCheckable(True)
                        sub_item.setChecked(True)
                    sub_item.setObjectName(f"{menu_item}_{sub}")
                    menu_item_main.addAction(sub_item)
            else:
                if menu_item == 'CSV':
                    csv_action = QAction(menu_item, self)
                    csv_action.setCheckable(True)
                    csv_action.setChecked(self.output_csv)
                    csv_action.triggered.connect(lambda: self.switch_save_csv(csv_action.isChecked()))
                    menu.addAction(csv_action)
                elif menu_item == 'Label size':
                    label_size_action = QAction(menu_item, self)
                    label_size_action.setIcon(QIcon(munu_dict[menu_item]["Icon"]))
                    label_size_action.triggered.connect(self.adjustment_label_size)
                    menu.addAction(label_size_action)
        
        self.menu_btn.setMenu(menu)
        horizontalLayout_main.addWidget(self.menu_btn)

        self.openfile_btn = QPushButton(verticalLayout_Widget)
        self.openfile_btn.setIcon(QIcon('icon/openfile.png'))
        self.openfile_btn.setIconSize(QSize(30, 30))
        self.openfile_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.openfile_btn.clicked.connect(self.func_openfile)
        self.openfile_btn.setToolTip("Open file")
        horizontalLayout_main.addWidget(self.openfile_btn)

        self.openfolder_btn = QPushButton(verticalLayout_Widget)
        self.openfolder_btn.setIcon(QIcon('icon/openfolder.png'))
        self.openfolder_btn.setIconSize(QSize(30, 30))
        self.openfolder_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.openfolder_btn.clicked.connect(self.func_openfolder)
        self.openfolder_btn.setToolTip("Open folder")
        horizontalLayout_main.addWidget(self.openfolder_btn)

        self.savefile_btn = QPushButton(verticalLayout_Widget)
        self.savefile_btn.setIcon(QIcon('icon/savefile.png'))
        self.savefile_btn.setIconSize(QSize(30, 30))
        self.savefile_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.savefile_btn.setToolTip("Save")
        horizontalLayout_main.addWidget(self.savefile_btn)

        self.restore_btn = QPushButton(verticalLayout_Widget)
        self.restore_btn.setIcon(QIcon('icon/restore_btn.png'))
        self.restore_btn.setIconSize(QSize(30, 30))
        self.restore_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.restore_btn.setToolTip("Restore")
        self.restore_btn.setEnabled(False)
        horizontalLayout_main.addWidget(self.restore_btn)

        self.previous_btn = QPushButton(verticalLayout_Widget)
        self.previous_btn.setIcon(QIcon('icon/previous_btn.png'))
        self.previous_btn.setIconSize(QSize(30, 30))
        self.previous_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.previous_btn.setToolTip("Previous")
        self.previous_btn.setEnabled(False)
        horizontalLayout_main.addWidget(self.previous_btn)

        line_1 = QFrame(verticalLayout_Widget)
        line_1.setFrameShape(QFrame.VLine)
        line_1.setFrameShadow(QFrame.Sunken)
        horizontalLayout_main.addWidget(line_1)
        
        self.camera_btn = QPushButton(verticalLayout_Widget)
        self.camera_btn.setIcon(QIcon('icon/Screenshot.png'))
        self.camera_btn.setIconSize(QSize(30, 30))
        self.camera_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.camera_btn.clicked.connect(self.open_camera)
        self.camera_btn.setToolTip("Camera screenshot")
        horizontalLayout_main.addWidget(self.camera_btn)

        self.calibration_btn = QPushButton(verticalLayout_Widget)
        self.calibration_btn.setIcon(QIcon('icon/calibration.png'))
        self.calibration_btn.setIconSize(QSize(30, 30))
        self.calibration_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.calibration_btn.setToolTip("Calibration")
        horizontalLayout_main.addWidget(self.calibration_btn)
        self.calibration_btn.clicked.connect(self.calibration_dialog)

        self.HSV_btn = QPushButton(verticalLayout_Widget)
        self.HSV_btn.setIcon(QIcon('icon/hsv_btn.png'))
        self.HSV_btn.setIconSize(QSize(30, 30))
        self.HSV_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.HSV_btn.setToolTip("HSV")
        horizontalLayout_main.addWidget(self.HSV_btn)
        self.HSV_btn.clicked.connect(self.hsv_dialog)
        self.HSV_btn.setEnabled(False)

        line_2 = QFrame(verticalLayout_Widget)
        line_2.setFrameShape(QFrame.VLine)
        line_2.setFrameShadow(QFrame.Sunken)
        horizontalLayout_main.addWidget(line_2)

        self.point2point_btn = QPushButton(verticalLayout_Widget)
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.point2point_btn.setIconSize(QSize(30, 30))
        self.point2point_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.point2point_btn.clicked.connect(self.func_point2point)
        self.point2point_btn.setToolTip("point-point distance")
        horizontalLayout_main.addWidget(self.point2point_btn)

        self.angle_btn = QPushButton(verticalLayout_Widget)
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.angle_btn.setIconSize(QSize(30, 30))
        self.angle_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.angle_btn.clicked.connect(self.func_angle)
        self.angle_btn.setToolTip("Angle")
        horizontalLayout_main.addWidget(self.angle_btn)

        self.boundingbox_btn = QPushButton(verticalLayout_Widget)
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.boundingbox_btn.setIconSize(QSize(30, 30))
        self.boundingbox_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.boundingbox_btn.clicked.connect(self.func_boundingbox)
        self.boundingbox_btn.setToolTip("Boundingbox")
        horizontalLayout_main.addWidget(self.boundingbox_btn)

        self.point_2_centercircle_btn = QPushButton(verticalLayout_Widget)
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.point_2_centercircle_btn.setIconSize(QSize(30, 30))
        self.point_2_centercircle_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.point_2_centercircle_btn.clicked.connect(self.func_point_2_centercircle)
        self.point_2_centercircle_btn.setToolTip("Point to center of circle distance")
        horizontalLayout_main.addWidget(self.point_2_centercircle_btn)

        self.line_2_centercircle_btn = QPushButton(verticalLayout_Widget)
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIconSize(QSize(30, 30))
        self.line_2_centercircle_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.line_2_centercircle_btn.clicked.connect(self.func_line_2_centercircle)
        self.line_2_centercircle_btn.setToolTip("Line to center of circle distance")
        horizontalLayout_main.addWidget(self.line_2_centercircle_btn)

        self.line_2_point_btn = QPushButton(verticalLayout_Widget)
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.line_2_point_btn.setIconSize(QSize(30, 30))
        self.line_2_point_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.line_2_point_btn.clicked.connect(self.func_line_2_point)
        self.line_2_point_btn.setToolTip("Line to Point distance")
        horizontalLayout_main.addWidget(self.line_2_point_btn)

        self.centercircle_2_centercircle_btn = QPushButton(verticalLayout_Widget)
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.centercircle_2_centercircle_btn.setIconSize(QSize(30, 30))
        self.centercircle_2_centercircle_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.centercircle_2_centercircle_btn.clicked.connect(self.func_centercircle_2_centercircle)
        self.centercircle_2_centercircle_btn.setToolTip("Two circle center of distance")
        horizontalLayout_main.addWidget(self.centercircle_2_centercircle_btn)

        self.ellipse_diameter_distance_btn = QPushButton(verticalLayout_Widget)
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.ellipse_diameter_distance_btn.setIconSize(QSize(30, 30))
        self.ellipse_diameter_distance_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.ellipse_diameter_distance_btn.clicked.connect(self.func_ellipse_diameter_distance)
        self.ellipse_diameter_distance_btn.setToolTip("Ellipse diameter distance")
        horizontalLayout_main.addWidget(self.ellipse_diameter_distance_btn)

        self.concentric_circles_diameter_btn = QPushButton(verticalLayout_Widget)
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIconSize(QSize(30, 30))
        self.concentric_circles_diameter_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.concentric_circles_diameter_btn.clicked.connect(self.func_concentric_circles_diameter)
        self.concentric_circles_diameter_btn.setToolTip("Concentric circles diameter")
        horizontalLayout_main.addWidget(self.concentric_circles_diameter_btn)

        self.radius_of_curvature_btn = QPushButton(verticalLayout_Widget)
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.radius_of_curvature_btn.setIconSize(QSize(30, 30))
        self.radius_of_curvature_btn.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.radius_of_curvature_btn.clicked.connect(self.func_radius_of_curvature)
        self.radius_of_curvature_btn.setToolTip("Radius of curvature")
        horizontalLayout_main.addWidget(self.radius_of_curvature_btn)

        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        horizontalLayout_main.addItem(spacerItem)
        verticalLayout_main.addLayout(horizontalLayout_main)

        self.pre_Button = QPushButton()
        self.pre_Button.setIcon(QIcon('icon/previous.png'))
        self.pre_Button.setIconSize(QSize(30, 30))
        self.pre_Button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.pre_Button.clicked.connect(self.handle_previous)
        self.next_Button = QPushButton()
        self.next_Button.setIcon(QIcon('icon/next.png'))
        self.next_Button.setIconSize(QSize(30, 30))
        self.next_Button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.next_Button.clicked.connect(self.handle_next)
        horizontalLayout_main.addWidget(self.pre_Button)
        horizontalLayout_main.addWidget(self.next_Button)
        
        scrollArea = QScrollArea(widgetResizable=True)
        scrollArea.setWidgetResizable(True)
        scrollArea.setBackgroundRole(QPalette.Dark)

        self.zoom_in_btn = QPushButton(centralwidget)
        self.zoom_in_btn.setGeometry(QRect(10, 150, 50, 50))
        self.zoom_in_btn.setIconSize(QSize(40, 40))
        self.zoom_in_btn.setIcon(QIcon('icon/zoom-in.png'))
        self.zoom_in_btn.setStyleSheet('border-radius: 10px; border: 2px groove gray;border-style: outset;')

        self.zoom_out_btn = QPushButton(centralwidget)
        self.zoom_out_btn.setGeometry(QRect(10, 400, 50, 50))
        self.zoom_out_btn.setIconSize(QSize(40, 40))
        self.zoom_out_btn.setIcon(QIcon('icon/zoom-out.png'))
        self.zoom_out_btn.setStyleSheet('border-radius: 10px; border: 2px groove gray;border-style: outset;')

        self.vertical_Slider = QSlider(centralwidget)
        self.vertical_Slider.setValue(self.ratio_value)
        self.vertical_Slider.setGeometry(QRect(28, 210, 15, 180))
        self.vertical_Slider.setStyleSheet('''
            QSlider::groove:vertical {border: 0px solid transparent;width: 8px;background: #bebebe;margin: 2px 0;}
            QSlider::handle:vertical {background-color: #353f53;height: 12px;width: 6px;border: 0px solid transparent;margin: 0 -2px;border-radius: 6px;}
            QSlider::handle:vertical:pressed {background-color: #353f53;height: 18px;width: 6px;border: 0px solid transparent;margin: 0 -5px;border-radius: 9px;}
            QSlider::handle:vertical:disabled {background-color: #bebebe;height: 8px;width: 2px;border: 0px solid transparent;margin: -1px -1px;border-radius: 4px;}''')

        self.vertical_Slider.valueChanged.connect(self.getslidervalue)

        self.measure_info = QLabel(centralwidget)
        self.measure_info.setFont(QFont('Arial', 7))
        self.measure_info.setGeometry(QRect(5, 50, 115, 65))
        self.measure_info.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.measure_info.setStyleSheet(f'color:{info_font_color}')
        self.measure_info.setAttribute(Qt.WA_TranslucentBackground)

        self.exit_btn = style_Button("           Exit", centralwidget)
        self.exit_btn.setFont(QFont("微軟正黑體", 15, QFont.Bold))
        pixmap = QPixmap('icon/exit_btn.png').scaled(60, 40, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.exit_btn.setPixmap(pixmap)
        self.exit_btn.setGeometry(QRect(895, 520, 110, 45))
        self.exit_btn.setStyleSheet('border-radius: 10px; border: 3px groove gray;border-style: outset;')
        self.exit_btn.clicked.connect(self.exit_measure)

        self.zoom_in_btn.clicked.connect(self.func_zoom_in) 
        self.zoom_out_btn.clicked.connect(self.func_zoom_out)
        self.label = paint_Label(self.create_init_img_info(),self.ratio_value)
        # self.label.mousePressEvent = self.set_clicked_position 查修用,開啟後無法量測
        self.pre_Button.setEnabled(self.label.previous_enable)
        self.next_Button.setEnabled(self.label.next_enable)

        self.savefile_btn.clicked.connect(lambda: self.label.save_panter_img())
        self.restore_btn.clicked.connect(self.label.painter_restore) 
        self.previous_btn.clicked.connect(self.label.painter_previous) 
        self.label.savefile_enable_status.connect(self.savefile_btn_status)
        self.label.restore_enable_status.connect(self.restore_btn_status)
        self.label.previous_enable_status.connect(self.previous_btn_status)
        self.label.currentinfo_save_status.connect(self.measure_info_save_status)
        self.label.zoom_enable_status.connect(self.zoom_btn_status)
        self.label.save_CSV = self.output_csv

        scrollArea.setWidget(self.label)
        verticalLayout_main.addWidget(scrollArea)

        self.setWindowOpacity(0.9) # 设置窗口透明度
        if theme == 'Default':
            self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
        else:
            with open(f"./statics/manual_tool_theme/{theme}.qss", "r", encoding="UTF-8") as file:
                style_sheet = file.read()
            self.setStyleSheet(style_sheet)
    
    # @pyqtSlot(QAction)
    @Slot(QAction) # 0411 Polly PySide2 
    def menu_actionClicked(self, action):
        if action.text() in ["Line Color", "Box Color", "Box aids Color","Angle Color", "Label Color", "Info Color"]:
            self.open_pallete(action.text())
        elif action.text() in ["Default","Adaptic","Darkeum","Diplaytap","Dtor","EasyCode","Eclippy","Geoo","Obit","PicPax","TCobra","Webmas","Wstartpage"]:
            measuretool_config['Style']['theme'] = action.text()
            with open('measure_tool_config.ini', 'w', encoding='utf-8') as configfile: 
                measuretool_config.write(configfile)
            messageBox = QMessageBox()
            messageBox.setWindowTitle('Measure Tool Theme change message')
            messageBox.setText('Please restart the measurement program to take effect.')
            messageBox.setStandardButtons(QMessageBox.Ok)
            messageBox.exec_()

    def open_pallete(self, func_type):
        self.color = QColorDialog.getColor()
        if self.color.isValid():
            if func_type == "Line Color":
                self.label.lineColor = self.color
            elif func_type == "Box Color":
                self.label.boxColor = self.color
            elif func_type == "Box aids Color":
                self.label.box_previewColor = self.color
            elif func_type == "Angle Color":
                self.label.angleColor = self.color
            elif func_type == "Label Color":
                self.label.labelColor = self.color
            elif func_type == "Info Color":
                self.measure_info.setStyleSheet(f'color: {self.color.name()}')
                info_font_color = self.color.name()
                measuretool_config['Style']['info_font_color'] = info_font_color
                with open('measure_tool_config.ini', 'w', encoding='utf-8') as configfile: 
                    measuretool_config.write(configfile)
            self.label.update_pen_color()
    
    def cal_zoom_ratio(self):
        self.app_ratio_rate = pow(10, (self.ratio_value - 50)/50)
    
    def getslidervalue(self):  
        self.ratio_value = self.vertical_Slider.value()
        self.cal_zoom_ratio()
        self.current_info["Zoom Ratio"] = "{:.3f}".format(self.app_ratio_rate)
        self.update_measure_info()
        self.label.update_painter(self.ratio_value)

    def func_zoom_in(self):
        self.ratio_value = max(0, self.ratio_value + 1)
        self.cal_zoom_ratio()
        self.current_info["Zoom Ratio"] = "{:.3f}".format(self.app_ratio_rate)
        self.update_measure_info()
        self.vertical_Slider.setValue(self.ratio_value)
        self.label.update_painter(self.ratio_value)

    def func_zoom_out(self):
        self.ratio_value = min(100, self.ratio_value - 1)
        self.cal_zoom_ratio()
        self.current_info["Zoom Ratio"] = "{:.3f}".format(self.app_ratio_rate)
        self.update_measure_info()
        self.vertical_Slider.setValue(self.ratio_value)
        self.label.update_painter(self.ratio_value)

    def func_point2point(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_on_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.label.point2point_fun = True
        self.label.boundingbox_fun = False
        self.label.angle_fun = False
        self.label.point2CC_fun = False
        self.label.line2CC_fun = False
        self.label.line2point_fun = False
        self.label.CC2CC_fun = False
        self.label.Ellipse_diameter_fun = False
        self.label.ConC_diameter_fun = False
        self.label.radius_of_curv_fun = False

    def func_boundingbox(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_on_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.label.boundingbox_fun = True
        self.label.point2point_fun = False
        self.label.angle_fun = False
        self.label.point2CC_fun = False
        self.label.line2CC_fun = False
        self.label.line2point_fun = False
        self.label.CC2CC_fun = False
        self.label.Ellipse_diameter_fun = False
        self.label.ConC_diameter_fun = False
        self.label.radius_of_curv_fun = False
        self.label.setMouseTracking(True)
        messageBox = QMessageBox()
        messageBox.setWindowTitle('BoundingBox mode message')
        messageBox.setText('Save BoundingBox crop image?')
        messageBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Close)
        yes_crop_input = messageBox.button(QMessageBox.Yes)
        no_crop_input = messageBox.button(QMessageBox.No)
        Q_close = messageBox.button(QMessageBox.Close)
        Q_close.setText("Cancel") # 0414 Pollymodified
        messageBox.exec_()

        self.label.save_biundingbox = True if messageBox.clickedButton() == yes_crop_input else False
    
    def func_angle(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_on_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.label.boundingbox_fun = False
        self.label.point2point_fun = False
        self.label.angle_fun = True
        self.label.point2CC_fun = False
        self.label.line2CC_fun = False
        self.label.line2point_fun = False
        self.label.CC2CC_fun = False
        self.label.Ellipse_diameter_fun = False
        self.label.ConC_diameter_fun = False
        self.label.radius_of_curv_fun = False

    def func_point_2_centercircle(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_on_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.label.boundingbox_fun = False
        self.label.point2point_fun = False
        self.label.angle_fun = False
        self.label.point2CC_fun = True
        self.label.line2CC_fun = False
        self.label.line2point_fun = False
        self.label.CC2CC_fun = False
        self.label.Ellipse_diameter_fun = False
        self.label.ConC_diameter_fun = False
        self.label.radius_of_curv_fun = False
    
    def func_line_2_centercircle(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_on_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.label.boundingbox_fun = False
        self.label.point2point_fun = False
        self.label.angle_fun = False
        self.label.point2CC_fun = False
        self.label.line2CC_fun = True
        self.label.line2point_fun = False
        self.label.CC2CC_fun = False
        self.label.Ellipse_diameter_fun = False
        self.label.ConC_diameter_fun = False
        self.label.radius_of_curv_fun = False

    def func_line_2_point(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_on_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.label.boundingbox_fun = False
        self.label.point2point_fun = False
        self.label.angle_fun = False
        self.label.point2CC_fun = False
        self.label.line2CC_fun = False
        self.label.line2point_fun = True
        self.label.CC2CC_fun = False
        self.label.Ellipse_diameter_fun = False
        self.label.ConC_diameter_fun = False
        self.label.radius_of_curv_fun = False
    
    def func_centercircle_2_centercircle(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_on_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.label.boundingbox_fun = False
        self.label.point2point_fun = False
        self.label.angle_fun = False
        self.label.point2CC_fun = False
        self.label.line2CC_fun = False
        self.label.line2point_fun = False
        self.label.CC2CC_fun = True
        self.label.Ellipse_diameter_fun = False
        self.label.ConC_diameter_fun = False
        self.label.radius_of_curv_fun = False
    
    def func_ellipse_diameter_distance(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_on_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.label.boundingbox_fun = False
        self.label.point2point_fun = False
        self.label.angle_fun = False
        self.label.point2CC_fun = False
        self.label.line2CC_fun = False
        self.label.line2point_fun = False
        self.label.CC2CC_fun = False
        self.label.Ellipse_diameter_fun = True
        self.label.ConC_diameter_fun = False
        self.label.radius_of_curv_fun = False
    
    def func_concentric_circles_diameter(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_on_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_btn.png'))
        self.label.boundingbox_fun = False
        self.label.point2point_fun = False
        self.label.angle_fun = False
        self.label.point2CC_fun = False
        self.label.line2CC_fun = False
        self.label.line2point_fun = False
        self.label.CC2CC_fun = False
        self.label.Ellipse_diameter_fun = False
        self.label.ConC_diameter_fun = True
        self.label.radius_of_curv_fun = False

    def func_radius_of_curvature(self):
        self.point2point_btn.setIcon(QIcon('icon/slash_btn.png'))
        self.boundingbox_btn.setIcon(QIcon('icon/boundingbox_btn.png'))
        self.angle_btn.setIcon(QIcon('icon/angle_btn.png'))
        self.point_2_centercircle_btn.setIcon(QIcon('icon/point_2_centercircle_btn.png'))
        self.line_2_centercircle_btn.setIcon(QIcon('icon/line_2_centercircle_btn.png'))
        self.line_2_point_btn.setIcon(QIcon('icon/line_2_point_btn.png'))
        self.centercircle_2_centercircle_btn.setIcon(QIcon('icon/centercircle_2_centercircle_btn.png'))
        self.ellipse_diameter_distance_btn.setIcon(QIcon('icon/ellipse_diameter_btn.png'))
        self.concentric_circles_diameter_btn.setIcon(QIcon('icon/concentric_circles_diameter_btn.png'))
        self.radius_of_curvature_btn.setIcon(QIcon('icon/radius_of_curvature_on_btn.png'))
        self.label.boundingbox_fun = False
        self.label.point2point_fun = False
        self.label.angle_fun = False
        self.label.point2CC_fun = False
        self.label.line2CC_fun = False
        self.label.line2point_fun = False
        self.label.CC2CC_fun = False
        self.label.Ellipse_diameter_fun = False
        self.label.ConC_diameter_fun = False
        self.label.radius_of_curv_fun = True

    def func_openfolder(self):
        folder_path = QFileDialog.getExistingDirectory(self,"Openfloder","./", options=QFileDialog.DontUseNativeDialog)
        logger.info(f"Open folder path='{folder_path}'")
        if folder_path:
            file_list = [os.path.join(folder_path, f).replace('\\','/') for f in os.listdir(folder_path) if f.split('.')[-1] in ['jpg','png','jpeg']]
            logger.info(f"Have {len(file_list)} image.")
            folder_Path = pathlib.Path(folder_path)
            self.label.project_name = folder_Path.name # 把目錄名稱當成專案名稱
            project_record_data_path = folder_Path / f"{self.label.project_name}_screenshotinfo.json"
            if project_record_data_path.exists():
                import json

                with open(project_record_data_path, "r", encoding="UTF-8") as file:
                    camera_img_dict = json.load(file)
                self.label.image_lines_info = {int(index)-1:{"file_path":img_info["path"],"save":False,"paints":[]} for index, img_info in camera_img_dict.items()}
            else:
                self.image_list += file_list
                self.label.image_lines_info = self.create_init_img_info()
                self.image_list = [] # 清空image list

            self.label.current_index = 0
            self.pre_Button.setEnabled(self.label.previous_enable)
            self.next_Button.setEnabled(self.label.next_enable)
            self.current_info["Image No."] = "1"
            self.HSV_btn.setEnabled(True)
            self.update_measure_info()
        
    def func_openfile(self):
        filename, filetype = QFileDialog.getOpenFileName(self,"Openfile","./", 'Image files (*.jpg *.png *.jpeg)', options=QFileDialog.DontUseNativeDialog)
        logger.info(f"Open file path='{filename}'")
        if filename:
            self.image_list.append(filename)
            self.label.image_lines_info = self.create_init_img_info()
            self.label.current_index = 0
            self.image_list = [] # 清空image list
            self.pre_Button.setEnabled(self.label.previous_enable)
            self.next_Button.setEnabled(self.label.next_enable)
            self.current_info["Image No."] = "1"
            self.HSV_btn.setEnabled(True)
            self.update_measure_info()
    
    def handle_next(self):
        self.label.current_index += 1
        self.current_info["Image No."] = "%d"%(self.label.current_index+1)
        self.update_measure_info()
        self.pre_Button.setEnabled(self.label.previous_enable)
        self.next_Button.setEnabled(self.label.next_enable)

    def handle_previous(self):
        self.label.current_index -= 1
        self.current_info["Image No."] = "%d"%(self.label.current_index+1)
        self.update_measure_info()
        self.pre_Button.setEnabled(self.label.previous_enable)
        self.next_Button.setEnabled(self.label.next_enable)
    
    def create_init_img_info(self):
        if self.image_list:
            self.savefile_btn.setEnabled(True)
            return {p:{"file_path":path,"save":False,"paints":[]} for p, path in enumerate(self.image_list)}
        else:
            self.savefile_btn.setEnabled(False)
            return {0:{"file_path":"icon/empty.png","save":False,"paints":[]}}

    def open_camera(self):
        import manual_camera
        global calibration_scale

        self.Camera_w = manual_camera.Camera_MainWindow()
        self.Camera_w.app_widget.submitClicked.connect(self.receive_camera_screenshot)
        self.Camera_w.app_widget.magnification_value.connect(self.receive_magnification_value)
        self.Camera_w.show()
    
    def receive_camera_screenshot(self, camera_img_dict):
        self.label.project_name = camera_img_dict["Project"]
        camera_img_dict = camera_img_dict["Image"]
        if camera_img_dict:
            self.label.image_lines_info = {int(index)-1:{"file_path":img_info["path"],"save":False,"paints":[]} for index, img_info in camera_img_dict.items()}
            self.label.current_index = 0
            self.HSV_btn.setEnabled(True)
            self.pre_Button.setEnabled(self.label.previous_enable)
            self.next_Button.setEnabled(self.label.next_enable)

    def calibration_dialog(self):
        global calibration_scale

        messageBox = QMessageBox()
        messageBox.setWindowTitle('Calibration mode message')
        messageBox.setText('Select one mode.')
        messageBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Close)
        Q_value_input = messageBox.button(QMessageBox.Yes)
        Q_value_input.setText("Manual input")
        Q_camera_input = messageBox.button(QMessageBox.No)
        Q_camera_input.setText("Camera calibration")
        Q_close = messageBox.button(QMessageBox.Close)
        Q_close.setText("Cancel") # 0414 Pollymodified
        messageBox.exec_()

        if messageBox.clickedButton() == Q_value_input:
            import digit_keyboard

            self.keyboard_w = digit_keyboard.keyboard_MainWindow(current_pixel=calibration_scale, window_title="像素比例值輸入")
            self.keyboard_w.app_widget.input_values.connect(self.receive_calibration_value)
            self.keyboard_w.app_widget.input_unit.connect(self.receive_calibration_unit)
            self.keyboard_w.show()
        elif messageBox.clickedButton() == Q_camera_input:
            import calibration_pixelscale

            self.calibration_w = calibration_pixelscale.MainWindow()
            self.calibration_w.calibration_scale.connect(self.receive_calibration_value)
            self.calibration_w.calibration_unit.connect(self.receive_calibration_unit)
            self.calibration_w.show()
    
    def hsv_dialog(self):
        import manual_hsv as hsv

        self.HSV_w = hsv.HSV_MainWindow(img_path=self.label.image_path)
        self.HSV_w.show()
    
    def receive_magnification_value(self, manualcamera_magnification_value):
        global OutputModule # 0508 Sarah modify
        OutputModule.updateIniValue({'magnification' : manualcamera_magnification_value}) # 0508 Sarah modify
        previousNum, nextNum = coeffLogic.findNearest(int(manualcamera_magnification_value))
        magnificationX, magnificationY = coeffLogic.calculateCoefficient(previousNum, nextNum)
        calibration_scale = (magnificationX+magnificationY)/2

        self.current_info["Pixel Sacle value"] = "{:.4f}".format(calibration_scale)
        measuretool_config['Calibration']['value'] = str(calibration_scale)
        with open('measure_tool_config.ini', 'w', encoding='utf-8') as configfile: 
            measuretool_config.write(configfile)
        self.update_measure_info()

    def receive_calibration_value(self, calibration_value):
        global calibration_scale
        calibration_scale = calibration_value
        self.current_info["Pixel Sacle value"] = "{:.4f}".format(calibration_scale)
        measuretool_config['Calibration']['value'] = str(calibration_scale)
        with open('measure_tool_config.ini', 'w', encoding='utf-8') as configfile: 
            measuretool_config.write(configfile)
        self.update_measure_info()
        # print("calibration_scale:",calibration_scale)

    def receive_calibration_unit(self, calibration_unit):
        global calibration_scale_unit
        calibration_scale_unit = calibration_unit
        measuretool_config['Calibration']['unit'] = calibration_unit
        self.current_info["Pixel Sacle uint"] = '%s/pixel'%calibration_scale_unit
        with open('measure_tool_config.ini', 'w', encoding='utf-8') as configfile: 
            measuretool_config.write(configfile)
        self.update_measure_info()
        # print("calibration_scale_unit:",calibration_scale_unit)

    def savefile_btn_status(self, bool_status):
        self.savefile_btn.setEnabled(bool_status)

    def restore_btn_status(self, bool_status):
        self.restore_btn.setEnabled(bool_status)
    
    def previous_btn_status(self, bool_status):
        self.previous_btn.setEnabled(bool_status)
    
    def update_measure_info(self):
        self.measure_info.setText('\n'.join(["%s: %s"%(k, str(self.current_info[k])) for k in self.current_info]))

    def measure_info_save_status(self, bool_status):
        self.current_info["Image Save"] = bool_status
        self.measure_info.setText('\n'.join(["%s: %s"%(k, str(self.current_info[k])) for k in self.current_info]))
        if bool_status:
            self.savefile_btn.setIcon(QIcon('icon/savefile_on.png'))
        else:
            self.savefile_btn.setIcon(QIcon('icon/savefile.png'))
    
    def zoom_btn_status(self, bool_status):
        self.zoom_in_btn.setEnabled(bool_status)
        self.zoom_out_btn.setEnabled(bool_status)
        self.vertical_Slider.setEnabled(bool_status)
    
    def switch_save_csv(self, csv_status:bool):
        self.output_csv = csv_status
        self.label.save_CSV = csv_status
        measuretool_config['SET']['save_csv'] = str(csv_status)
        with open('measure_tool_config.ini', 'w', encoding='utf-8') as configfile: 
            measuretool_config.write(configfile)
        logger.info(f"Switch save CSV status={csv_status}")
    
    def adjustment_label_size(self):
        label_size, ok = QInputDialog.getText(self, 'Label size input dialog', 'Please input a number', text=str(self.label.measure_lable_size))
        if ok:
            logger.info(f"Input label size={label_size}.")
            if label_size.isdigit():
                measuretool_config['SET']['measure_lable_size'] = label_size
                with open('measure_tool_config.ini', 'w', encoding='utf-8') as configfile: 
                    measuretool_config.write(configfile)
                self.label.measure_lable_size = int(label_size)
            else:
                QMessageBox.warning(None, 'Warning Message', 'Input value not number! Please try again!',QMessageBox.Ok)
        else:
            logger.info("Cancle input label.")
    
    def exit_measure(self):
        if self.current_info["Image Save"] == True:
            reply = QMessageBox.warning(None, 'Close window', 'Exit Manual Measure tool?',QMessageBox.Ok ,QMessageBox.Close)
            if reply == QMessageBox.Ok:
                self.parent().close()
        elif self.current_info["Image Save"] == False:
            reply = QMessageBox.warning(None, 'Close window', 'The measurement data has not been saved yet. Do you want to exit?',QMessageBox.Ok ,QMessageBox.Close)
            if reply == QMessageBox.Ok:
                self.parent().close()
        else:
            self.parent().close()

if __name__=="__main__":
    app = QApplication(sys.argv)
    app_icon = QIcon("icon/main.png")
    app.setWindowIcon(app_icon)
    app_window = MainWindow()
    sys.exit(app.exec_())
