import re, cv2, json, pathlib, shutil
import numpy as np
import datetime as dt
from PySide2.QtWidgets import  *
from PySide2.QtGui import *
from PySide2.QtCore import *
import utils.stylelogging as log
import focus as fo
import ctypes # 0505 Sarah add

import configparser
measuretool_config = configparser.ConfigParser()
measuretool_config.read("measure_tool_config.ini",encoding='utf-8')
theme = measuretool_config.get('Style','theme')
tag_coordinates_x = int(measuretool_config.get('CAMERA','tag_coordinates_x'))
tag_coordinates_y = int(measuretool_config.get('CAMERA','tag_coordinates_y'))
tag_r, tag_g, tag_b = [int(x) for x in measuretool_config.get('CAMERA','tag_rgb').split(',')]
tag_size = float(measuretool_config.get('CAMERA','tag_size'))
focus_bg_r, focus_bg_g, focus_bg_b = [int(x) for x in measuretool_config.get('CAMERA','focus_background_rgb').split(',')]


if theme == 'Default':
    import qdarkstyle
logger = log.StyleLog(loglevel='DEBUG', logger_name='manual-screenshot', logfile='./log/log.log')

class Camera_MainWindow(QMainWindow): 
    def __init__(self, parent=None):
        super().__init__(parent)
        
        # creating EmailBlast widget and setting it as central
        self.setWindowTitle("Edge Device Manual Screenshot.")
        self.resize(1024, 580)
        self.app_widget = Camera_App(parent=self)
        self.setCentralWidget(self.app_widget)
    
class VideoThread(QThread):
    
    # 0411 Polly PySide2
    change_pixmap_signal = Signal(np.ndarray)
    camera_status = Signal(str)

    def __init__(self):
        super().__init__()
        self._run_flag = False
        self.record_flag = False
        self.record_path = None
        
    def run(self):
        # capture from web cam
        cap = cv2.VideoCapture(0)
        if cap:
            self._run_flag = True
            self.camera_status.emit('connect')
        while self._run_flag:
            ret, cv_img = cap.read()
            if ret:
                # focus judge 
                if self.record_flag:
                    self.origin_out.write(cv_img)
                lightness_value = fo.get_lightness(cv_img)
                grayCrop = cv2.cvtColor(cv_img, cv2.COLOR_RGB2GRAY)
                hist_value = fo.hist(grayCrop)
                mean, focus_flag = fo.detect_blur_fft(grayCrop, lightness_value,hist_value)
                if mean < 0:
                    mean = 0
                dif = int(mean)*4

                # 計算數值文本寬高
                retval, baseLine = cv2.getTextSize(str(int(mean)),fontFace=cv2.FONT_HERSHEY_SIMPLEX,fontScale=0.5, thickness=1)
                topleft = (cv_img.shape[1]-30, 30 - retval[1]-5)
                bottomright = (topleft[0] + retval[0], topleft[1] + retval[1]+8)
                # 放上Bar條(往左offset-5)+數值文本背景框
                cv2.rectangle(cv_img, (cv_img.shape[1]-30-dif-5, topleft[1] - baseLine), bottomright, thickness=-1, color=(focus_bg_r, focus_bg_g, focus_bg_b))

                cv2.putText(cv_img, str(int(mean)), (cv_img.shape[1]-30, 30-baseLine), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0,), 1, cv2.LINE_AA)
                cv2.rectangle(cv_img, (cv_img.shape[1]-30-dif, 20),  (cv_img.shape[1]-35, 30), (0,0,0), -1)

                self.change_pixmap_signal.emit(cv_img)
            else:
                self.camera_status.emit('disconnect')
                self.stop()
        cap.release()
    
    def stop(self):
        """Sets run flag to False and waits for thread to finish"""
        self._run_flag = False
        self.quit()
    
    def create_mp4(self):
        record_filename = '%d.mp4'%(int(dt.datetime.now().timestamp()*1000))
        record_file_path = self.record_path / record_filename
        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        self.origin_out = cv2.VideoWriter(record_file_path.resolve().as_posix(), fourcc, 20.0, (640,  480))
        record_file_path.chmod(mode=0o777)
    
    def kill_record(self):
        self.origin_out.release()

class switch_btn(QPushButton):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.setCheckable(True)
        self.setMinimumWidth(120)
        self.setMinimumHeight(40)

    def paintEvent(self, event):
        self.status = "live" if self.isChecked() else "stop"
        bg_color =  Qt.green if self.isChecked() else Qt.red

        radius = 15 # 控制btn大小?
        width = 55 # 控制btn長度
        center = self.rect().center()

        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.translate(center)
        painter.setBrush(QColor(0,0,0))

        pen = QPen(Qt.black)
        pen.setWidth(2)
        painter.setPen(pen)

        painter.drawRoundedRect(QRect(-width, -radius, 2*width, 2*radius), radius, radius)
        painter.setBrush(QBrush(bg_color))
        sw_rect = QRect(-radius, -radius, width + radius, 2*radius)
        if not self.isChecked():
            sw_rect.moveLeft(-width)
        painter.drawRoundedRect(sw_rect, radius, radius)
        painter.drawText(sw_rect, Qt.AlignCenter, self.status)
    
class Camera_App(QWidget):

    # 0411 Polly PySide2
    submitClicked = Signal(dict)
    calibration_scale = Signal(float)
    magnification_value = Signal(str)
    def __init__(self,parent=None):
        super().__init__(parent)

        self.create_img_folder()
        self.screenshot_info_dcit = {}
        self.camera_live = True
        self.table_index = 0
        self.add_tag = False # 圖片加上流水號
        self.tag_num = 1
        self.tag_dict = {}
        self.specified_folder_name = None # 指定資料夾名; 在日期資料夾再創建一個資料夾
        self.now_folder_path
        self.record_flag = False

        self.centralwidget = QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QRect(5, 5, 1010, 570))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)

        # Button
        horizontalLayout_main = QHBoxLayout()

        self.switch_button = switch_btn(self.verticalLayoutWidget)
        self.switch_button.setChecked(True)
        self.switch_button.clicked.connect(self.live_or_stop)
        horizontalLayout_main.addWidget(self.switch_button)

        self.screenshot_button = QPushButton(self.verticalLayoutWidget)
        self.screenshot_button.setIcon(QIcon('icon/screenshot_btn.png'))
        self.screenshot_button.setIconSize(QSize(30, 30))
        self.screenshot_button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.screenshot_button.setEnabled(False)
        self.screenshot_button.clicked.connect(self.screenshot)
        # self.screenshot_button.clicked.connect(self.update_label)
        self.screenshot_button.setToolTip("Screenshot")
        horizontalLayout_main.addWidget(self.screenshot_button)

        self.record_button = QPushButton(self.verticalLayoutWidget)
        self.record_button.setIcon(QIcon('icon/record_btn.png'))
        self.record_button.setIconSize(QSize(30, 30))
        self.record_button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.record_button.setEnabled(False)
        self.record_button.clicked.connect(self.recordFrame)
        self.record_button.setToolTip("Record")
        horizontalLayout_main.addWidget(self.record_button)
        
        self.input_magnification_button = QPushButton(self.verticalLayoutWidget)
        self.input_magnification_button.setIcon(QIcon('icon/adjustment_btn.png'))
        self.input_magnification_button.setIconSize(QSize(30, 30))
        self.input_magnification_button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.input_magnification_button.setEnabled(False)
        self.input_magnification_button.clicked.connect(self.input_magnification)
        self.input_magnification_button.setToolTip("Input camera magnification value")
        horizontalLayout_main.addWidget(self.input_magnification_button)

        self.tag_button = QPushButton(self.verticalLayoutWidget)
        self.tag_button.setIcon(QIcon('icon/tag_btn.png'))
        self.tag_button.setIconSize(QSize(30, 30))
        self.tag_button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.tag_button.setEnabled(False)
        self.tag_button.clicked.connect(self.tag_image)
        self.tag_button.setToolTip("Inscribe a sequential number on the image")
        horizontalLayout_main.addWidget(self.tag_button)

        self.input_button = QPushButton(self.verticalLayoutWidget)
        self.input_button.setIcon(QIcon('icon/camera_input_btn'))
        self.input_button.setIconSize(QSize(30, 30))
        self.input_button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.input_button.setEnabled(False)
        self.input_button.clicked.connect(self.input_folder_name)
        self.input_button.setToolTip("Create a folder with the specified name")
        horizontalLayout_main.addWidget(self.input_button)

        self.previous_button = QPushButton(self.verticalLayoutWidget)
        self.previous_button.setIcon(QIcon('icon/camera_previous_btn.png'))
        self.previous_button.setIconSize(QSize(30, 30))
        self.previous_button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.previous_button.setEnabled(False)
        self.previous_button.clicked.connect(self.previous)
        horizontalLayout_main.addWidget(self.previous_button)

        int_validator = QIntValidator()
        self.input_tag_line_edit = QLineEdit(self.verticalLayoutWidget)
        self.input_tag_line_edit.setFixedSize(30, 30)  # 设置大小限制为30x30
        self.input_tag_line_edit.setAlignment(Qt.AlignCenter)  # 文本居中显示
        self.input_tag_line_edit.setEnabled(False)
        self.input_tag_line_edit.setValidator(int_validator)
        self.input_tag_line_edit.textChanged.connect(self.tag_value_changed)
        horizontalLayout_main.addWidget(self.input_tag_line_edit)

        self.next_button = QPushButton(self.verticalLayoutWidget)
        self.next_button.setIcon(QIcon('icon/camera_next_btn'))
        self.next_button.setIconSize(QSize(30, 30))
        self.next_button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.next_button.setEnabled(False)
        self.next_button.clicked.connect(self.next)
        horizontalLayout_main.addWidget(self.next_button)

        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        horizontalLayout_main.addItem(spacerItem)

        self.measure_button = QPushButton(self.verticalLayoutWidget)
        self.measure_button.setIcon(QIcon('icon/exit2measurement_btn.png'))
        self.measure_button.setIconSize(QSize(30, 30))
        self.measure_button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.measure_button.setEnabled(False)
        self.measure_button.clicked.connect(self.measure_check)
        self.measure_button.setToolTip("Window close and return screenshot")
        horizontalLayout_main.addWidget(self.measure_button)

        self.close_button = QPushButton(self.verticalLayoutWidget)
        self.close_button.setIcon(QIcon('icon/quit_btn.png'))
        self.close_button.setIconSize(QSize(30, 30))
        self.close_button.setStyleSheet('border-radius: 5px; border: 1px groove gray;border-style: outset;')
        self.close_button.clicked.connect(self.close_check)
        self.close_button.setToolTip("Window close without return screenshot")

        horizontalLayout_main.addWidget(self.close_button)

        self.verticalLayout.addLayout(horizontalLayout_main)
        
        # VIDEO
        horizontalLayout_video = QHBoxLayout()

        self.video_label = QLabel(self.verticalLayoutWidget)
        self.video_label.setFrameShape(QFrame.Box)
        self.video_label.setFrameShadow(QFrame.Raised)
        self.video_label.setLineWidth(2)
        horizontalLayout_video.addWidget(self.video_label)

        # Screenshot list
        horizontalLayout_info = QVBoxLayout()

        font_big = QFont()
        font_big.setFamily("微軟正黑體")
        font_big.setPointSize(15)

        self.count_label = QLabel(self.verticalLayoutWidget)
        self.count_label.setFont(font_big)
        self.count_label.setFrameShape(QFrame.Box)
        self.count_label.setFrameShadow(QFrame.Raised)
        self.count_label.setLineWidth(2)
        self.count_label.setText('Screenshot count: %d'%(len(self.screenshot_info_dcit)))
        horizontalLayout_info.addWidget(self.count_label)

        font_small = QFont()
        font_small.setFamily("微軟正黑體")
        font_small.setPointSize(10)

        self.screenshot_table = QTableWidget(20, 3)
        self.screenshot_table.resizeColumnsToContents()
        self.screenshot_table.resizeRowsToContents()
        self.screenshot_table.setColumnWidth(0, 20)
        self.screenshot_table.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)
        self.screenshot_table.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)
        self.screenshot_table.horizontalHeader().setSectionResizeMode(2, QHeaderView.Stretch)
        self.screenshot_table.setSelectionMode(QAbstractItemView.SingleSelection)#设置只能选中一行
        self.screenshot_table.setEditTriggers(QTableView.NoEditTriggers)#不可编辑
        self.screenshot_table.setSelectionBehavior(QAbstractItemView.SelectRows);#设置只有行选中
        self.screenshot_table.setShowGrid(False) # #表格中不显示分割线
        self.screenshot_table.horizontalHeader().hide()
        self.screenshot_table.verticalHeader().setVisible(False) # index不顯示
        self.screenshot_table.setItem(0, 0, QTableWidgetItem(''))
        self.screenshot_table.setItem(0, 1, QTableWidgetItem(''))
        self.screenshot_table.setItem(0, 2, QTableWidgetItem(''))

        selection_model = self.screenshot_table.selectionModel()
        selection_model.selectionChanged.connect(self.on_selectionChanged)

        horizontalLayout_info.addWidget(self.screenshot_table)

        # preview-table
        self.preview_label = QLabel(self.verticalLayoutWidget)
        self.preview_label.setFrameShape(QFrame.Box)
        self.preview_label.setFrameShadow(QFrame.Raised)
        self.preview_label.setLineWidth(2)
        self.preview_label.setGeometry(50, 80, 100, 100)
        horizontalLayout_info.addWidget(self.preview_label)

        horizontalLayout_video.addLayout(horizontalLayout_info)

        self.verticalLayout.addLayout(horizontalLayout_video)

        self.activate_thread()

        self.setWindowOpacity(0.9) # 设置窗口透明度
        if theme == 'Default':
            self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
        else:
            with open(f"./statics/manual_tool_theme/{theme}.qss", "r", encoding="UTF-8") as file:
                style_sheet = file.read()
            self.setStyleSheet(style_sheet)

    # @pyqtSlot('QItemSelection')
    @Slot('QItemSelection')  # 0411 Polly PySide2
    def on_selectionChanged(self, selected):
        indexes = selected.indexes()
        if indexes:
            selected_index = indexes[0]
            if self.screenshot_table.item(selected_index.row(), 0):
                self.tag_num = int(self.screenshot_table.item(selected_index.row(), 0).text())
                self.input_tag_line_edit.setText(str(self.tag_num))
                img_filename = self.screenshot_table.item(selected_index.row(), 1).text()
                img_file_path = '%s/%s'%(self.now_folder_path, img_filename)
                self.show_preview(img_file_path)
                logger.debug(f"Select tag={self.tag_num}; filename={img_filename} .")
    
    def live_or_stop(self):
        self.camera_live = False if self.camera_live else True
        logger.info(f"Switch camera live={self.camera_live}.")
        
    def create_img_folder(self):
        now_datetime = dt.datetime.now().strftime('%Y-%m-%d')
        self.main_folder_path = pathlib.Path('./screenshot_image/%s'%now_datetime)
        self.now_folder_path = pathlib.Path('./screenshot_image/%s'%now_datetime)
        if not self.main_folder_path.exists():
            self.main_folder_path.mkdir(parents=True) 
            self.main_folder_path.chmod(mode=0o777)
            logger.info(f"Create screenshot date folder='{str(self.main_folder_path)}'.")

    def activate_thread(self):
        self.thread = VideoThread()
        self.thread.change_pixmap_signal.connect(self.update_image)
        self.thread.camera_status.connect(self.camera_disconect_message)
        self.thread.start()
    
    def deactivate_thread(self):
        self.thread.stop()

    def camera_disconect_message(self,camera_info):
        if camera_info == 'disconnect':
            QMessageBox.warning(None, 'Warning Message', 'Camera disconnect！',QMessageBox.Ok)
            logger.warning("Camera disconnect.")
            self.screenshot_button.setEnabled(False)
            self.input_magnification_button.setEnabled(False)
            self.tag_button.setEnabled(False)
            self.input_button.setEnabled(False)
            self.record_button.setEnabled(False)
        elif camera_info == 'connect':
            self.screenshot_button.setEnabled(True)
            self.input_magnification_button.setEnabled(True)
            self.tag_button.setEnabled(True)
            self.input_button.setEnabled(True)
            self.record_button.setEnabled(True)

    # @pyqtSlot(np.ndarray)
    @Slot(np.ndarray)  # 0411 Polly PySide2
    def update_image(self, cv_img):
        """Updates the image_label with a new opencv image"""
        if self.camera_live:
            qt_img = self.convert_cv_qt(cv_img)
            self.video_label.setPixmap(qt_img)
    
    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
         # 0505 Sarah modify
        if self.camera_live:
            rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
            if self.add_tag:
                # 將流水號tag到圖片
                cv2.putText(rgb_image, f'No.{self.tag_num}', (tag_coordinates_x,tag_coordinates_y), cv2.FONT_HERSHEY_DUPLEX, tag_size, (tag_r, tag_g, tag_b), 1,  cv2.LINE_AA)
            self.temp_rgb_image = rgb_image
            h, w, channel = rgb_image.shape
            bytes_per_line = channel * w
            ch = ctypes.c_char.from_buffer(rgb_image, 0)
            rcount = ctypes.c_long.from_address(id(ch)).value
            convert_to_Qt_format = QImage(ch, w, h, bytes_per_line, QImage.Format_RGB888)
            p = convert_to_Qt_format.scaled(700, 520, Qt.KeepAspectRatio)
            ctypes.c_long.from_address(id(ch)).value = rcount
            return QPixmap.fromImage(p)

    def screenshot(self):
        self.file_name = '%d.png'%(int(dt.datetime.now().timestamp()*1000))
        file_path = self.now_folder_path / self.file_name
        rgb_image = cv2.cvtColor(self.temp_rgb_image, cv2.COLOR_BGR2RGB)
        cv2.imwrite(str(file_path),rgb_image)
        file_path.chmod(mode=0o777)
        if self.tag_num not in self.screenshot_info_dcit:
            self.screenshot_info_dcit[self.tag_num] = {"file_name":self.file_name, "path":pathlib.Path(file_path).resolve().as_posix()}
            self.count_label.setText('Screenshot count: %d'%(len(self.screenshot_info_dcit)))
            self.screenshot_table.insertRow(self.screenshot_table.rowCount())

            deleteButton = QPushButton("Delete")
            index_item = QTableWidgetItem(str(self.tag_num))
            if self.specified_folder_name:
                index_item.setBackground(QColor(210,210,210))
                index_item.setForeground(QColor(0, 0, 0))
                index_item.setTextAlignment(Qt.AlignCenter)  # 设置文本水平居中
                index_item.setTextAlignment(Qt.AlignVCenter)  # 设置文本垂直居中
            self.screenshot_table.setItem(self.table_index, 0, index_item)
            self.screenshot_table.setItem(self.table_index, 1, QTableWidgetItem(self.file_name))
            deleteButton.clicked.connect(self.delete_clicked)
            self.screenshot_table.setCellWidget(self.table_index, 2, deleteButton)
            logger.info(f"Screenshot new filename={self.file_name} tag={self.tag_num}.")
            self.table_index += 1
        else:
            select_row = self.current_row()
            self.screenshot_table.setItem(select_row, 1, QTableWidgetItem(self.file_name))
            self.screenshot_info_dcit[self.tag_num] = {"file_name":self.file_name, "path":pathlib.Path(file_path).resolve().as_posix()}
            logger.info(f"Screenshot old filename={self.file_name} tag={self.tag_num}.")
            # 如果截圖時,tag已經存在,截完圖後,tag取列表最大值+1(下一張)
        self.tag_num = max(self.screenshot_info_dcit.keys())+1
        self.input_tag_line_edit.setText(str(self.tag_num))
        self.measure_button.setEnabled(True)
        self.show_preview(file_path)
    
    def current_row(self):
        for row in range(self.screenshot_table.rowCount()):
            index_num = self.screenshot_table.model().data(self.screenshot_table.model().index(row, 0))
            if index_num:
                if int(index_num) == self.tag_num:
                    return row
        return None

    def measure_check(self):
        reply = QMessageBox.warning(None, 'Close window warning message', 'Return measurement tool.',QMessageBox.Ok ,QMessageBox.Close)
        if reply == QMessageBox.Ok:
            self.thread.stop()
            self.parent().close()
            if self.specified_folder_name is not None:
                specified_folder_record_data_path = self.now_folder_path / f'{self.specified_folder_name}_ScreenshotInfo.json'
                with open(specified_folder_record_data_path, 'w', encoding='utf-8') as f:
                    json.dump(self.screenshot_info_dcit, f)
                specified_folder_record_data_path.chmod(mode=0o777)
            logger.info(f"Return measurement tool with {len(self.screenshot_info_dcit)} image.")
            self.submitClicked.emit({"Project":self.specified_folder_name, "Image":self.screenshot_info_dcit}) # 把指定資料夾名稱帶回量測頁面
                
    def close_check(self):
        reply = QMessageBox.warning(None, 'Close window warning message', 'Just close.',QMessageBox.Ok , QMessageBox.Close)
        if reply == QMessageBox.Ok:
            self.thread.stop()
            self.parent().close()
            if self.now_folder_path.exists():
                shutil.rmtree(self.now_folder_path)
            logger.info("Just close.")
    
    def show_preview(self, file_path):
        img = QPixmap(str(file_path))                
        smaller_pixmap = img.scaled(300, 260, Qt.KeepAspectRatio, Qt.FastTransformation)
        self.preview_label.setPixmap(smaller_pixmap)
        
    # @pyqtSlot()
    # @Slot()   # 0411 Polly PySide2
    def delete_clicked(self):
        button = self.sender()
        if button:
            row = self.screenshot_table.indexAt(button.pos()).row()
            select_tag_num = int(self.screenshot_table.model().data(self.screenshot_table.model().index(row, 0)))
            filename = self.screenshot_table.model().data(self.screenshot_table.model().index(row, 1))
            self.screenshot_table.removeRow(row)
            pathlib.Path(self.screenshot_info_dcit[select_tag_num]["path"]).unlink()
            del self.screenshot_info_dcit[select_tag_num]
            logger.info(f"Delete one image filename={filename}(tag:{select_tag_num}) row index={row} success.")
            self.table_index -= 1
            if list(self.screenshot_info_dcit.keys()):
                last_screenshot_tag_num = list(self.screenshot_info_dcit.keys())[-1]
                self.show_preview(self.screenshot_info_dcit[last_screenshot_tag_num]["path"])
            if self.screenshot_info_dcit == {}:
                self.tag_num = 1
                self.preview_label.clear()
                self.input_tag_line_edit.setText(str(self.tag_num))
                logger.info("All image was delete, clear preview label.")
            else:
                self.tag_num = max(self.screenshot_info_dcit.keys())+1
                self.input_tag_line_edit.setText(str(self.tag_num))
            self.count_label.setText('Screenshot count: %d'%(len(self.screenshot_info_dcit)))
                
    def input_magnification(self):
        self.magnification, ok = QInputDialog.getText(self, 'Magnification input dialog', 'Please input a number in the range of 20~250')
        logger.info(f"Input magnification='{self.magnification}'.")
        if ok:
            if self.magnification.isdigit():
                if int(self.magnification) >= 20 and int(self.magnification) <= 250:
                    self.magnification_value.emit(self.magnification)
                    QMessageBox.information(None, 'Application Message', 'Camera magnification applied to measure tool',QMessageBox.Ok)
                    logger.info("Camera magnification apply.")
                else:
                    QMessageBox.warning(None, 'Warning Message', 'Number in the range of 20~250!',QMessageBox.Ok)
            else:
                QMessageBox.warning(None, 'Warning Message', 'Input value not number! Please try again!',QMessageBox.Ok)
    
    def tag_image(self):
        if self.add_tag:
            self.add_tag = False
            self.tag_button.setIcon(QIcon('icon/tag_btn.png'))
            self.previous_button.setEnabled(False)
            self.input_tag_line_edit.setEnabled(False)
            self.next_button.setEnabled(False)
            # 當中途停用,設定tag為紀錄tag列表最大值+1,避免tag卡住在使用者選擇停留數值
            if self.screenshot_info_dcit:
                self.tag_num = max(self.screenshot_info_dcit.keys())+1
            logger.info("Deactivate tag image.")
        else:
            self.add_tag = True
            self.tag_button.setIcon(QIcon('icon/tag_on_btn.png'))
            self.input_tag_line_edit.setEnabled(True)
            self.tag_value_changed()
            logger.info("Activate tag image.")

    def input_folder_name(self):
        if self.specified_folder_name:
            specified_folder_name_copy = self.specified_folder_name + '' # 暫存用,防使用者取消後,回傳值為空,下次再開啟就變空白的問題
        self.specified_folder_name, ok = QInputDialog.getText(self, 'Specified name dialog', 'Please enter the specified folder name', text=self.specified_folder_name)
        if ok:
            if self.specified_folder_name == '':
                self.specified_folder_name = None
                self.now_folder_path = self.main_folder_path.copy()
                logger.info(f"Use default screenshote date folder='{str(self.main_folder_path)}'.")
                self.input_button.setIcon(QIcon('icon/camera_input_btn'))
                QMessageBox.information(None, 'Apply Message', 'Change the storage location to the default path.',QMessageBox.Ok)
            else:
                logger.info(f"Input specified name = {self.specified_folder_name}")
                folder_name_pattern = r"^[a-zA-Z0-9_-]+[^_-]$" # linux fromat
                # folder_name_pattern = r"^[a-zA-Z0-9\s\-\(\)\[\]\{\}\&\%\$\#\@\!\^\`\',.+=_]+$" # win format
                if re.match(folder_name_pattern, self.specified_folder_name) is not None:
                    self.now_folder_path = self.main_folder_path / self.specified_folder_name
                    if not self.now_folder_path.exists():
                        self.now_folder_path.mkdir(parents=True)
                        self.now_folder_path.chmod(mode=0o777)
                        logger.info("The folder has been created.") 
                        self.input_button.setIcon(QIcon('icon/camera_input_on_btn'))
                    else:
                        self.input_button.setIcon(QIcon('icon/camera_input_on_btn'))
                        logger.warning("The folder already exists.")
                        QMessageBox.warning(None, 'Warning Message', 'The directory folder already exists and has been set as the save folder.！',QMessageBox.Ok)
                else:
                    logger.warning("The folder naming rules error.")
                    QMessageBox.warning(None, 'Warning Message', "The folder name does not comply with the system's naming format for folders！",QMessageBox.Ok)
        else:
            self.specified_folder_name = specified_folder_name_copy + ''
    def next(self):
        if self.tag_num <= max(self.screenshot_info_dcit.keys()):
            self.tag_num += 1
            logger.debug(f'Switch next tag={self.tag_num}')
            self.tag_value_changed()

    def previous(self):
        if self.tag_num > 1:
            self.tag_num -= 1
            logger.debug(f'Switch previous tag={self.tag_num}')
            self.tag_value_changed()
    
    def tag_value_changed(self, value=None):
        if value:
            value = int(value)
            self.tag_num = value + 0
        self.input_tag_line_edit.setText(str(self.tag_num))
        if self.tag_num in self.screenshot_info_dcit:
            self.show_preview(self.screenshot_info_dcit[self.tag_num]["path"])
        if self.add_tag:
            if self.tag_num == 1 and self.screenshot_info_dcit == {}: # 起始或是截圖全刪除狀態
                self.previous_button.setEnabled(False)
                self.next_button.setEnabled(False)
            elif self.tag_num == 1 and self.screenshot_info_dcit != {}: # 切到第一張
                self.previous_button.setEnabled(False)
                self.next_button.setEnabled(True)
            elif self.tag_num > 1 and self.tag_num > max(self.screenshot_info_dcit.keys()):
                self.previous_button.setEnabled(True)
                self.next_button.setEnabled(False)
            elif self.tag_num > 1 and self.tag_num <= max(self.screenshot_info_dcit.keys()):
                self.previous_button.setEnabled(True)
                self.next_button.setEnabled(True)
    
    def recordFrame(self):
        if not self.record_flag:
            reply = QMessageBox.warning(None, 'Record message', 'Are you sure you want to record the video?.', QMessageBox.Ok ,QMessageBox.Close)
            if reply == QMessageBox.Ok:
                self.record_flag = True
                self.thread.record_path = self.now_folder_path
                self.thread.create_mp4()
                self.thread.record_flag = True
                self.record_button.setIcon(QIcon('icon/record_on_btn.png'))
                logger.info('Start record video.')
        else:
            self.record_flag = False
            self.thread.record_path = None
            self.thread.kill_record()
            self.thread.record_flag = False
            self.record_button.setIcon(QIcon('icon/record_btn.png'))
            logger.info('Stop record video.')



            
