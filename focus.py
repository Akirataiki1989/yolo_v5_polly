# version 2 from mail 20230119
import numpy as np
import cv2

def detect_blur_fft(image, lightness_value,hist_value):
    if lightness_value < 120:
        size = 30
        thresh = 27

    if lightness_value >= 120:
        size = 100
        thresh = 30

    if lightness_value >= 150:
        size = 160
        thresh = 30

    if hist_value < 1:
        size = 20  #30
        thresh = 23

    if hist_value < 0.5:
        size = 10  #30
        thresh = 13

    # grab the dimensions of the image and use the dimensions to
    # derive the center (x, y)-coordinates
    (h, w) = image.shape
    (cX, cY) = (int(w / 2.0), int(h / 2.0))

 
    # compute the FFT to find the frequency transform, then shift
    # the zero frequency component (i.e., DC component located at
    # the top-left corner) to the center where it will be more
    # easy to analyze
    fft = np.fft.fft2(image)
    fftShift = np.fft.fftshift(fft)


    # zero-out the center of the FFT shift (i.e., remove low
    # frequencies), apply the inverse shift such that the DC
    # component once again becomes the top-left, and then apply
    # the inverse FFT

    fftShift[cY - size:cY + size, cX - size:cX + size] = 0
    fftShift = np.fft.ifftshift(fftShift)
    recon = np.fft.ifft2(fftShift)


    # compute the magnitude spectrum of the reconstructed image,
    # then compute the mean of the magnitude values
    magnitude = 20 * np.log(np.abs(recon))
    mean = np.mean(magnitude)

    # the image will be considered "blurry" if the mean value of the
    # magnitudes is less than the threshold value

    # 芷珊新增 不然會有OverflowError: cannot convert float infinity to integer問題
    if mean in [float("-inf"),float("inf")]:
        mean = float(100)
    return (mean, mean <= thresh)

 

def get_lightness(src):
    # 計算亮度
    hsv_image = cv2.cvtColor(src, cv2.COLOR_BGR2HSV)
    lightness = hsv_image[:,:,2].mean()
    # print('lightness:',lightness)    #polly暫時註解
    return  lightness  

def hist(src):
    histb = cv2.calcHist([src], [0], None, [256], [0,255])
    white_value = histb[:150].sum()
    dark_value = histb[150:].sum()
    # print('hist_value:',white_value / dark_value)    #polly暫時註解
    return white_value / dark_value


# if __name__ == '__main__':
#     # main()
#     lightness_value = get_lightness(frame)
#     hist_value = hist(gray)
#     mean, blurry = detect_blur_fft(gray,lightness_value,hist_value)