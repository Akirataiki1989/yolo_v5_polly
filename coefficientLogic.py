import os
import configparser
from bisect import bisect_left

def findNearest(num):
    numList = [ 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95,   \
    100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175, 180, 185, 190, 195,  \
    200, 205, 210, 215, 220, 225, 230, 235, 240, 245, 250 ]
    pos = bisect_left(numList, num)
    if pos == 0:
        return numList[0]
    if pos == len(numList):
        return numList[-1]
    before = numList[pos - 1]
    after = numList[pos]
    return before, after

def calculateCoefficient(previousNum, nextNum):
    config = configparser.ConfigParser()
    config.read('magnificationRate.ini')
    with open(os.path.join('./captureAndSave/setParameter.txt'), 'r') as f:
        data = f.read()
    magnification = data.split(',')[0] 
    previousNumX = float(config.get('OM', str(previousNum)+ 'M_X'))
    nextNumX = float(config.get('OM', str(nextNum)+ 'M_X'))
    previousNumY = float(config.get('OM', str(previousNum)+ 'M_Y'))
    nextNumY = float(config.get('OM', str(nextNum)+ 'M_Y'))
    coefficientX =  (previousNumX-nextNumX)/5
    coefficientY =  (previousNumY-nextNumY)/5


    if int(magnification) % 5 == 0:
        magnificationX = float(config.get('OM', magnification + 'M_X'))
        magnificationY = float(config.get('OM', magnification + 'M_Y'))
    elif int(magnification) % 5 == 1:
        magnificationX = previousNumX-coefficientX
        magnificationY = previousNumY-coefficientY
    elif int(magnification) % 5 == 2:
        magnificationX = previousNumX-coefficientX*2
        magnificationY = previousNumY-coefficientY*2
    elif int(magnification) % 5 == 3:
        magnificationX = previousNumX-coefficientX*3
        magnificationY = previousNumY-coefficientY*3
    elif int(magnification) % 5 == 4:
        magnificationX = previousNumX-coefficientX*4
        magnificationY = previousNumY-coefficientY*4
    return  magnificationX, magnificationY



# # # 調整OM倍率
# magnificationList = [ 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95,   \
#     100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175, 180, 185, 190, 195,  \
#     200, 205, 210, 215, 220, 225, 230, 235, 240, 245, 250 ]
    
# magnification = int(input('magnification:'))
# previousNum, nextNum = findNearest(magnification)
# magX, magY = calculateCoefficient(previousNum, nextNum)
# print(magX)
# print(magY)